[![Se communecter, 
c’est se connecter à sa commune 
Un e-mail,  un code postal et c’est parti…
](https://framapic.org/iUtBZoyM8HMe/mrffwNL9t4OH.png)](https://vimeo.com/161594058)


# Sommaire

* [I. Le projet “Communecter” : c’est quoi ?](https://github.com/pixelhumain/communecter/wiki/Pr%C3%A9sentation-simplifi%C3%A9e#i-le-projet-communecter---cest-quoi-)
* [II. L’outil “Communecter” : description](https://github.com/pixelhumain/communecter/wiki/Pr%C3%A9sentation-simplifi%C3%A9e#ii-loutil-communecter---description)
* [III. Pourquoi souhaitons-nous réaliser “Communecter” ?](https://github.com/pixelhumain/communecter/wiki/Pr%C3%A9sentation-simplifi%C3%A9e#iii-pourquoi-souhaitons-nous-r%C3%A9aliser-communecter-)
* [IV. “Communecter” est un projet pour qui ?](https://github.com/pixelhumain/communecter/wiki/Pr%C3%A9sentation-simplifi%C3%A9e#iv-communecter-est-un-projet-pour-qui-)
* [V. Moyens mis en œuvre et engagements](https://github.com/pixelhumain/communecter/wiki/Pr%C3%A9sentation-simplifi%C3%A9e#v-moyens-mis-en-%C5%93uvre-et-engagements)
* [VI. Partenaires](https://github.com/pixelhumain/communecter/wiki/Pr%C3%A9sentation-simplifi%C3%A9e#vi-partenaires)
* [VII. Résultats](https://github.com/pixelhumain/communecter/wiki/Pr%C3%A9sentation-simplifi%C3%A9e#vii-r%C3%A9sultats---voir-doc-evaluation-et-indicateurs-pixel-humainpdf)
* [VIII. Viabilité du projet](https://github.com/pixelhumain/communecter/wiki/Pr%C3%A9sentation-simplifi%C3%A9e#viii-viabilit%C3%A9-du-projet)

# I. Le projet “Communecter” - c’est quoi ?
“Communecter” est un outil innovant et open source de développement de territoire avec une approche locale (quartier, commune, département, région...) disponible sur des applications mobiles et internet. 
Cette plateforme, portée par le collectif “Pixel Humain”, simple et gratuite de concertation et d'interactions entre citoyens, associations, entreprises et collectivités locales est un véritable catalyseur de dynamique de territoire. Cet outil est le lien manquant entre habitants pour une nécessaire cohésion sociale, l’outil fédérateur pour des initiatives d’intérêt général durables marquées sous le sceau de la coopération, l’interaction efficiente entre les différents acteurs du territoire. Il est le reflet des actions et des projets menés sur le terrain.  “Communecter” avec son réseau social citoyen est un outil unique en son genre, libre d’accès, ouvert à tous, reproductible sur différents territoires et traitant de thématiques d’intérêt général.

L’application permet aux citoyens de bénéficier d’une plateforme commune pour participer à la vie de leur territoire. A l’heure d’internet, de la simplification de la communication et du partage de l’information, tout est réuni pour fédérer et organiser cette démarche avec le citoyen au centre de la dynamique.
Ainsi, la plateforme propose aux citoyens de se “communecter”, se connecter à leur commune. Ils deviennent alors :
informés en quasi temps réel de la dynamique de leur territoire; activités, événements, débats, actions, projets…
actifs en coopérant à la dynamique locale; partage d’information, dialogue interactif facilité avec les collectivités, regroupement de citoyens par affinités pour réfléchir ou débattre, échanger, agir concrètement sur le terrain au travers d’événements, d’actions, de projets.
connectés avec d’autres utilisateurs du même territoire pour partager des centres d’intérêts communs, avec des associations locales qui proposent des initiatives sociales et solidaires, avec des entreprises et producteurs qui fabriquent localement des produits qu’ils achètent... Bref connecter le virtuel et le réel, le Pixel et l’Humain.
promoteurs de leur activité propre, associative, entreprenariale...

La finalité du collectif Pixel Humain est d’apporter aux Collectivités, Associations et Entreprises les réflexions individuelles et de donner aux individus l’intelligence collective.

# II. L’outil “Communecter” - description

L’outil “Communecter” est simple d’utilisation et accessible à tous. Il suffit de se « communecter » (en fournissant simplement un code postal et une adresse mail) pour accéder à une plateforme de concertation et d’interaction où l’utilisateur va pouvoir utiliser :

### a. Des fonctionnalités innovantes via différents modules interconnectés :

![](http://misc.nouvelle-aquitaine-fr.eu/communecter/images/moduleinterco0001.png)

La part innovante du projet se retrouve dans les fonctionnalités suivantes dont une partie a été incubée à la Technopole de la Réunion :
Application collectivité
Une interface bidirectionnelle et interactive citoyens/collectivités
Un Module Opendata d'ouverture des données publiques 
Possibilité d’assister aux conseils municipaux de sa ville « on air » en direct ou différé depuis son domicile, couplé à un « chat » permettant d’interagir directement avec le conseil municipal.
Application débats
Un module de débat citoyen innovant
Une modération des débats.
Les idées, projets, actions seront agrégés et proposés au vote des utilisateurs.
Des applications complètes de gestion d’événements, d’actions terrains et de projets qui permettent de bien refléter et capitaliser les résultats atteints. 

### b. Un système d’information filtrable et paramétrable : 

L’utilisateur définit ses centres d’intérêts à l’aide d’une liste de thématiques, tiré de l’activité et des compétences locales

![](http://misc.nouvelle-aquitaine-fr.eu/communecter/images/listethem.png)

- il navigue et filtre l’information à l’aide d’un zoom administratif au travers de 6 périmètres géographiques 

![](http://misc.nouvelle-aquitaine-fr.eu/communecter/images/zoomgeo.png)

Quelques exemples d’actions concrètes réalisables à l’aide de la plateforme “Communecter” : 
Périscolaire : outils d’informations pour découvrir et utiliser les activités périscolaires locales
Associatif : donner aux associations une vitrine et un outil de valorisation de leurs actions. Utiliser un outil de cartographie des compétence au sein d’un groupe
Economie : référencement des entreprises et des compétences locales. La valorisation des circuits courts de distribution favorise l'économie locale.
Territoire : cartographie des compétences, des ressources, des projets, des acteurs d’un territoire, d’un groupe, d’une association ou d’une entreprise
Tourisme : les habitants d’une commune sont ceux qui la connaissent et la valorisent le mieux 
Déchets : lutte contre le gaspillage, valorisation des déchets, la maîtrise du volume de déchets...
Culture : partage d'information sur l'animation du territoire, création d'événements, capitalisation d'événements, échange de services (ex cours de guitare contre des cours de chant)...
Social : transfert de compétence, de savoir-faire...
Emploi : identification des manques d’offre de services pour favoriser des initiatives pouvant déboucher sur  des d'emplois et d'entreprises...
Transport : facilitateur de  covoiturage, changements de comportements (télétravail, conduite plus souple, alternative moins polluantes etc…), débats sur les transports et recueil des besoins des administrés, projets Pédibus (accompagnement de groupe d’enfants à pied à l’école)...
Logement : service d'annonces interactives, recueil des besoins des administrés, interactions avec les collectivités locales, débats citoyens agrégés...
Énergie : sensibilisation à la dépense énergétique, consommation produits HQE, partage d'information, débats sur la consommation plus durable / biens énergivores et non durables…
Agriculture : mise en évidence des producteurs locaux, potager à la maison, création de projets de maraîchages collectifs / jardin participatif, de production alimentaire pour réduire le gachis...
Alimentation : liens étroits avec les associations locales (permaculture…), créations de projets de fermes pédagogiques

# III. Pourquoi souhaitons nous réaliser “Communecter” ?
### a. Contexte

Nous sommes dans une époque charnière où le moment est venu de proposer au citoyen une alternative pour s'investir et participer  plus activement dans la vie locale. Une évolution qui passe par une adoption plus massive des avantages que propose la technologie actuelle. 
La participation des habitants dans l'élaboration et la mise en place des projets de territoire sont des facteurs déterminants dans l'efficacité des politiques publiques.

### b. Besoins

Il apparaît important et urgent de redéfinir la place du citoyen dans la société, des besoins qui sont remontés au travers de l’étude de marché réalisée pour le projet. Ces besoins expriment un désir de coopération des citoyens pour améliorer leur qualité de vie, leur bien-être, la société au sein de laquelle ils vivent. En parallèle ils expriment un manque d’interaction avec les différentes parties prenantes de la vie publique. Les citoyens souhaitent désormais que chaque acteur puisse faire partie d’une concertation globale afin de permettre plus d’efficience dans les démarches entreprises pour améliorer leur environnement quotidien.
 
### c. Pertinence

Ce type de projet est pertinent car
la nécessité d’une cohésion sociale et d’un changement de paradigme est un sujet d’actualité : http://www.lemonde.fr/idees/article/2013/06/03/inventons-une-cyberdemocratie-pour-accompagner-la-civilisation-du-numerique_3423259_3232.html
l’évolution et l’innovation sont en marche : http://www.internetactu.net/2008/02/28/villes-20-la-ville-comme-plate-forme-dinnovations-ouvertes/
le projet répond  aux enjeux et aux besoins  exprimés par les citoyens
le projet est en cohérence avec les politiques publiques actuelles :  http://www.ville.gouv.fr/IMG/pdf/dp-la-participation-des-citoyens-au-coeur-de-la-politique-de-la-ville.pdf) 
 
### d. Dans le cadre de projets d'intérêt général, il est plus vertueux de collaborer que de se concurrencer

Il existe certes de nombreuses initiatives positives, tous domaines confondus, menées par des passionnés, des associations, des entreprises, et des administrations. Ces actions positives contribuent chacune à leur échelle à améliorer  la société mais restent cloisonnées et ne répondent que partiellement à la problématique. Il est temps de s’organiser et de se regrouper pour trouver de nouvelles orientations et solutions. A l’heure d’internet, tout est réuni pour fédérer les différentes initiatives positives mais isolées. 
Le collectif Pixel Humain apporte un changement de paradigme accompagné d'innovation technologique; le projet “Communecter” répond à la problématique  de façon systémique en s'adressant à tous les acteurs du territoire,  puis en leur offrant des avantages et bénéfices à une participation, le projet fédère ces acteurs pour devenir un véritable catalyseur d'initiatives. De plus le collectif Pixel Humain a initié une démarche collaborative avec d’autres projets citoyens  (W3C Social Working Group, Linkdata Platform, Assemblée Virtuelle, Unisson, GGouv, ChezNous, Démocratie Ouverte, OpenState, Sensorica ... ) pour créer une boite à outils commune (Toolkit Citoyen), inter opérable et développer une synergie n'ayant pas d'équivalent.

### e. Objectifs globaux du projet
 
Développement durable du territoire
Fédérer les citoyens et rendre les habitants acteurs de la vie de leur quartier, territoire
Favoriser l'interaction entre les différentes acteurs du territoire
Développer et favoriser les circuits courts de distribution 
 

# IV. “Communecter” est un projet pour qui ?

### a. Acteurs du projet

![](http://misc.nouvelle-aquitaine-fr.eu/communecter/images/acteursprojet0001.png)

### b. Bénéficiaires

![](http://misc.nouvelle-aquitaine-fr.eu/communecter/images/beneficiaires0001.png)

Le projet est ouvert à tous, sans distinction. Les bénéficiaires directs seront les acteurs du projet ; les jeunes, les adultes travaillant dans les domaines d’intérêt général, ceux au chômage, la population enclavée, les associations, les entreprises ainsi que les collectivités locales.
Sur le territoire de la Réunion, nous avons ciblé, sur 5 ans, une fourchette basse et haute de participants: entre 17 000 et 30  000 citoyens sur une population de 350 000 actifs dont environ 7000 chômeurs et 8000 personnes vivant dans les hauts de l’île. Une estimation de 500 associations sur plus de 2000 actives sur le territoire, une projection de 900 entreprises sur 34 000 présentes, devraient rejoindre la dynamique proposée par le collectif Pixel Humain. Le projet vise l’adhésion de 60% des collectivités progressivement à savoir 16 sur 24 communes, le Conseil Général et le Conseil Régional.
Si nous incluons la métropole, suivant nos prévisions, quelques 200 000 utilisateurs et plus de  
7 000 collectivités seront réunis sur la plateforme “Communecter”.
Le pro­jet aura des retombées économiques avec des dizaines d’emplois crées au sein de la structure, déclinés en une équipe dirigeante, une équipe de développeurs, des assistants administratifs, des administra­teurs de la plateforme et des commerciaux. Mais aussi des emplois induits et une baisse des coûts supportés par les collectivités sur chaque territoire communecté. 

D’un point de vue qualitatif, La population dans son ensemble sera bénéficiaire direct du projet, les résultats du projet visent à améliorer la cohésion sociale, la qualité de vie, le bien-être, la santé en proposant un développement durable du territoire. La protection de l’environnement sera  aussi indirectement bénéficiaire du projet.


# V. Moyens mis en œuvre et engagements

Pour correspondre aux valeurs et à l’éthique du projet, le projet “Communecter” est porté par une société coopérative d’intérêt collectif “Pixel Humain”,  l’outil est gratuit pour les utilisateurs, évolutif et open source.
  
Pour offrir une qualité de service à la population, l’équipe met les moyens suivants en place :
Une approche locale couplée à un projet reproductible sur différents territoires  
Un outil complet répondant aux besoins de la population
Un outil innovant, incubé à la Technopole de la Réunion
Un outil simple d’utilisation grâce à un design adapté à une cible large
Une organisation et management de projet d’entreprise
Un outil accessible également sur des applications mobile
Des partenariats stratégiques
Un projet protégé (achat de noms de domaine, design)
 
Pour atteindre une masse critique d’utilisateurs, les stratégies suivantes sont mises en place :
Inscription libre des citoyens, associations et entreprises
Propagation virale : dès qu’un seuil plancher d’utilisateurs sur une commune est atteint, nous démarchons la collectivité avec une stratégie adaptée.
Garantie de relation gagnant/gagnant en utilisant et recommandant  “Communecter”
Un plan marketing et une communication adaptés : motion, site vitrine, plaquettes, diffusion originale, actions terrain...

# VI. Partenaires

![](http://misc.nouvelle-aquitaine-fr.eu/communecter/images/partenaires0001.png)

# VII. Résultats  : voir doc Evaluation et indicateurs Pixel Humain.pdf

Le projet “Communecter” est un projet autant efficace qu’efficient. Le support choisi pour le projet est celui d’un site internet et d’applications mobiles. Sachant que la majorité de la population est titulaire d’un abonnement internet,  il est un outil précieux pour fédérer les gens,  se rencontrer,  simplifier la communication et partager de l’information. Les applications mobiles permettront des actions simples en temps réél, 24h/24 et de n’importe où. Les fonctionnalités offertes grâce à la technologie actuelle permettront la mise en réseau des utilisateurs, la valorisation du territoire, la mise en relation avec des services de proximité, la concertation et l’interaction. Le côté virtuel du projet (le Pixel) favorisera ainsi les actions, les événements et les projets qui seront entrepris sur le terrain (l’Humain).
 L’innovation technique du projet permettra un investissement faible comparé aux actions qui seront entreprises au sein du projet et aux résultats obtenus pour les participants et la population.

# VIII. Viabilité du projet

Pour garantir la pérennité du projet, l’équipe à l’origine du projet souhaite s’investir durablement dans le pro­jet et en assurer la gouvernance, garantir son éthique, son évaluation ainsi que sa montée en charge.
Pour garantir la partie financière du projet, le chiffre d’affaire, détaillé dans un Business Plan Social, sera assuré par cinq sources de revenus diversifiées :
Des prestations de services et d’accompagnement à la carte pour les collectivités adhérentes, à choisir dans un panel d’offres de service. 
Aides publiques
Des dons publics et privés défiscalisables
Investissement en parts sociales et titres participatifs
Crowdfunding
 
Le projet prévoit une montée en charge progressive des territoires communectés et du dimensionnement de son équipe. L’évolution du projet est assurée par une mutualisation des coûts partagée par les différentes parties prenantes du projet. 

Au delà de la viabilité du projet et de sa structure porteuse, le projet en lui-même démontre une valeur d’usage mais possède également une valeur indirecte (sensibilisation, cohésion sociale, civisme, interactivité communautaire, meilleure prise en compte des besoins, etc.). Tous ces atouts viennent en supplément de l’analyse financière du projet et démontrent le caractère souhaitable du projet d’un point de vue économique.
 





