![Logo de Communecter](https://framapic.org/uSICwrUabU1o/4g3pYLzOJ5JU.png)

#### ▶️ [Introduction](https://github.com/pixelhumain/co2/wiki) ◀️ 
* [ ℹ️ Accueil ℹ️ ](Home)
* [[Présentation simplifiée]]
* [[Documentation utilisateur]]
* [[Outils de communication]]
* [[Méthode basée sur l'action]]
* [[Liste des channels du chat]]

#### 🙋 [[Contribuer au projet]] 🙋
* [[Discussion]]
* [[Comment prendre des décisions ?]]

#### 🔧 Documentation technique 🔧 
* [Raspberry Pi](https://github.com/pixelhumain/co2/wiki/CoPi)
* [Importer des données](https://github.com/pixelhumain/co2/wiki/Importer-des-donn%C3%A9es)
* [Créer une carte](https://github.com/pixelhumain/co2/wiki/Network)
* [Applications et éléments](https://github.com/pixelhumain/co2/wiki/Cr%C3%A9er-une-nouvelle-application-ou-un-%C3%A9l%C3%A9ment)
* [API](https://github.com/pixelhumain/co2/wiki/Doc-de-l'API)
* [Interopérabilité](https://github.com/pixelhumain/co2/wiki/Interoperabilit%C3%A9)
* [[Specs]]

[[Archives]]