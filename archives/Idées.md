# Prise de décision
Comment une idée est retenue ? Quelle priorisation ?

## DDA
Le DDA permettrait de construire collectivement les nouvelles fonctionnalités de communecter. De mettre en ligne une idée, de discuter sur cette dernière pour faire en sorte qu'elle soit bonne et qu'on la mette dans la roadmap des développeurs.

Liste des fonctionnalités à ajouter dans le DDA : 
* Traitement de texte collaboratif où l'on peut commenter des parties de texte bien précise pour mettre des commentaires (framapad a ses fonctionnalités, à voir si l'on peut l'intégrer dans le DDA)
* Pouvoir alpaguer des communecteurs pour qu'ils donnent leur avis sur l'idée que tu as eu.

# Liste d'idées à ajouter dans communecter
* [[Le type LIEU]]