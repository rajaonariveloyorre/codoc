Les développeurs sont les personnes qui ajoute du code dans la plateforme

# Liste des développeurs

* Tibor (La Réunion)
* Tango (Nouvelle Calédonie)
* Raphael (La Réunion)
* Clément (USA)

# Actions d'un développeur

* Créer et push du code sur le github
* Faire la documentation du code source en anglais
* Mettre à jour la page wiki des développeurs

# Nouveau développeur sur communecter ?

Voici la [[Documentation pour les nouveaux développeurs]]