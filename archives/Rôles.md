# Pourquoi utiliser des rôles ?

Dans communecter on fonctionne par rôle, chaque action que l'on va effectuer correspond à un rôle bien précis. Le but est de catégoriser les actions pour savoir dans quel espace du wiki vous pouvez [documenter votre action](https://github.com/pixelhumain/co2/wiki/Documenter-son-action) et ainsi améliorer l'expérience de toutes les personnes ayant le même rôle que vous.

# Liste des rôles

* [[Ambassadeur]] 😄 : Leur but est de sensibiliser les personnes sur leur territoire à l'utilisation de communecter.
* [[Architecte]] 📜 : Leur but est de faire en sorte que la structure du commun répond à son objectif.
* [[Créateurice de liens]] 🔗 : Leur but est d'amener de nouve-aux/lles contributeur/ices au sein du commun.
* [[Développeur]] 💻 : Leur but est d'ajouter du code dans la plateforme.
* [[Glaneur]] 💸 : Leur but est de trouver de l'argent pour pouvoir payer les contributeurs.
* [[Inventeur]] 🎓 : Leur but est de trouver de nouvelles idées pour améliorer la plateforme.
* [[Messager]] ✉️ : Leur but est de faire de la communication autour de communecter sur les réseaux sociaux.
* [[Scribouilleur]] ✍️ : Leur but est de référencer les organisations, projets et événements sur leur territoire.

# Créer un rôle 🖌 

Pour créer un nouveau rôle dans ce wiki, soyez sûr qu'il ne soit pas redondant avec les rôles déjà exsitant. Utilisez le modèle juste en dessous pour créer votre page. Merci !

## Modèle pour une page wiki 🎨 

[[Modèle:rôle]]