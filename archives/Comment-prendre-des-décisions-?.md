[EN COURS D'ECRITURE]

Les règles du commun "Communecter" en terme de gouvernance sont faites pour faciliter la prise d'initiative personnel, si vous pensez que ces règles ne sont pas adaptées à cet objectif, vous pouvez les remettre en question en commençant par en parler sur le chat dans le canal [#co_gouvernance](https://chat.lescommuns.org/channel/co_gouvernance).

Si vous ne savez pas ce qu'est une action au sein de Communecter, veuillez lire cette page avant : [[Méthode basée sur l'action]]

# Quand doit-on prendre une décision ?

En général dans la vie, on prend des décisions absolument tout le temps. Pour chaque action que l'on effectue, on a pris la décision d'effectuer cette action que ce soit conscient ou inconscient. Au sein d'un projet collectif, quand on parle de prise de décision, c'est pour déterminer si l'on a le droit de faire telle ou telle action.

Au sein de communecter, il n'y pas de réelles prise de décision collective. Il y a des personnes qui veulent faire des actions et d'autres qui les conseillent, ou qui les aident en contribuant à leur action. La prise de décision est avant tout personnelle : Ai-je envie de donner de mon temps pour faire cette action ?

# Règles pour la prise de décision

Néanmoins, il existe quelques règles qui permettent de transformer l'initiative personnelle en moteur de l'action collective.

## Documentation

Quand vous avez une idée d'une action, il est bon de la documenter. Cela permet de mettre des mots sur une idée, ce qui permet d'y voir plus clair et de savoir comment on va la mettre en place, mais aussi et surtout de la partager avec le reste du commun, voire dans un second temps de la propager à travers le monde. Voici comment [[Documenter une action]]

## Communiquer sur votre idée

Maintenant que votre action est bien claire pour tout le monde, vous pouvez communiquer dessus

### Chat

Un petit message sur le chat dans le ou les channel(s) qui sont en rapport avec votre action. Ce qui permettra aux personnes de pouvoir lire ce que vous avez écrit dans le wiki, et de prendre part à votre action. Comme ces personnes ont la possibilité de lire votre action voici ce que cela permet :

* Que ces personnes vous aident
* Qu'elles vous conseillent sur la manière dont vous pourriez faire votre action
* Qu'elles ne fassent pas la même choses elles aussi dans leur coin
* Qu'elles réutilisent vos travaux

## Si peu de monde est concerné par cette action

Si l'action que l'on veut faire impact une ou plusieurs personnes dans le projet, il faut les consulter avant de faire cette action. Pour ce faire, vous pouvez leur demander leur avis sur le [chat](https://chat.lescommuns.org/channel/communecter_accueil). Le mieux étant de les mettre au courant via l'un des channels spécialisés : [[Liste de tous les channels]]. 

## Si beaucoup de monde est concerné par cette action

A partir d'un certain nombre, il sera préférable de mettre son action sur le [loomio de communecter](https://www.loomio.org/g/i4Pf57pG/communecter); en créant soit une nouvelle discussion, soit une nouvelle décision, en fonction de vos besoins.

Le lien loomio sera alors mis dans le canal associé et épinglé le temps de la prise de décision.

## Si l'action ne concerne personne

Vous êtes alors libre d'effectuer l'action sans en parler à personne. Il faudra néanmoins toujours la renseigner sur le wiki. Et en parler aux personnes potentiellement intéressées pour deux choses : 
* Que ces personnes vous aident
* Que ces personnes soient au courant de ce que vous faites. Ce qui permet deux choses :
* * Qu'elles ne fassent pas la même choses elles aussi dans leur coin
* * Qu'elles réutilisent vos travaux

## Exemple

J'ai envie d'ajouter du contenu pour améliorer la plateforme. Prenons l'exemple d'un contenu complexe qui mérite une documentation détaillée (en fonction de la complexité du contenu, vous n'aurez pas à suivre toutes les étapes ci dessous).

Au lieu de partir seul dans la création de ce nouveau contenu et ensuite push le résultat directement. Voici ce qu'il convient de faire : 
1. Créer une nouvelle page dans le wiki
2. Écrire précisément ce que vous avez en tête
3. Mettre le lien dans la page des [Idées]
4. Parler de votre idée et mettre le lien dans la page [Issues](https://github.com/pixelhumain/co2/issues)
5. Parlez en sur le chat sur [le canal #co_amelioration](https://chat.lescommuns.org/channel/co_amelioration) car c'est le canal adapté pour parler des améliorations concernant la plateforme

Si on a les droits, on peut ajouter le contenu que l'on souhaite, l'objectif étant de renforcer la documentation sur communecter. Ensuite il est bon puis on épinglera ce lien comme ça chacun peut retrouver la prise de décision facilement. Si la prise de décision concerne tout le monde, on mettra le lien dans le canal #communecter_accueil, etc...