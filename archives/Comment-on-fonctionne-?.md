Organisation interne du commun Communecter.
[Source] Ceci est inspiré de [l'oganisation](https://github.com/MonnaieLibreLille/organisation/wiki) interne du groupe de la monnaie libre sur Lille.

Voici les pages disponibles qui apporteront chacune un contenu bien spécifique pour consolider l'organisation interne du projet :
- [[Méthode basée sur l'action]]
- [[Charte]]
- [[Contenu]]
- [[Prise de décision]]
- [[Développement logiciel]]
- [[Discussion]]
- [[Outils]]

## Schéma
Ceci est une ébauche de schéma pour comprendre rapidement comment est structuré le commun : https://app.mindmup.com/map/_free/0033c9d0f3ab11e6906b390795ff6858