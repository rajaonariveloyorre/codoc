*Page parente : [[Rôles]]*

"WHY et WHAT du rôle : expliquer le plus brièvement possible pourquoi ce rôle est important et quel est son objectif principal"

* [Liste des NomRôle](https://github.com/pixelhumain/co2/wiki/NomRôle#liste-des-NomRôles)
* [Missions spécifiques](https://github.com/pixelhumain/co2/wiki/NomRôle#missions-spécifiques)
* * [NomMission](https://github.com/pixelhumain/co2/wiki/NomRôle#NomMission)
* * [NomMission](https://github.com/pixelhumain/co2/wiki/NomRôle#NomMission)
* [Liste des channels](https://github.com/pixelhumain/co2/wiki/NomRôle#liste-des-channels-utiles)

# Liste des NomRôles
*N'hésitez pas à vous ajouter*

* 
* 

# Missions spécifiques

Si vous faites ce genre de choses, c'est que vous êtes un-e NomRôle :

## NomMission

### WHY

### WHAT

### HOW

### Ressources utilisées

### Ressources créées ou à créer


# Liste des channels utiles

[#co_](https://chat.lescommuns.org/channel/co_)

# Pages liées

* [[NomPage]]
