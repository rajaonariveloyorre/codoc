Sur le schéma suivant, on peut voir une vue d'enssemble de tout le chantier sur l'intéropérabilité de Communecter avec divers sources.

![Schéma explicatif de l'intéropérabilité de Communecter avec diverses sources](https://github.com/GrondinDam/co2/blob/copedia/docs/interop_schema.png)

A gauche on à la liste des sources extérieurs sur lesquelles on récupère les données : 
* Wikidata
* Wikipédia 
* OpenStreetMap
* OpenDataSoft (la base SIRENE)
* Data.gouv
* Datanova (les enseignes La Poste)
* Pôle Emploi
* SCANR

Au milieu, le processus de Convertion des données (détails sur la prochaine figure)

A droite, l'affichage des données converties sur le site de Communecter ainsi que des exemple d'usage deces données par des sites exterieurs.

Le schéma suivant décrit en détail la convertion des données sémantiques.

![Détail dela convertion Sémantique](https://github.com/GrondinDam/co2/blob/copedia/docs/conversion_semantique.png)


# We interoperate with 

## using their API 

### Wikidata 
* For any city, We retreive main information available on Wikidata
* We'll want to contributing back any extra data we can offer(coming soon) 

### DBpedia
* For any city, We retreive main information available on Wikipedia 

## Smart Citizen (coming soon)
* onclick : we'll show all SCK kits for a given city

## Umaps (coming soon)
* POI's of type geoJson, on click we show the content on our map  

## WordPress RSS (coming soon)
*any WP blog's RSS can be pluggued to an elements wall 

## using an iframe

## FramaPads
* users can use Framapads from inside CO(simple Iframe)