Chez les créateurs de lien de communecter, on aimerait bien créer du lien avec les projets similaires à communecter, pour voir s'il est possible de mutualiser les énergies et de travailler tou-te-s ensemble.

# Pour la mission sur les outils pour gérer son entité

Communecter veut ajouter une nouvelle fonctionnalité : pouvoir gérer son entité au sein de la plateforme. Voici une liste de projets similaires et open source avec lesquels on pourrait co-construire.

## Ressources utilisées

* [Top 10 applis open source de 2014](https://opensource.com/business/14/12/top-10-open-source-projects-2014)
* [Top 10 applis open source de 2016](https://opensource.com/article/16/12/yearbook-top-10-open-source-projects)
* [Tableur google qui recense toutes les plateformes de gestions de projet](https://docs.google.com/spreadsheets/d/1776kzMsW6Yk1BTo8O7WG8jlaqnaGu0R3P4Z7fP4u0qY/edit?usp=sharing)

## Liste des plateformes

* [Taiga](https://taiga.io/) : Project management platform. Très bon site, on comprend tout de suite à quoi on a à faire.
* [MyCollab](https://community.mycollab.com/) : Outil de relation entre clients et gestionnaires de projets.
* NextCloud
* ExoPlatform
* Only Office

## Liste des applis retenues

On peut aussi co-construire avec les applications qui auront été retenues pour faire partie de la plateforme. Voir la liste sur la page [[Inventeurs : Outils pour gérer ses communs]]

# Pour la partie carto

D'autres acteurices du web font aussi dans la carto participative : 

* [Transformap](https://riot.allmende.io/#/room/#transformaps:matrix.allmende.io)