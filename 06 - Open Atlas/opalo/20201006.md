### OPALO 10/06

## ordre du jour 

### [x] penser à organiser l'apero de Flo

### [x] organisation d'équipe 
    - prise de contact avec Hassina (prof principale de Nico et Anatole ) pour creer un tiers lieux numérique et augmenter nos ressources
    - fin du stage fin juillet de Laurent : quid de la suite ? motivé à continuer et dans quellles conditions 
        - piste pour médiawiki 
        - missions à fréquences variables
        - nouveau modèle à créer à travers la Scic - le réunion du 9 juillet sur la scic va permettre de co-construire le fonctionnement 
        - plus de ressource à partir de fin juin - organiser l'enchènement - pousser le projet Fabmob et soliciter Ademe pour signer un budget associé sur du médiawiki
        - interoperabilité : en perspective 4 chantiers différents en interne - on doit flécher nos budgets et décider du budget de ta première mission
            - dans le cadre du stage et sous la supervision du maitre de stage : faire un sprint pour discuter de ce que c'est qu'être indépendant et comment vont fonctionner les rémunérations et refléchir ensemble là-dessus
    - fin de stage pour Anatole et Nico vers le 10 juillet - objectif est d'essayer de vous positionner sur des chantiers et voir aussi comment vous pouvez en ramener. Voir le statut de Freelance à Madagascar

### [x] point SCIC 
    - @karine où en est le dossier déposé à la préfecture 
    - contact benjamin et Marie pour voir si ya des choses à faire ? 
    - [seminaire SCIC](https://docs.google.com/document/d/15tKZiP459KNjVwNo8g0QLtq_IxoYjRAUcslnKkw9iC4/edit#) ( @Flo @Bea @Pierre ) 9 juillet 
        - **SESSION** orga mer 17 juin 2020 (?) 
        - lieu ? Pier Lieux ou PointoSel 
        - faire une session pour organiser la structre
        - les conditions de fonctionnement TODO
        - les sociétaires [wekan](https://)

### [x] point RGPD 
    - attendre le passage en SCIC pour poser les CGU et les RGPD 
    - officer RGPD - doit avoir un data officer - à voir - avec fiche de poste ou autre à voir - quelqu'un référent auprès de la structure avec un prestataire externe - mission strururelle de la structure 
    - Anticiper se poser des questions avant peut être structurant pour la SCIC

### - [x] formation au Custom
    - programmer et réaliser une session de formation a la mise en oeuvre de Custom 
    - formation ouverte en interne et réseau de proximité
    - faible niveau de technicité en prérequis (html de base)
    - la plateforme va beacoup évoluer donc plutot programmer à partir de septembre
    - installation perso pour @pierre @tibor
### usage d'OCECO en interne http://oce.co.tools
### scrums organiser : récurent en plus de la dev plus orienté orga
    - fréquence d'opalo toute 2semaines mercredi 13h RE
    - presence tjrs d'un core dev

### Cheque Numérique 
    - **SESSION** (Florent, Pierre, Bea, Jerome)
    - lettre type pour beneficier des enveloppes "cheque numerique" (Florent, Pierre, Bea, Jerome)
    - **TODO** inscrire Open Atlas : Aptic **@Karine @flo**
    - offres générique 
        - autres support
        - organiser les ressources de la charge 
### vision produit : COSTUM , COform, CObservatoire, OCECO
    - **SESSION** (@Tib @Bea @jerome @florent @Pierre)
    - [PPT CO.OP.TOOLS](https://docs.google.com/presentation/d/1sgDu7mya0yq9PgDOj0KuKEFc77j3aevuuuyzO7qSLZE/edit#slide=id.g87ac77bfdb_0_0)
    - ![](https://codimd.communecter.org/uploads/upload_1cb9e25559c2fbffc410d83c7e6f5b6a.png)



