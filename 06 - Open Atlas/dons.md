# Campagne de don
## Protocole nouveau donateur
1. Ajouter dans le groupe Pixel Humain avec le rôle “Donateur” et Coop
2. Ajout à la newsletter dans la liste “Donateur”
3. Envoi du mail de remerciement aux nouveaux donateurs
4. Ping sur la messagerie de Pixel Humain
