# Outils pratiques

si votreCheminVersCO = /var/www/communecter
est le chemin vers le dossier contenant pixelhumain et modules 

## LIGNES DE COMMANDES

### Créer un racourci de travail
alors à la racine 
```
cd
ln -s cheminVers d
```

### Vous pourrez alors faire des chose comme 

#### Mettre tout les repository à jour d'un coup
```
./d/pixelhumain/scripts/gitPull.sh
```

#### Faire un checkout de tout en une ligne
```
pour tout mettre sur DEV 
./d/pixelhumain/scripts/gitCheckout.sh development

ou tout mettre sur QA
./d/pixelhumain/scripts/gitCheckout.sh qa
```

## NAVIGATEURS

### TRAVAILLER , Rafraichir F5
dans CO, on utilise du JS et on recharge que la partie active grace au hash de l'url 
Donc plutot que de tjrs recharger toute la page , dans la console (Ctrl+Shift+C) vous pouvez executer 
```
urlCtrl.loadByHash(location.hash)
```

#### Créer un racourci RELOAD
- Afficher la Barre des favoris, click droit en haut du nav
- Afficher barre des favoris
- dessus click droit ajouter une page 
    + nom : RELOAD
    + url : javascript:urlCtrl.loadByHash(location.hash)

### Vider le cache 
```
sudo rm -Rf ~/d/pixelhumain/ph/protected/runtime/cache/*
```

### DEUGGUER
- Quand ca marche pas, verifier toujours la **CONSOLE** et **NETWORK**
- suivez les pistes des erreurs
- vous pourriez aussi verfier dans ~/d/pixelhumain/ph/protected/runtime/application.log 
- vous pourriez aussi verfier dans vos logs apache (chez moi /var/log/apache2/error/log)