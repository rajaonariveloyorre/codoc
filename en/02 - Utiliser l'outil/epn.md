# Communecter son Espace Public Numérique

Pour les EPN COmmunecter peut être vu comme un réseau de médiateur⋅ice⋅s du numérique, un réseau local au service de vos usagers, et un support de vulgarisation pour des concepts tels que le logiciel libre, l'open data ou les réseaux sociaux.

## Quelques conseils pour le déploiement
-   S'approprier l'outil avec les médiateurs numérique locaux en référençant son EPN en tant qu'organisation et en complétant son profil
-   Faites une [cartopartie](/5 - Déployer/Tutoriels et ressources/cartopartie.md)
-   Invitez un [ambassadeur](/5 - Déployer/Lancer une dynamique locale/ambassadeur.md) ([communauté](https://www.communecter.org/#@coambassadeurs.view.directory.dir.members))
-   Faire voter les futurs ateliers grâce à l'e[space collaboratif](/2 - Utiliser l'outil/Fonctionnalités/espaceco.md)

## Ressources
-   [Guide ambassadeur](/codoc/5 - Déployer) : pleins de conseils pour lancer une dynamique locale
-   [Plaquette de présentation](https://cloud.co.tools/index.php/s/Lyf3tBGi3PCmPWi)

## Communecter mon Hub France Connectée
> Pour accélérer la consolidation de l’offre de médiation numérique sur l’ensemble du territoire et mettre en cohérence les politiques publiques en matière d’inclusion numérique, la Banque des Territoires de la Caisse des Dépôts et la Mission Société Numérique s’associent pour faire émerger une dizaine de hubs territoriaux pour un numérique inclusif.
> Ces hubs ont vocation à incarner des têtes de réseau des acteurs de la médiation numérique.
> Ils fourniront un appui et des outils destinés à renforcer les actions d’inclusion et de médiation numérique.

Communecter est partie prenante du [HUB ULTRA NUMÉRIQUE](https://docs.google.com/document/d/1FPCYFvbAowb0YnDgexirSMsDG4R2kx47GUvzpjdah0s/edit?usp=sharing) (Réunion). Nous travaillons notamment sur une adaptation de communecter pour faciliter les échanges entre les acteurs de la médiation numérique réunionnaise.
