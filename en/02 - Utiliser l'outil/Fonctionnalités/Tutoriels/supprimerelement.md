Il faut obligatoirement être membre de l'organisation pour pouvoir demander la suppression.

S'il y a un administrateur il recevra une notification et pourra annuler la suppression dans un délai de 5 jours.

[![Capture-d’écran_2020-03-06_09-44-43.png](https://doc.co.tools/uploads/images/gallery/2020-03/scaled-1680-/Capture-d%E2%80%99%C3%A9cran_2020-03-06_09-44-43.png)](https://doc.co.tools/uploads/images/gallery/2020-03/Capture-d%E2%80%99%C3%A9cran_2020-03-06_09-44-43.png)
