# Messagerie de Communecter

## Présentation
Le chat permet de discuter entre membres d'une même communauté, d'échanger des fichiers et même de faire des visioconférences.

### Accès rapide dans Communecter
![](/Images/chat-icone.png)

[chat.communecter.org](/2 - Utiliser l'outil/Fonctionnalités/chat.md) permet de se connecter au chat sans passer par Communecter. Dans ce cas, utilisez les mêmes identifiants que lorsque vous vous connectez communecter.org.

### Rejoindre une discussion
Positionnez-vous sur la page de l'organisation ou du projet en question, et cliquez sur Messagerie. Si l'édition libre est désactivée, il faut obligatoirement être membre de l'organisation pour que Messagerie apparaisse.

![](/Images/chat-rejoindre.png)


## Fonctionnement
### Dans votre navigateur
![](/Images/chat-interface.png)
1.  **Préférences** générale. Vous pouvez, par exemple, choisir de trier vos discussions selon leurs activités (les non-lu en haut), paramétrer vos notifications ou changer la langue de l'interface
2.  **Outils** associés à la discussion en cours : recherche, liste des participants, liste des fichiers, etc.. Ce menu vous permet également de lancer une visioconférence
3.  **Liste de vos discussions**
4.  **Envoi de fichiers** et publication de messages audio ou vidéo

Quand une personne est concernée par votre message, mentionnez là en tapant `@[son pseudo]` à l'intérieur du message. `@all` notifie tous les partipants à la discussion.

Pour contacter quelqu'un via le chat il faut que cette personne l'ai utilisé au moins une fois.


### Application mobile (Android et iOS)
[Télécharger](https://rocket.chat/install)


#### Configuration
1.  Ouvrez l'application et cliquez sur "Se connecter à un serveur"
2.  Entrez `chat.communecter.org`
3.  Rentrez l'adresse utilisée pour vous inscrire sur communecter ainsi que le mot de passe qui vous sert à vous identifier sur communecter.org.

![](/Images/chat-mobile1.png)
![](/Images/chat-mobile2.png)
![](/Images/chat-mobile3.png)


#### Interface
![](/Images/chat-mobile-interface.png)

1.  **Configuration**. Ce menu permet d'ajouter d'autres instances Rocket.Chat comme [chat.lescommuns.org](http://chat.lescommuns.org/) ou [chat.alternatiba.eu](http://chat.alternatiba.eu/).
2.  **Trier vos discussions**. Vous pouvez les grouper par activité (les non-lu en haut) et par type (discussions privées et public).
3.  **Liste de vos discussions**. Cliquez pour discuter.


#### Désactiver les notifications pour un canal
Pour désactiver au niveau d'une discussion (même les mentions) :

![](/Images/chat-mobile-notif.png)
