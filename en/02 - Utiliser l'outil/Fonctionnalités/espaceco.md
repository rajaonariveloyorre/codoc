# Espace collaboratif

## A quoi ça sert ?
L'espace coopératif peut être considéré comme un outil de gestion de projet collaboratif, permettant de mettre en place une forme de gouvernance transparente et horizontale.

C'est un outil d'aide à la décision, qui vous permettra de prendre des décisions collectives, en concertation avec l'ensemble des membres de votre organisation. Il vous permettra également de gérer les différentes actions (tâches) à réaliser dans le cadre de votre activité, et d'attribuer ces actions à vos membres.


## Comment ça marche ?
Parce que chaque organisation est différente, vous commencerez par créer des **espaces thématiques** en fonction de vos besoins.
Chaque espace ainsi créé peut recevoir des **propositions** et des **actions**, en lien avec le thème de l'espace.
Par exemple, si vous gérez un club sportif, vous pourrez créer un espace nommé "Utilisation du budget du club", dans lequel vos adhérents pourront faire leurs propositions en lien avec ce thème.
Il est possible de **restreindre l'accès** à vos espaces : définissez des rôles dans la page `Membres` de votre organisation et remplissez cette zone à la création de votre espace.

![](/Images/espaceco-acces.jpg)


## C'est parti !
-   1 - Créer un nouvel espace
-   2 - Ajouter une proposition
-   3 - Invitez à voter

![](/Images/espaceco-rejoindre.jpg)

En partageant l'URL de votre navigateur les personnes pouvant voter arriveront directement sur la proposition.


## Processus de vote
### 1 - Proposer
Une proposition est un texte écrit par un membre de votre organisation. L'auteur définit également la durée de la procédure de vote, plus ou moins longue en fonction du besoin de réflexion collective autour du sujet proposé.

### 2 - Amender
> **L'auteur d'une proposition peut activer ou désactiver la procédure d'amendement**, selon la nécessité ou non de celle-ci. Un amendement est une modification, soumise au vote, dont le but est de corriger, compléter ou annuler tout ou une partie de la proposition en cours de délibération.

Tous les membres de votre organisation peuvent proposer des amendements aux propositions. Chaque amendement est soumis au vote. Lorsque la période d'amendement est achevée, les amendements validés par le vote sont automatiquement ajoutés à la proposition originale, et la période de vote commence.

### 3 - Voter
Chaque membre peut donner son avis en choisissant une des propositions ci-dessous.
![](/Images/espaceco-vote.png)

### Et après ?
Lorsque la période de vote est terminée, la proposition est automatiquement fermée, puis transformée en résolution. Vous pouvez retrouver l'ensemble des résolutions adoptées ou refusées de chaque espace dans la section suivante : ![](/Images/espaceco-resolution.png)

## Et les actions dans tout ça ?
Des actions peuvent être créées librement dans chaque espace. Les membres peuvent ainsi se positionner sur une ou plusieurs de ces actions pour en devenir un "référent". L'espace commentaire permet d'organiser l'action.
