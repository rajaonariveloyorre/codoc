# Annonces

## Introduction
Le module annonce permet de donner, louer et vendre des biens et des services. L'intérêt de COmmunecter est d'associer un territoire à son annonce pour constituer un véritable **réseau d'entraide local**.

L'application est accessible par le menu de gauche ou directement via cet URL : [communecter.org/#annonces](https://communecter.org/#annonces)

![](/Images/boutonannonce.png)


## Rechercher une annonce
Le menu de filtre permet d'affiner sa recherche. Selon le type d'annonce sélectionné vous accéderez à d'autres menus encore plus précis.

![](/Images/filtre.png)


## Publier une annonce
Le bouton (+) en bas à gauche permet d'accéder au formulaire de publication.

![](/Images/ajoutannonce.png)
