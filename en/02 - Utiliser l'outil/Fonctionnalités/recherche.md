# Les filtres dans Communecter

## Filtres disponibles
![](/Images/recherche-filtres.png)


## Filtrer par mot-clé, lieu et type d'élément
La recherche s'effectue de la même manière pour les autres applications (Agenda, Actualités, Annonce, etc..).
![](/Images/recherche.gif)


## Thématiques
![](/Images/recherche-thematiques.png)
