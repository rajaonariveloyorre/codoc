# HVA

Site : https://www.portailhva.org/

## Description

Le Portail HVA est le site des acteurs et initiatives de la Haute Vallée de l'Aude.

## Caractéristique du costum

- L'accueil du costum c'est la page `#agenda`
- `#agenda` ne posède pas de calendrier
- Le design des élements dans les pages de recherche sont surcharger
- Pour que les organisations apparaisent dans `#search`, ils doivent être validé par les administrateurs.
- Les événements et annonces ont forcement une organisation porteuse. Et elle apparaissent dans les pages `#annonces` et `#agenda` seulement si l'organisation porteuse est validé.
- Les pages `#agenda`, `#search`, `#annonces` ont leur propres liste de tags associer a une couleur définit dans hva.json


## Fichiers spécifiques au costum hva

```
costum
|-----data
|     |-----hva.json # paramètres du costum
|-----asset
|     |-----css
|     |     |-----hva.css #Style de hva
|     |     |-----calendar.css
|     |-----js
|     |     |-----about.js # Surcharge les events de editInPlace.js de la page about.ph de co2
|     |     |-----admin.js # Gestions des vue de l'espace admin
|     |     |-----hva.js # Init JS de HVA, surcharge notament le directory
|     |     |-----pageProfil.js # Surcharge pageProfil
|     |     |-----calendar.js
|     |-----images
|     |     |-----banner.jpg
|     |     |-----logo.jpg
|-----models
|     |-----Hva.php # Class du costum HVA
|-----views
|     |-----contact.php # surcharge la vue about.php de co2
|     |-----banner.php # OLD
|     |-----filters.php # Filtre des pages recherches
|     |-----footer.php # 
|     |-----home.php # Page d'accueil du costum
|     |-----importorga.php # Vue des imports (OLD)
|     |-----importsiret.php # Vue des imports (OLD)
|     |-----map.php # Page qui affiche la carto

```

# Amélioration possible

- Revoir la gestion des tags dans les pages de recherches
- Bouton calendrier à cote du bouton de la carte, qui afficherait une version miniature du calendrier
- 