https://codimd.communecter.org/xNgV-tkfTU-NkPmZXJKpvw#

# sitemap CTE
[Admin]() 

## Apps :menu:
### Home :static:



### Annuaire territoires :directory:
:::spoiler
#### page CTER :project: 
- **actions**
  * Déposer un projet
- **menu**
  * [ Home ](http://127.0.0.1/costum/co/index/slug/ctenat#@SigneLe9Avril2019.view.home)
  * [ Communauté ](http://127.0.0.1/costum/co/index/slug/ctenat#@SigneLe9Avril2019.view.community)
  * [ Gallerie ](http://127.0.0.1/costum/co/index/slug/ctenat#@SigneLe9Avril2019.view.gallery.dir.file)
  * [ Actions ](http://127.0.0.1/costum/co/index/slug/ctenat#@SigneLe9Avril2019.view.projects.dir.projects)
  * [ Agenda ](http://127.0.0.1/costum/co/index/slug/ctenat#@SigneLe9Avril2019.view.directory.dir.events)
  * [ Journal ](http://127.0.0.1/costum/co/index/slug/ctenat#@SigneLe9Avril2019.view.newspaper)
  * [ Admin ](http://127.0.0.1/costum/co/index/slug/ctenat#@SigneLe9Avril2019.view.adminCter)
    - **actions**
      + activer appel à projet (.activeAAP)
      + déposer un action (.btn-cter-create)
      + Ajouter une organisation (.btn-add-organization)
      + Ajouter une orientation (.btn-add-badge)
      + Créer un nouveau canal de chat (.btnNavAdmin data-view="newRocketChat")
  - **menu**
    + [ Gestion des Actions ](http://127.0.0.1/costum/co/index/slug/ctenat#@SigneLe9Avril2019.view.adminCter.subview.candidatures)
    + [ Membres ](http://127.0.0.1/costum/co/index/slug/ctenat#@SigneLe9Avril2019.view.adminCter.subview.communityCter)
    + [ Gestion des orientations ](http://127.0.0.1/costum/co/index/slug/ctenat#@SigneLe9Avril2019.view.adminCter.subview.strategyCter)
:::


### Annuaire Fiches Actions

# page ficheAction :project:
## menu

### Acceuil 
:::info
:::

### Communauté
:::info
:::

### En Chiffres
:::info
:::

### Agenda
:::info
:::


### Journal
:::info
:::

### Candidature (porteur only)
:::info
#### Detail
  - Auteur
  - Structure Porteuse
  - Projet déposé
#### Caractérisation
  - Domaine Actions
  - ODD
  - Indicateurs
#### Murir
  - Calendrier 
  - Partenaire
  - Budget
  - Financement
#### Contractualiser
  - COPIL
  - CTE
  - ETAT
#### Suivre
:::

### Agenda Global #agenda
:::info
:::

### Dashboard Globale #dashboard
:::info
:::

### Live Globale #live
:::info
:::

### Forum Globale #forum
:::info
:::

### Press #press
:::info
:::
### Doc #documentation
:::info

- Parcours Utilisateur
    - [Acteurs Public](http://127.0.0.1/costum/co/index/slug/ctenat#docs.page.public.dir.costum.views.custom.ctenat.docs) 
    - [Acteurs socio Eco](http://127.0.0.1/costum/co/index/slug/ctenat#docs.page.socioeco.dir.costum.views.custom.ctenat.docs)
- [Doc détaillé](http://127.0.0.1/costum/co/index/slug/ctenat#docs.page.socioeco.dir.costum.views.custom.ctenat.docs)  
:::