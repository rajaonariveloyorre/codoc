# Phase d'Identification 
1.5mois
territoire pilote 
connecté les acteurs startégique des communautés locales
veille et identification des données existantes
Structuration et Caratéroisation Globale du contexte
Imaginer la campagne et la dynamique de communication 


# Création du Costum Adapté au Coeur Culture 
2mois
## Annuaire : 
1.5mois
approche locale et thématique
Import de données 
Creation de Formulaire Dynamique 
Représnetation en Observatoire 
    créer du lien entre acteurs et avec les habitants
    organiser l’interconnaissance et la visualiser
    Mesurer la Coopération et solidarité entre acteurs
## Dynamique Projets 
1.5mois
    Formulaire de Fiche Projet
    Meilleure visibilité des acteurs, des actions, des oeuvres, des projets 
    Mettre en avant le circuit court culturel
    Favoriser le développement de projets en commun et coopératifs 
## Tester des espaces de proposition et de gouvernance ouverte 
1.5mois
    Prise de décisions collectives
## Agenda 
1mois
Démarrage de l'agenda Agenda mutualisé d’évènements 
(ateliers de réflexion, débats, manifestation, performance, propositions artistiques)

# Formation et Utilisation de la collection d'outils logiciel libre pour les dynamiques communautaire 
1mois
Création d'espace de discussion Théamtique (en mode Chat)
Partage de documents (Cahier de doléance, manifeste, charte, pétition, étude, comptes-rendus…)
Inclusion des citoyens dans la vie culturelle
Outiller les acteurs pour le travail collectifs et collaboratifs 

