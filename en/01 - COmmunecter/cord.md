# Collectif CORD

Ce collectif informel a beaucoup contribué à l'émergence de COmmunecter.

C.O.R.D (CO & Open R&D) regroupe des acteurs du numérique (web, mobile, design, objets connectés). Elle est spécialisée dans les modes de communication innovants et défend depuis toujours l’innovation et la philosophie Open Source comme vecteurs de Collaboration Massive.

CORD regroupe une communauté de talents et d'expertises, bénéficiant de retours d’expérience éprouvés. Son objet est d'accompagner les entreprises et associations dans leur démarche d'innovation. Un clusters dédié à la recherche et au développement sous une forme décentralisé.

Nous offrons une gamme de produits et services à forte valeur ajoutée :

-   Développement de solutions numériques
-   Création Graphique (logo, direction artistique)
-   Développement de Hardware (Arduino, capteurs, imprimante 3D…)
-   Barcamp / Atelier collaboratif + créatif (Brainstorming, Serious game, Hackathon…)
-   Formations : outils collaboratifs, organisation projet, développement web, design…
-   Hébergement (hosting cloud ou dédié)
-   Animation Vidéo (design, motion...)
-   E-commerce
-   Installation et animation digitale (design intéractif)
-   Conseil et Think Tank innovation
-   Cartographie (SIG : Système d’information géographique)

CORD promeut l’intelligence collective et l’initiative citoyenne en militant pour la conjugaison de l’éthique et de l’efficacité, autour de l’économie numérique et en œuvrant dans le principe de développement soutenable, durable et solidaire.
