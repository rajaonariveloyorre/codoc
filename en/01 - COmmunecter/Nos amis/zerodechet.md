Bienvenue sur la documentation Communecter. Cet espace est dédié à la communauté Zéro Déchet, par l'intermédiaire des groupes locaux de Zero Waste France. Si vous ne connaissez pas l'une de ces organisations nous vous invitons à lire la présentation de [Communecter](https://doc.co.tools/books/1---le-projet/page/pr%C3%A9sentation "PRÉSENTATION") ou celle de [Zero waste France](https://www.zerowastefrance.org/fr/zero-waste-france).

Le canal de discussions commun est accessible ici : [co-network](https://chat.communecter.org/channel/co-network).

## Cartographie des commerçants
### Avant-propos
Le 27 juin 2017, 1800 adresses de magasins vendant des produits en vrac ont été publiées par [Zero Waste Paris](https://www.zerowasteparis.fr/) sur Communecter. Ces adresses ont été transmises notamment par les sites "Terre et Avenir" et "Zero Waste For Life".

Cette action a permis de structurer la gestion commune d'une base de données ouverte des lieux zéro déchet. Les réunir sur Communecter présente plusieurs avantages :

-   **Participation au bien commun** : toutes les données sont en open data et accessibles gratuitement via une API
-   **Gestion plus efficiente** : les groupes locaux alimentent les cartes régionales et nationales, et vice-versa
-   **Réutilsation facilitée** : n'importe qu'elle organisation peut, en autonomie, créer une carte zéro déchet localisée sur son territoire
-   **Contribution intégrée** : comme Wikipédia, Communecter est alimenté par ces utilisateur·ices
-   **Mise en réseau des acteurs** : chaque lieu zéro déchet peut, s'il le souhaite, participer à l'animation de Communecter.org (réseau social libre et sans but lucratif) en publiant son actualité, et en intéragissant avec sa communauté
-   **Coût mutualisé** : chaque amélioration de l'outil bénéficie à l'ensemble des parties prenante

Pour mieux comprendre le contexte de ce partenariat nous vous invitons à lire [cet article](http://www.les-communs-dabord.org/zerowastecommunecter/), [cette présentation](https://docs.google.com/presentation/d/1lQ6B2wR78RdQlIG8MaOV7Feor8x_6JTOlDtKAff6WcQ) ou [ce document](https://docs.google.com/document/d/1hYyK4sx7Rmn2vGpnCiGoed4GbbfM1sOurV7KpQqfB3Q/edit).

### Tags

La clé de ce projet a été d’établir un socle de mot clé commun (tag) permettant le partage de points d’intérêts aux différentes échelles.

Voici les principaux tags spécifiques au Zéro Déchet et pouvant s'associer sur chaque fiche d'organisation:

-   AccepteContenant
-   Vrac
-   100% Vrac

Les autres mots clés de description, utiles pour le partage des données entre plusieurs cartes, ont été choisies selon leur existence dans Wikipedia ou le dictionnaire universel Wikidata. [Wikidata](https://www.wikidata.org) est garant de la définition des termes et de leur éventuel traduction.

[Voir la LISTE DES TAGS DE RÉFÉRENCE des Groupes Locaux Zero Waste](https://docs.google.com/spreadsheets/d/1VDa2DKVRVdMWNsCVg3arg1TRY2XRhEe9sR6wqPyS-7s/edit#gid=0)

### Importer des données

Remplissez le [fichier d'importation spécifique au zéro déchet](https://cloud.co.tools/index.php/s/gDK6Ct7LGCfWBET) puis suivez le [tutoriel d'importation](https://doc.co.tools/books/1---le-projet/page/z%C3%A9ro-d%C3%A9chet#h_11426096541578921253642).

### Créer une carte locale Zéro Déchet

Cartes fonctionnelles : [NATIONAL](https://www.communecter.org/network/default/index?src=https://gist.githubusercontent.com/manoncuille/785a76ac4c18e50be54ef7d6b1272910/raw/moncommercantzerodechet.json), [Strasbourg](https://zds.fr/adressesfaq/carte-zero-dechet/), [Paris](http://paris.zerowastefrance.org/la-carte), [Nancy](https://www.communecter.org/network/default/index?src=https://gist.github.com/ZD-Nancy/592f55afee69418185e8ee0680949b73/raw/ZD-Nancy), [Val de Marne](https://www.communecter.org/network/default/index?src=https://gist.githubusercontent.com/Simroubriff/6990001299dd617c51c749cd41514d39/raw/be787989feec923ccc7930943186781ef6971ce6/Transition-VAL-DE-MARNE), [Lyon](https://www.communecter.org/network/default/index?src=https://gist.githubusercontent.com/Simroubriff/9eb49b0c9655d4f5e80ec71cd62d26b5/raw/7726c2658dc353543720264c45d038f883e242f7/VracLyon), [Bordeaux](http://zerowastebordeaux.fr/mon-commercant-zero-dechet/), [Nice](https://www.communecter.org/network/default/index?src=https://gist.githubusercontent.com/ZDNice/fbe4b6bc6f6ca31f5d03706964679629/raw/5dba775d6ea17e841ead984054364efe967f156a/ZD-Nice), [Bretagne Sud](https://www.communecter.org/network/default/index?src=https://gist.githubusercontent.com/zwpacarto/2add4cf0968e9c77e4f9dc64a22fef3e/raw/ecf8f41058c48cf4ae25cd28880bb940847fa4f8/ZD-Auray.json), [Haute-Vienne](https://www.communecter.org/network/default/index?src=https%3A%2F%2Fgist.github.com%2Fzerowaste87%2F25279d1c4cc8fd0cbb5f3a17ef2ebd04%2Fraw%2F0481b10ecd22a90677f4194e9c02b5e5644c5012%2FHaute-Vienne.json&fbclid=IwAR3EtTjHK1lYTn1dpYrMvjpXXPInV_zBOlcNnlPpLugeb6KKSeYIQAxYqvM)

![](http://hackmd.co.tools/uploads/upload_151e636e825d3cc3f1c140004ac1472f.png)

1.  Copiez l'exemple de configuration ci-dessous dans un nouveau [gist.github.com](http://gist.github.com/). _Inscription obligatoire mais gratuite._
2.  Modifier `title`, `shortDescription` et ajoutez votre territoire sur `searchLocalityREGION` vers la fin du code. _Vous pouvez également utiliser `searchLocalityDEPARTEMENT` ou `searchLocalityNAME` (ville)._ **[En savoir + sur les paramètres de la carte](https://doc.co.tools/books/2---utiliser-loutil/page/cr%C3%A9er-un-network "Créer un network")**
3.  Ajoutez un nom se terminant par `.json`, cliquez sur `Create public gist`, puis sur `Raw` (en haut à droite de votre code).
4.  Ouvrez un nouvel onglet avec l'URL suivante : `https://www.communecter.org/network/default/index?src=LienDuRAW`. _Ceci est votre carte, vous pouvez d'ors et déjà partager cet URL._
5.  En ajoutant le code suivant dans une page de vos site, vos utilisateurs pourront utiliser la carte sans quitter le site de votre groupe local : `<iframe src="https://www.communecter.org/network/default/index?src=LienDuRAW" width="100%" height="700px" frameborder="0"></iframe>`

Un chat d'entraide est disponible ici : [co-network](https://chat.communecter.org/channel/co-network)

N'hésitez à passer votre code dans un validateur (ex : [jsonformatter](https://jsonformatter.curiousconcept.com/)) si vous pensez avoir oublier une virgule ;).

{
    "name": "Carte Zéro Déchet",
    "mode": "server",
    "skin": {
        "logo": false,
        "title": "Carte Zéro Déchet Bordeaux",
        "shortDescription": "Carte du Groupe Local Zero Déchet Bordeaux",
        "displayScope": false,
        "docs": true,
        "displayCommunexion": true,
        "displayNotifications": false,
        "profilBar": false,
        "breadcrum": true,
        "displayButtonGridList": false,
        "class": {
            "mainContainer": false
        },
        "iconeAdd": false,
        "iconeSearchPlus": false,
        "loginTitle": "",
        "front": {
            "organization": true,
            "project": false,
            "event": false,
            "community": false,
            "dda": false,
            "live": false,
            "search": false,
            "need": false,
            "poi": true
        },

        "menu": {
            "aroundMe": false,
            "connectLink": false,
            "add": true,
            "detail": true,
            "news": true,
            "directory": true,
            "gallery": true
        },

        "open": {
            "filter": true
        }
    },

    "filter": {
        "types": false,
        "paramsFiltre": {
            "conditionBlock": "or",
            "conditionTagsInBlock": "and"
        },

        "linksTag": {
            "Je me nourris": {
                "tagParent": "Alimentation",
                "background-color": "#f5f5f5",
                "image": "Alimentation.png",
                "tags": {
                    "Boulangerie": ["Boulangerie"],
                    "Café": ["Café"],
                    "Caviste": ["Caviste"],
                    "Épicerie": ["Épicerie"],
                    "Épicerie (100% Vrac)": ["Épicerie", "100% Vrac"],
                    "Chocolaterie": ["Chocolaterie"],
                    "Fromagerie": ["Fromagerie"],
                    "Miel": ["Miel"],
                    "Restaurant": ["Restaurant"],
                    "Salon de thé": ["Salon de thé"],
                    "Thé & café (Vrac)": ["Thé & café"],
                    "Traiteur": ["Traiteur"],
                    "Accepte contenant": "Accepte contenant"
                }
            },
            
            "Je prends soin de moi et de ma maison": {
                "tagParent": "Maison",
                "background-color": "#f5f5f5",
                "image": "Logement.png",
                "tags": {
                    "Cosmétique (Vrac)": ["Cosmétique", "Vrac"],
                    "Droguerie (Vrac)": ["Droguerie","Vrac"]
                }
            },

            "Je donne j'echange et j'achète d'occasion": {
                "tagParent": "Equipement",
                "background-color": "#f5f5f5",
                "image": "Equipement.png",
                "tags": {
                    "Don": "Don",
                    "Ressourcerie": "Ressourcerie",
                    "Meubles (Occasion)": ["Meubles", "Occasion"],
                    "Électroménager (Occasion)": ["Électroménager", "Occasion"],
                    "Informatique (Occasion)": ["Informatique", "Occasion"],
                    "Jouets (Occasion)": ["Jouets", "Occasion"],
                    "Outils (Occasion)": ["Outils", "Occasion"],
                    "Puériculture (Occasion)": ["Puériculture", "Occasion"],
                    "Friperie": "Friperie",
                    "Vêtements (Occasion)": ["Vêtements", "Occasion"]
                }
            },

            "Je fais moi même ou je répare": {
                "tagParent": "Réparation",
                "background-color": "#f5f5f5",
                "image": "Reparation.png",
                "tags": {
                    "Réparation": "Réparation",
                    "Fablab": "Fablab",
                    "Repair Café": "RepairCafé",
                    "Couturier": "Couturier"
                }
            },

            "J'emprunte": {
                "tagParent": "Surface",
                "background-color": "#f5f5f5",
                "image": "Loisir.png",
                "tags": {
                    "Bibliothèque": "Bibliothèque",
                    "Ludothèque": "Ludothèque",
                    "Médiathèque": "Médiathèque",
                    "Outillothèque": "Outillothèque"
                }
            },

            "Je m'engage": {
                "tagParent": "Surface",
                "background-color": "#f5f5f5",
                "image": "Loisir.png",
                "tags": {
                    "Association": "Association"
                }
            },

            "Je fais appel à des acteurs de l'économie solidaire": {
                "tagParent": "Surface",
                "background-color": "#f5f5f5",
                "image": "Loisir.png",
                "tags": {
                    "Déménagement": "Déménagement"
                }
            },

            "Je jardine, je composte et je recycle": {
                "tagParent": "Conteneur",
                "background-color": "#f5f5f5",
                "image": "Service.png",
                "tags": {
                    "Jardinerie": "Jardinerie",
                    "Jardins partagés": "Jardins partagés",
                    "Compost": "Compost",
                    "Emballage (recyclage)": ["Emballage", "Recyclage"],
                    "Textile (recyclage)": ["Textile", "Recyclage"],
                    "Verre (recyclage)": ["Verre", "Recyclage"]
                }
            }
        }
    },

    "add": {
        "organization": true
    },
    "result": {
        "displayImage": true,
        "displayType": true,
        "fullLocality": true,
        "datesEvent": false,
        "displayShortDescription": true
    },
    "request": {
        "sourcekey": "zeroDechet",
        "pagination": 1000,
        "searchType": [
         "organizations"
      ],
        "searchLocalityDEPARTEMENT": [
         "GIRONDE"
      ],
        "searchTag": [
        "100% Vrac",
        "Accepte contenant",
        "Alimentation",
        "Amap",
        "AMAP Culturelle",
        "Ameublement",
        "Artothèque",
        "Association",
        "Atelier de réparation de vélo",
        "Bibliothèque",
        "Bibliothèque de rue",
        "Bien-être",
        "BioDéchet",
        "Biologique",
        "Boucherie",
        "Boulangerie",
        "Café",
        "Caviste",
        "Chocolaterie",
        "Commerce",
        "Compostage",
        "Compostage collectif",
        "Conteneur",
        "Cosmétique",
        "Couturier",
        "Crèche",
        "Décheterie",
        "Déco-Bijoux",
        "DIY",
        "Déménagement",
        "Don",
        "Droguerie",
        "Electromenager",
        "Electronique",
        "Emballage",
        "Énergie",
        "Entreprise",
        "Epicerie",
        "Épicerie",
        "FabLab",
        "Friperie",
        "Fromager",
        "Givebox",
        "Glacier",
        "Grainothèque",
        "Groupe",
        "Groupement d'achat",
        "Groupement de producteur",
        "Habitat",
        "Incroyables comestibles",
        "Informatique",
        "Institution",
        "Jardinerie",
        "Jardins partagés",
        "Jouets",
        "LaRuche qui dit oui",
        "Librairie",
        "Location",
        "Ludothèque",
        "Magasin de producteurs",
        "Marché",
        "MCMD",
        "Médiathèque",
        "Miel",
        "Mutualisation de matériels",
        "Occasion",
        "Organisation gouvernemental",
        "Outillothèque",
        "Outils",
        "Papier",
        "Patisserie",
        "Pile",
        "Poissonnerie",
        "Poissonnier",
        "Prêt-à-porter",
        "Primeur",
        "Puériculture",
        "Recyclage",
        "Recyclerie",
        "Recyclerie",
        "Repair Café",
        "Réparation",
        "Ressourcerie",
        "Restaurant",
        "Salon de thé",
        "Source d'eau potable",
        "Textiles",
        "Thé",
        "Traiteur",
        "Troc",
        "Troc Don",
        "Vente directe",
        "Vente en vrac",
        "Verre",
        "Vrac",
        "Vêtements",
        "Zone de gratuité"
      ]
    }
}


## Foire Aux Questions

**Lorsqu'ils sont importés, au bout de combien de temps apparaissent-ils sur la carte ?** Quelques secondes suffisent. S'ils n'apparaissent pas, soit la carte est mal configurée, soit il y a un bug ([déclarer un bug](https://doc.co.tools/books/3---contribuer/page/tester-loutil "Tester l'outil")).

## Amélioration de l'outil

En savoir plus sur le [modèle économique de Communecter](https://doc.co.tools/books/1---le-projet/page/mod%C3%A8le-%C3%A9conomique "Modèle économique").

Open Atlas (association portant Communecter) peut accompagner des groupes locaux pour répondre à des appels à projets incluant une partie dédiée à la cartographie. Une réponse positive permettra d'améliorer ce bien commun pour l'ensemble des utilisatrices et utilisateurs, y compris les membres des autres groupes locaux. Les améliorations peuvent concerner par exemple :

-   Rendre l'interface plus intuitive
-   Améliorer le système de filtre
-   Simplifier la création de cartes
-   Développer l'application mobile
-   Permettre de choisir les infos apparaissant sur la carte
-   ...
-   _Les idées ne manquent pas : [améliorations proposées](https://doc.co.tools/books/3---contribuer/page/proposer-des-am%C3%A9liorations "Proposer des améliorations")_

## Cartographie des groupes locaux

Pour participer au référencement des groupes locaux sur Communecter ça se passe sur **[ce fichier](https://docs.google.com/spreadsheets/d/1OVARbzgGevQaXo7NbZZ14XLYuVZPsKHzrdFQxw-FrD0/edit?usp=sharing)**. Une fois les données essentielles complétées, une importation automatique sera effectuée et nous pourrons générer une carte des groupes locaux (comme la [framacarte actuelle](https://framacarte.org/fr/map/groupes-locaux-zero-waste_28165)).

## Réseau social Zéro Déchet

Au delà de la cartographie Communecter est un réseau social libre et éthique. La fonction [Actualité](https://www.communecter.org/#live) permet de partager de l'information locale et/ou thématisée. Pour accéder aux publications concernant les déchets : ![](https://cloud.co.tools/index.php/s/q7Se2So3xx3NCpN/preview)

Une réflexion est en cours sur la création d'un réseau social dédié au zéro déchet basé sur les fonctionnalités de communecter ([COstum](https://doc.co.tools/books/2---utiliser-loutil/page/costum "COstum")).
