English version

# Présentation de Communecter
## C'est quoi COmmunecter ?
"COmmunecter" : se connecter à sa commune, est une boîte à outils citoyenne collaborative, ouverte à tous, un **réseau sociétal innovant**, open source et libre, de développement de territoire avec une **approche locale** (quartier, arrondissement, commune, ville, ...). La plateforme offre des outils adaptés aux échanges, aux rencontres, aux débats, au montage de projets, à l'expression des besoins des uns, aux services offerts par les autres.

Une plateforme pour **connecter véritablement les acteurs d'un territoire** : habitants, associations, entreprises et collectivités.

## Philosophie de COmmunecter
Pixel Humain ne désigne plus la plateforme mais la communauté participant à ce projet.

En savoir + sur l'historique et la raison d'être du projet : [code social](codesocial.md)
<iframe src="//player.vimeo.com/video/73771864?title=0&amp;amp;byline=0" width="800" height="450" allowfullscreen="allowfullscreen"></iframe>

## Quelles sont les possibilités de COmmunecter ?
Liste complète des fonctionnalités : [documentation](/2 - Utiliser l'outil/Fonctionnalités/liste.md)

<iframe src="//player.vimeo.com/video/133636468?title=0&amp;amp;byline=0" width="800" height="450" allowfullscreen="allowfullscreen"></iframe>
