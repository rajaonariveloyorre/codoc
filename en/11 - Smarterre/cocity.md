# COCity
Un Territoire citoyen et connecté

# slogan
- dispositif Viral pour un impacte local 
- citoyens tous maire !!
- agir en commun
- une commune a t elle vraiment besoin d'un maire ?
- une commune, géré par des citoyens connectés, est ce possible ? experimentons !!!
- créons ensemble les raccourcis qui iront plus vite et plus loin
- ce que vous ne voulez plus 
- partager ce que vous aimeriez 
- coconstruire votre commune et pouvoir etre dans l'opposition 
- cocity : collaborative, connecté, consciente, connaissance 

# Slideshow , Phrase clefs
- démarrer votre cocity en 1 click 
- innovons la facon de vivre et participer au fonctionnement de nos communes 
- construisez votre commune connecté, transparente, interactive et souveraine 
- augmentez la connaissance par des questionnaire permanent 
- soutenez ou questionnez les élus sur leurs projets
- organiser les services, compétences, besoins et domaine d'expertise de votre commune 
-  connecté tout les filières entre les commune 
-  si toute COCity alors CORegion 

# features
- People 
- Mes Service 
- Mes Competence 
- Mes Ressource 
- Entraide 
  - Mes craintes, mes probelemes
  - Mes Besoins
  - Mes solutions
- J'ai une organisation Local ou un group d'interet partagé
- J'ai un Projet
- Je fais un Event  
- Coszette : Journaliste actu local 
  [ ] actu participative :form:
  [ ] regrouper les bookmarks d'une actu dans gallery bookmark
- outil d'Entraide -> Classifieds 
  [ ] production local
[ ] Creer une zone locale 
  url unique par zone.slug RE_07427_Lagune :form:
  - container activity
    organisation Local
  - creer une orga 
  - easy invite :input:
[ ] Easy Viral Invite your neighbour :btn:

* vision globale local
  [ ] tout les projets, events, orga
  [ ] tout les services et les sujets surlesquels ya deja en mvt
  [ ] présentation de la COzette 
  [ ] voir les bookmarks du groupe 

# Btns
- outil d'impacte local 
- projet pourri

# comm
  [ ] diffusion de pair à pair 
    [ ] vos amis 
    [ ] vos collègues
    [ ] les faiseurs : autour de vous ceux qui font des choses
    [ ] vos concitoyens
  [ ] coambassadeur 
  [ ] Groupe Pacte 
  [ ] France Inter , Freedom
  [ ] Partager c sympas 
  [ ] Boite à lettre locale
  [ ] Affiche 
  [ ] contacter toute les plateformes à se joindre  
    [ ] plateforme add system, request interoperability
    ensemble créer un virus sociétal
  [ ] [[https://www.facebook.com/groups/2584954295126995/permalink/2711025099186580/]]
  [ ] [[https://empreintes-citoyennes.fr/]]

# Cocity Doc == smarterre
Fiche apprenante pour faire des territoires intelligents  
Veille permanente d'actions locales et internationales
