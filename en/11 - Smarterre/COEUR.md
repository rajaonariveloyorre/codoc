# COEUR 

[PRESENTATION](https://docs.google.com/presentation/d/1NZr1DMFeS3I-JYuXrGvecijvgWW4zLaVvUyaevK-jHM/edit#slide=id.g6dd83f3ed1_0_0)

> COLLABORATION entre Organisations et Évennemnts pour UNIR les Ressources

Une dynamique au sein d'un groupe d'acteur qui un interet pour partagér leur ressources par proximité d'objectifs, de domaines d'action...
S'applique bien dans une approche sectoriel locale, national ou international.

![](https://codimd.communecter.org/uploads/upload_ffb5029934c8641993ee5552287d5bd1.png)

![](https://codimd.communecter.org/uploads/upload_82a34a1e00937fc1497d03d3ba24e7b7.png)

![](https://codimd.communecter.org/uploads/upload_47d762faaefbbb7e4a4dae1c0a53bdc2.png)

# OUTILS COEUR
- COstum (Portail personnalisé)
- COForm (Recoltte de connaissance)
- Observatoire (Visualiser la recolte)
- OCECO (Mise en action)
- Smarterre (Créer du lien avec d'autre COEUR) 

- critere d'evaluation de la commune 

# Créer un COEUR 
- un communauté (Xcel, emails, sites web)
- créer un questionnaire
- définir les types de visualisation 
    + listing 
    + carto
    + graph diagramme

# Exemple de dynamique COEUR
- [COEUR Numérique réunion](https://docs.google.com/document/d/1pALaL8QUURWV8MI7Atr5xvl1YVZKBJGF2bE3L4jn1bM/edit)
- [PPT Coeur Num](https://docs.google.com/presentation/d/1F13Wtt-f5w8hAVIyXwE8Ritq7HtoQsAQEf_qmL24q7g/edit#slide=id.p)
