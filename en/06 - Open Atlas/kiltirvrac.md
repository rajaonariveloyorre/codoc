# Kiltir Vrac
## Quelques mots sur le concept
Le Kiltirvrac vous permet :
-   de commander et d’acheter des produits dans la quantité de votre choix
-   de réduire le nombre d’emballages superflus
-   de manger des produits de meilleure qualité à des prix inférieurs

Le [Kiltir Vrac](https://www.communecter.org/#@kiltirvrac) est un relais du [Comptoir du Vrac](https://www.communecter.org/#page.type.organizations.id.5948f7c840bb4ed10687aa76) situé au 466 chemin des Badamiers, Ligne Paradis, Saint Pierre.

> Le Kiltir Lab est un espace citoyen tenu par l’association Open Atlas. Vous pouvez, entre autre, venir nous parler de votre projet ou y déposer votre ordinateur usagé. Toutes les initiatives citoyennes y sont les bienvenues.

Nous tentons d’avoir le fonctionnement le plus simple et efficace, pour vous, comme pour nous.


## Infos pratiques
-   Les produits proposés sont ceux sélectionnés par le Comptoir du Vrac. Ce sont principalement des produits secs (farines, légumineuses, pâtes, produits ménagers …), parfois de l’huile et des fruits secs.
-   En tant que groupement d’achat nous bénéficions de tarifs préférentiels par rapport à ce qui est pratiqué dans la boutique du Comptoir du Vrac. Le vrac étant de base moins cher c’est donc doublement abordable.
-   Les commandes s’effectuent **toutes les 4 semaines**.
-   Nous ne prenons aucune commission. Si vous souhaitez soutenir la démarche nous vous invitons à [adhérer à l’association](https://www.helloasso.com/associations/open-atlas/adhesions/soutenez-et-adherez-a-open-atlas) ou à nous faire un [don récurrent](https://www.helloasso.com/associations/open-atlas/collectes/communecter).
-   Nous n’obligeons rien, nous comptons sur la bienveillance de chacun pour que tout se passe pour le mieux.

## Mode d’emploi
### Commande
-   **Tous les débuts de mois, un tableau collaboratif est publié** dans [le groupe Kiltir Vrac sur Communecter](https://www.communecter.org/#@kiltirvrac) dans lequel il est possible à chacun•e d'entrer sa commande selon les produits souhaités.
-   **Le deuxième vendredi du mois, nous verrouillons le document** afin de lancer la commande auprès du Comptoir du Vrac à l'Eperon. Vous avez donc quelques jours pour **mettre votre nom en haut de la colonne et d’indiquer sur chaque ligne la quantité souhaitée (en kilo ou litre)**. Le prix total de votre commande est automatiquement calculé.
-   **Il faut compter 2 mois à peu près entre la date de la commande passée et sa réception.** Lorsque les colis arrivent au Comptoir du vrac, nous appelons à la bonne volonté de chacun pour aider Giovani à les décharger (un effectif de 2 personnes est nécessaire / ça se passe au 146 Chemin Déguigné, 97424 Saint-Leu )
-   **Les colis de notre commande groupée sont transférés au KiltirLab**, là où vous pourrez les récupérer (466 chemin des Badamiers, Ligne Paradis, Saint Pierre)

> Nous vous invitons à prendre des colis entiers (par ex : un sac de 3kg de farine) pour que ce soit plus rapide pour tout le monde même si ce n’est pas du tout obligatoire.


### Distribution
> Nous nous occupons de récupérer la commande au Comptoir du Vrac (Piton Saint-Leu) si vous voulez le faire pour nous, n’hésitez pas à vous manifester :).
-   Nous vous laissons **2 semaines** pour venir récupérer vos produits. Nous sommes ouvert du **lundi au vendredi de 8h30 à 17h**. Passé ce délai votre commande est ajoutée au stock.
-   **Il est important que vous emmeniez vos propres contenants**. Nous en mettons à disposition, ainsi qu’une balance électronique, mais en quantité limitée.
-   Il ne vous restera plus qu’à payer. De préférence le jour-même et par virement ([RIB](https://drive.google.com/open?id=0B0BMYNdshloMMHVNcWlVdThhT2s)) ou par espèces.

