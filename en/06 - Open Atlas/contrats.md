# Contrats obtenus par Open Atlas

## Année 2021
|      Date     |           Projet            |  Montant TTC (en €)   |
|:-------------:|:---------------------------:|:---------------------:|
| 11/6/2021     |  ARCS-ARCI  Projet RIES     |    3000 €             |
|               |  Asso Préfiguration Nat.    |    5500 €             |
| 6/1/2021      |  ASSOCIATION KOSASA         |    4000 €             |
| 11/6/2021     |  CEDTM  Projet Sommom       |    6000 €             |
| 25/11/2021    |  CIE des TIERS-LIEUX        |    2500 €             |
|               |  CRESS                      |    6000 €             |
|               |  DEMORUN                    |    2530,32 €          |
|               |  DRFIP REUNION              |    22800 €            |
| 11/6/2021     |  HER                        |    3200 €             |
| 11/6/2021     |  HER  Projet HER - audit    |    800 €              |
| 6/12/2021     |  HORS CADRE   Notragora     |    3081,11 €          |
| 6/12/2021     |  HORS CADRE  Notragora      |    2764,98 €          |
| 24/8/2021     |  HVA   Maintenance          |    400 €              |
|               |  LA RAFFINERIE              |    4000 €             |
|               |  LE DIVELLEC Mona           |    1500 €             |
|               |  NEREE    Meteolamer        |    4000 €             |
| 17/6/2021     |  OXALIS SCOP SA  Faben      |    5000 €             |
| 8/3/2021      |  SCBCM / MTES Héberg CTE    |    6000 €             |
| 9/7/2021      |  SCBCM / MTES Héberg CTE    |    14000 €            |
| 15/7/2021     |  SCBCM / MTES Plateforme    |    25806,45 €         |
|               |  SCBCM ECOLOGIE             |    6000 €             |
| 5/11/2021     |  SGAR   Tiers-Lieux         |    35500 €            |
|               |  SHOW-CO ARTS               |    3200 €             |
|               |  SOLIDARNUM                 |    8000 €             |
| 5/11/2021     |  TECHNOPOLE  MEIR - 30%     |    11058 €            |
| 28/12/2021    |  TECHNOPOLE  MEIR - 20%     |    7373,27 €          |
| 29/12/2021    |  TECHNOPOLE  MEIR - Vidéo   |    1400 €             |
|:---------------------------:|:-------------:|:---------------------:| 

## Année 2020
|           Projet            |      Date     |  Montant TTC (en €)   |
|:---------------------------:|:-------------:|:---------------------:|
|  CMCAS Haute Bretagne       |  juillet. 2020|  8 000                |
|  Sommom                     |  avril. 2020  |  2 500                |
|  Pacte Transition 3         |  avril. 2020  |  12 500               |
|  CTE National 3             |  juin. 2020   |  40 000               |
|  CRESS                      |  Aout 2020    |  3 500                |
|  France Tiers Lieux         |  Sept 2020    |                       |
|  Afnic                      |  Aout 2020    | 14 000                |

## Année 2019
|           Projet            |      Date     |  Montant TTC (en €)   |
|:---------------------------:|:-------------:|:---------------------:|
|  CTE National 2             |  Mai. 2019    |  100 000              |
|  Pacte pour la Transition  1|  Jan. 2019    |  7 000                |
|  CRESS                      |  Mars 2019    |  4 500                |
|:---------------------------:|:-------------:|:---------------------:|

## Année 2018
|           Projet            |      Date     |  Montant TTC (en €)   |
|:---------------------------:|:-------------:|:---------------------:|
|  Le Port                    |  Août 2018    |  14 300               |
|  Centres sociaux connectés  |  Nov. 2018    |  5 000                |
|  CTE                        |  Juill. 2018  |  24 700               |
|:---------------------------:|:-------------:|:---------------------:|

## Année 2017
|           Projet            |      Date     |  Montant TTC (en €)   |
|:---------------------------:|:-------------:|:---------------------:|
|  NotrAgora                  |  2017         |  15 000               |
|  KGougle.nc                 |  2017         |  0                    |
|  TOTAL                      |               |  70  500              |
