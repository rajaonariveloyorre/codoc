## Missions spécifiques
* Compléter les fiches des élements autour de vous en rajoutant, par exemple, l'image de profil des organisations autour de vous ou en ajoutant une description courte.
* Créer des évènements dont vous connaissez l'existence.
* Ajouter des organisations

Sur Communecter vous pouvez également être référent d'une organisation sur Communecter. Vous effectuez alors toutes les tâches décrites ci-dessus mais en vous concentrant sur l'organisation dont vous êtes référent.
Dans votre structure ce sera souvent la même personne qui s'occupe des autres réseaux sociaux (facebook, youtube, ...) mais ça peut également quelqu'un qui est extérieur à votre organisation. Une personne bien informée de votre actualité remplira parfaitement ce rôle 😃.