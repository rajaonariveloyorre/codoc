### Ressources pour la doc
* [La Ville à Vélo](http://www.cc37.org/wp-content/uploads/2015/01/cartovelocite.pdf) - Référencement des parkings vélo de Lyon
* [Article de Zoomacom](http://www.zoomacom.org/2015/06/04/cartoparties-numerique-et-developpement-culturel-des-territoires/)
* [Organiser une cartopartie](http://www.wikigarrigue.info/wiki13/wakka.php?wiki=OrgaCartoPartie) (par le collectif des garrigues)
* [Organiser une cartopartie](http://www.manchenumerique.fr/content/download/14751/212549/file/Guide%20Mapping%20Party.pdf) (par Manche Numérique)
* [Si on faisait une Cartopartie ?](http://www.dogether.fr/2015/12/30/si-on-faisait-une-cartopartie-openstreetmap/) (sur together)
* [Organiser une cartopartie](http://tiriad.org/ressources/wakka.php?wiki=Organisationcartopartie) (par Tiriad)

### Notes de la réunion du 7 juin
Pour notre première cartopartie nous avons décider de cartographier les restaurateurs de Saint-Louis.
Voici la liste des données que nous souhaitons récolter et les questions que nous voulons poser aux commerçants :
* Est-ce que les produits utilisés sont **locaux** et/ou **bio** ?
* Est-ce que vous proposez des menus **végétariens**, **vegan**, **sans gluten**, **sans porc** et/ou entièrement **cru** ?
* Peux-t-on manger **sur place** et/ou est-ce **à emporter** ?
* Est-ce que vous **triez vos déchets** ?
* Acceptez-vous les **contenants réutilisables** ?
* Si un **compost** était disponible à proximité de votre restaurant, l'utiliseriez-vous ?
* Proposez vous des **doggy bag** ?
* Utilisez-vous des **sacs plastiques** ?
* Que faites-vous du **surplus alimentaire** ?
* Quelle est votre **gamme de prix** ?
* Quelle est votre **capacité d'accueil** ?
* Avez-vous un **site internet** ? Êtes-vous sur **Facebook** ?
* Quels sont vos **horaires** ?
* Peux-t-on **réserver** ?
* Quels sont vos coordonnées (**téléphone**, **adresse**) ?

### Prochaine cartopartie (15 juin)
#### Déroulement
Avant de partir il faudra tester l'ajout d'une organisation sur Communecter via une tablette et un téléphone.
Vu la thématique (restaurateurs) nous avons choisit les horaires suivantes : 10h30 - 12h / Pause + Mini bilan / 13h30 - 14h30.
Nous présenterons Communecter avec l'approche "annuaire collaboratif" : "on fait un annuaire collaboratif ça nous intéresse d'avoir quelques info sur votre restaurant".
Nous remplirons la fiche organisation dans Communecter directement avec le commerçant grâce à une tablette et/ou un smartphone.
Si des points nous semble améliorable nous proposerons de donner leur contact à des assos travaillant sur cette thématique (zéro waste, ekopratik, incroyable commestible, ...) : “on connaît des associations travaillant sur cette thématique souhaitez-vous qu'ils vous contact ?".

#### Traitement des données
Grâce aux données récoltées nous pourrons attribuer une note (de 1 à 5) sur des thèmes précis (déchet, fournisseur, ...).