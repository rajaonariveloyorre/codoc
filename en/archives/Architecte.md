*Page parente : [[Rôles]]*

Leur but est de faire en sorte que la structure du commun répond à son objectif. En clair, c'est elleux qui gère toute la partie organisation interne du commun et font en sorte que tout se passe bien.

# Liste des architectes

*N'hésitez pas à vous ajouter*

* Draft (World)

# Missions spécifiques des architectes

Si vous faites ce genre de choses, c'est que vous êtes un-e architecte : 

## Créer/Mettre à jour les règles du commun

### WHY ?

Faire en sorte que la communauté atteigne plus facilement l'objectif du commun

### WHAT ?

Améliorer le système derrière le commun.

### HOW ?

* Créer/Mettre à jour les règles sur le fonctionnement du commun (gouvernance, remettre en question les règles, en créer de nouvelles, etc...).
* S'informer sur les dernières pratiques en terme d'organisation horizontale et de méthodes basées sur l'action.

### Ressources utilisées

* [Méthode SKA](https://github.com/b3j0f/ska/wiki) : Une méthode basée sur l'action
* [Reinventing organization : Vidéo en français](https://www.youtube.com/watch?v=NZKqPoQiaDE)
* Le channel [#co_architect](https://chat.lescommuns.org/channel/co_architects) : Channel des pixels actifs architectes.

### Ressources créées ou à créer

* [[Comment on fonctionne ?]]
* [[Comment prendre des décisions ?]]
* [[Remettre en question les règles]]
* [[Écrire de nouvelles règles]]

## Créer du lien entre tous les Pixels Actifs

### WHY

Pour renforcer l'esprit fraternelle de la communauté et créer une bonne atmosphère de travail

### WHAT

Faire en sorte que l'information circule bien entre les différents groupes de rôles de communecter. Que tout le monde puisse avoir l'information qu'yel souhaite à propos du commun.

### HOW

* Créer et améliorer les outils numériques utiles pour gérer le commun.
* Définir une méthodo pour que tout le monde puisse accéder à l'information qu'yel cherche à propos du commun (aussi appelé holoptisme).
* Trouver des manières intéressantes de gérer les conflits.

### Ressources utilisés

* [#co_holoptisme](https://chat.lescommuns.org/channel/co_holoptisme)

### Ressources à créer

* [[Outils numériques]]
* [[Gérer les conflits]]

Faire en sorte que l'information circule bien entre les différents groupes de rôles de communecter.
* Créer et améliorer les outils numériques utiles pour gérer le commun.
* Garantir le fait que tout le monde puisse accéder à l'information qu'ielle cherche à propos du commun (aussi appelé holoptisme)

# Liste des channels utiles

[#co_contribution](https://chat.lescommuns.org/channel/co_contribution) : Channel des contributeur/ices à Communecter (Communecteurices).
[#co_doc_wiki](https://chat.lescommuns.org/channel/co_doc_wiki) : Channel en rapport avec la documentation du wiki
[#co_gouvernance](https://chat.lescommuns.org/channel/co_gouvernance) : Channel où l'on discute de la gouvernance au sein du commun.
[#co_holoptisme](https://chat.lescommuns.org/channel/co_holoptisme) : Channel où l'on discute de comment améliorer le fait de savoir tout ce qui se passe dans le commun.