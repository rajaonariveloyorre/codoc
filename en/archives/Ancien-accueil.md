Bienvenue sur le wiki de Communecter

Les pages suivantes sont maintenues par l'équipe Documentation de Communecter

Nous joindre par [tchat](https://chat.lescommuns.org/channel/communecter_accueil)

# Glossaire

* **Communecter** : réseau social citoyen qui est le coeur du projet

* **Commun'Event** : applicaton web (pour Android) qui permet d'accéder aux événements de Communecter sur smartphone

* **Network** : carte thématique basée sur une sélection de données dans Communecter
[En savoir plus](https://github.com/pixelhumain/communecter/wiki/Network)

* **Open Atlas** : association (basée à la réunion) qui porte juridiquement le projet

* **Pixel Humain** : collectif de développeurs qui code les outils autour

* **CoPi** : Communecter sur Raspery Pi : Autonome, en locale, intéroperable [En savoir plus](https://github.com/pixelhumain/communecter/wiki/CoPi)
