*Page parente : [[Rôles]]*

Leur but est de trouver de l'argent pour pouvoir payer les communecteur/ices. En clair, ils font un travail de prospection auprès des entités possédant de l'argent pour soutenir la plateforme dans son évolution. Cela peut être des particuliers, des collectivités locales, des services publiques, des fondations, des associations, des entreprises, ... La [[Rétribution du glaneur]] se fait en fonction de sa contribution, basé sur un pourcentage.

# Liste des glaneur-se-s

*Hésitez pas à vous ajouter*

# Actions spécifiques des glaneur-se-s

Quand on fait ça, c'est qu'on est un-e glaneur-se : 

* Aller demander des sous sous à une entité
* Créer une campagne pour récolter des fonds en alliance avec les messagers
* Consulter régulièrement les appels à projets ([Réunion](https://papaly.com/categories/share?id=48ac40175de3407f9d1d6adcc34074ca))

# Liens

* [Channel #co_subventions](https://chat.lescommuns.org/channel/co_subventions)
* [Modèle économique de Communecter](https://docs.google.com/document/d/1k1XmVgh4e5vKQozFxxEzYBL_0uWISbJCX9m0cZUhvp4/edit#heading=h.3qra5tfu8dzp)