# Présentation rapide

Bien le bonjour à vous visiteur,

Vous êtes sur le wiki de communecter. Communecter est une plateforme interactive qui a pour objectif la consolidation de nos forces communes, afin que l'on s'inspire, que l'on s'encourage, que l'on agisse ensemble pour le bien-être de notre cité. Vous pouvez voir **[une vidéo l'introduisant](https://vimeo.com/133636468)**.

La plateforme est un bien **[commun](https://fr.wikipedia.org/wiki/Communs)** et est géré comme tel ce qui permet à n'importe qui de pouvoir **contribuer** très facilement. De plus la plateforme est aussi considérée comme un bien commun, ce qui veut dire qu'elle **appartient à tou·te·s**.

[[Nos valeurs]] sont : le **partage**, l'**équité**, la **tolérance** et la **bienveillance**. Nous apprenons par l'**erreur** et l'**expérimentation**. Aussi, nous essayons au maximum de **désamorcer** les relations basées sur une ou plusieurs dominations. C'est pourquoi nous avons basé notre façon de travailler en groupe sur la [[méthode SKA]].

# Plan du wiki

## A propos du commun

* [[Qu'y a-t-il de spécial à propos de Communecter ?]] : Communecter a ce "je ne sais quoi", unique en son genre elle se veut être une plateforme éthique et performante à la fois !
* [[Nos valeurs]] : Droit à l'erreur, partage, équité, tolérance, bienveillance, sans rapport de domination, d'accord. Mais pourquoi ?
* [[Communecter : un commun ?]] : Qu'est ce qu'un commun ? Pourquoi est-ce une manière innovante de gérer une plateforme interactive ?

## Comment utiliser Communecter ?

* [[Guide de l'utilisateur débutant]] : Entrez dans un monde de communexion avec les pixels de votre cité... Prenez 20 minutes de votre temps pour comprendre cette nouvelle plateforme qui fait de votre quartier votre sens de gravité !
* [[FAQ]] : Foire Aux Questions

## Comment contribuer ?

* [[Guide pour pixel actif débutant]] : On vous explique où et comment contribuer en moins de 15min. A lire si vous venez d'arriver par chez nous.
* [[Comment contribuer ?]] : Page agrégeant toutes les infos sur "comment contribuer au sein de ce commun".
* [[Rôles]] : Liste des rôles des pixels actifs.

## Comment on fonctionne ?

* [[Méthode basée sur l'action]]
* [[Comment prendre des décisions ?]]