# Valeurs

- **créativité et partage** : légalement protégé pour le partage de connaissance et à la créativité
- **apartisant** : la neutralité de la plateforme ne doit pas être entâchée d'autres idéologies politiques que celle de la collaboration !
- **transparence et accessibilité** : faciliter tout type de contribution
- **droit à l'erreur** : ne pas bloquer l'initiative par de la décision collective et savoir revenir en arrière si besoin

# Cadre de travail

Dans un tel contexte, le cadre doit faciliter le travail en commun de participants plus ou moins impliqués, rîches de différents points de vue et compétences.

Le cadre de travail est basé sur 
- des [méthodes agiles](https://github.com/MonnaieLibreLille/organisation/wiki/charte#m%C3%A9thodes-agiles)
- le [principe KISS](https://fr.wikipedia.org/wiki/Principe_KISS) (restez simple et stupide),
- la [stigmergie](http://www.lilianricaud.com/travail-en-reseau/la-stigmergie-un-nouvelle-modele-de-gouvernance-collaborative/) : droit à l'erreur, centré sur l'action et les traces pour faciliter la réutilisation du travail.

## Méthodes agiles

- Ne pas chercher à tout anticiper pour pouvoir s'adapter à l'imprévu
- préserver son énergie sur le long terme : découpage de grandes tâches en petites pour conserver une motivation constante
- [[kanban|Action]] pour le suivi des tâches : état d'avancement des tâches et savoir qui fait quoi
- [[décisions par vote binaire|discussion]] : tout doit pouvoir être proposé et remis en question rapidement et simplement
- accepter l'imprévu comme une nouvelle activité à faire et non comme un problème indésirable

## [Principe KISS](https://fr.wikipedia.org/wiki/Principe_KISS)

Rester simple, stupide (*"Keep It Simple, Stupid"*, créé en 1960 par la marine américaine).

- penser à **faire gagner du temps aux autres** : rester **simple** et **synthétique** car les participants n'ont ni la même culture, ni les mêmes motivations et ni la même énergie à dépenser dans le projet
- rester **accessible** à toute personne motivée pour contribuer dans le projet

## [Stigmergie](https://codoit.github.io/#stigmergic-model)

![stigmergie](https://codoit.github.io/images/stigmergy.png)

### Étapes :

1. On se met d'accord sur l'**objectif global et principal** (ici, l'expérimentation de la monnaie libre) en _laissant les détails de côté_
2. On **cherche un projet** qui nous intéresse et **on le copie** (fork)
3. On **agit sur sa propre copie** du projet
4. On **propose** les **modifications** aux intéressés (contribution)
5. On **regroupe** les **actions** ou on **mène** des **actions séparées**
6. On fait en sorte que l'action laisse des **traces** pour qu'elle soit **réutilisable** par d'autres ou pour revenir en arrière dans le cas d'une erreur (retour en phase 1)

### Organisation : action et trace pour la créativité

**Modèle centré sur l'action et les traces** et non sur la décision comme le font les modèles hiérarchiques et coopératifs (qui concentrent le pouvoir de décision sur les personnes les plus influentes).

Il met en avant la **créativité**. L'open-source en informatique se base sur ce principe et a permi à l'informatique d'avancer rapidement et d'être aujourd'hui partout.

> Toutefois, la stigmergie n'est pas incompatible avec les organisations hiérarchiques et coopératives. Tout groupe au sein de la stigmergie peut fonctionner en hiérarchie ou coopération, à condition de laisser des traces aux autres groupes de travail

### Légitimité de l'action par l'usage

Dans ce modèle non-décisionnel, *plus une action est réutilisée et plus elle est légitime*. Sa réussite ne dépend pas uniquement de l'avis d'un décideur quelconque. Alors comme les [makers](https://fr.wikipedia.org/wiki/Culture_maker) et les [DIY](https://fr.wikipedia.org/wiki/Do_it_yourself), plutôt que demander l'autorisation d'agir, il faut se sentir légitime d'agir, en s'inspirant/réutilisant de ce qui existe, puis montrer l'action pour voir si elle intéresse, et peaufiner s'il y a de l'intérêt extérieur. Et accepter de passer à autre chose si l'action n'est pas utilisée.

### Droit à l'erreur

Il est important de **ne pas avoir peur de faire des erreurs**. L'historisation est là pour pouvoir revenir en arrière si besoin, et l'erreur est un processus important et nécessaire d'apprentissage par la pratique !

### Concurrence

Si **des actions rentrent en concurrence**, il est important de **rappeler l'objectif principal et global** pour trouver un terrain d'entente, afin de trouver un terrain d'entente qui n'est pas de faire son action, mais bien de parvenir à l'objectif final.

**L'impossibilité de se mettre d'accord sur deux actions concurrentes** doit amener à **annuler les deux actions ou d'en créer une troisième qui s'inspire des actions concurrentes**.