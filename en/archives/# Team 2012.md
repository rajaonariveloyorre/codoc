# Team 2012
## Présentation des membres de l’équipe

Il ne s'agit pas d'intégrer in extenso le CV de tous les membres de l'équipe mais de décrire brièvement les points saillants du profil de chacun en rapport avec le projet
Il pourra ainsi être répondu aux questions ci-après :
• Qui sont les membres clés de l'équipe ? (nom, prénom, âge)
• Quelles formations ont été suivies ?
• Quelles responsabilités ont été exercées ? quelles expériences ont été
acquises ?
• Quelles réalisations professionnelles peuvent servir de références ?
• Quels sont leurs domaines de compétence dans le projet ?
Qu’est ce qui vous a poussé à participer au PH ?
 
### Jérôme Gontier = Emorej Reitnog
Jérôme, âgé de 40 ans, est un des initiateurs du projet Pixel Humain. Ingénieur informaticien de formation, il entreprend en 2008 une formation professionnelle sur l’environnement en Master II Développement Durable à Clermont Ferrand. Motivé et désireux de se rendre utile, il a pour ambition de participer et de proposer des initiatives positives pour la société. La rencontre avec les autres initiateurs du projet va permettre de fusionner deux projets similaires pour donner naissance au Pixel Humain. Jérôme a une vision globale du projet et participe à l’élaboration du projet de manière transversale.

### Stéphanie Lorente = Einahpéts Etnerol
Stéphanie, 30 ans, est directrice artistique Print & web, ainsi qu’auteur-photographe.
Elle évolue depuis 10 ans dans le monde de la communication et de la publicité. Elle a collaboré avec des agences nationales et internationales, et développé une expérience significative en travaillant sur des projets de clients grands comptes du secteur privé et public. Elle a ainsi pu participer à la mise en place de dispositifs de communication pluri-média, tout domaines confondus. Elle a eu des responsabilités aussi bien en création qu’en production, ce qui lui donne une parfaite maîtrise et connaissance des exigences créatives et techniques des postes constituant la chaîne graphique Print et Web.
Aujourd’hui directrice artistique indépendante sur l’île, son aspiration est de mettre à profit ses compétences, son expérience et son expertise au service de son engagement personnel.
Convaincue, engagée, critique, passionnée, curieuse, ouverte, patiente, tenace, dynamique, elle montre une implication dans son travail qui est le fruit de la passion qu’elle entretient envers toute son activité professionnelle, mais également, le fruit de son aspiration pour une société renouvelée.
Pour faire bref, son rôle dans le projet Pixel Humain est de mettre son talent de communicateur graphique au service de l’intelligence collective incarnée par le projet.






### Tibor Katelbach : Robit Chabletak
Tibor, 37ans, ingénieur Informatique, Développeur Sénior et Chef de projet indépendant depuis presque 20ans sur des projets de toutes tailles et tous budgets (PME, Yves Rocher, Petit Bateau, Perfony, Open Atlas...). Il défend depuis toujours la philosophie Open Source et l’innovation comme arme de Collaboration Massive. 
Toujours à la recherche de nouvelles approches pour améliorer le fonctionnement des choses, sa soif pour l’innovation et les nouvelles technologies font de lui un atout majeur dans les projets qu'ils entreprend ou auxquels il participe.
Il applique tous les jours la pensée de Gandhi "il faut vivre le changement qu’on souhaite voir dans le monde" avec ou sans les dernières technologies informatique avec lesquelles il excelle.
Cherchons LaTerreNative sur uneTerreNette !!

### Sylvain Barbot = Niavlys Tobrab
Après plus de 10 ans d'expérience de consulting informatique (développement, expertise technique, accompagnement utilisateurs), Sylvain s'oriente vers le développement agile et particulièrement SCRUM. Il y voit une méthode de développement de projet efficace et qui supprime les effets de frottement (liés aux méthodes classiques de gestion de projet) entre expression des besoins utilisateurs et réalisation concrète.
Rapidement, il se rend compte que cette méthode basée sur le bon sens peut s'appliquer à d'autres secteur que le développement informatique. Il choisit d'organiser un voyage au long court (14 mois de volontariat dans des ONG et associations œuvrant pour la protection de l'environnement) en utilisant cette méthode. Le voyage deviendra le projet Green Trotter : partenariat avec des classes d'élèves, magasine Green Mag, site internet, promotion de l'écovolontariat...
Fort de ces expériences passées, il rencontre Jérome, Tibor et Stephanie et se lancent dans le projet Pixel Humain qui n'est autre que le bon sens de la méthode SCRUM appliqué à l'organisation de la société. 
Il participe à la définition des besoins, au développement ainsi qu'à l'organisation globale du projet afin d'appliquer les bonnes pratiques acquises lors de ses expériences passées et de faire du Pixel Humain la plate forme pour donner au citoyen sa place dans la Démocratie.


### Pierre Yves Fonteix = Sevy-Erreip Xietnof
Habite à Nouméa, Nouvelle-Calédonie, 33 ans, formation EGC Nouméa puis Sup de Co Toulouse. Autodidacte dans beaucoup de domaines faisant appel à la créativité, il s’est tourné pendant un temps dans la construction de maisons écologiques afin d’appliquer une célèbre phrase de Gandhi devenue pour lui une véritable directive de vie “ Deviens le changement que tu désires pour le monde”, puis est revenu à ses premières Amours avec la communication aussi bien graphique que stratégique et événementielle afin de vivre pleinement ses rêves.
Dynamique, créatif, perfectionniste, autonome et toujours souriant, il souhaite, aujourd’hui, mettre toutes ses compétences au service des changements nécessaires à l’évolution du monde dans lequel nous habitons. 

Mickael hoarau

Graziella Hoareau



