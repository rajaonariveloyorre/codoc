Définition d'un nouveau type qui a comme particularité d'avoir une adresse fixe, ce qui permet d'y aller physiquement pour y trouver ce qu'il propose (là où une organisation n'a pas forcément de lieu fixe). Cela peut être très pratique pour savoir les ressources d'un lieu par exemple.

# Ce qu'on peut faire avec le type LIEU :

## Réutilisation dans les formulaires en faisant le lien avec les adresses
Quand on veut ajouter un lieu à un élément (organisation, événement, projet, citoyen), il suffira de mettre le nom de ce lieu et il sera proposé de manière dynamique. Il n'y aura plus qu'à le sélectionner pour que l'adresse de ce lieu se mette automatiquement.

Et vice versa, quand on met une adresse, il nous donne le nom du lieu (s'il fait partie de la base de donnée).

## Avoir une liste de lieux favoris
On aurait une liste des lieux favoris qu'on pourrait ajouter et retirer facilement de sa liste (en appuyant sur un item comme une étoile par exemple (image utilisée par firefox))

## Rajouter des éléments (organisation, projet, événement, citoyen) à un LIEU

## Avoir facilement accès aux ressources d'un LIEU et comment accéder à ces ressources.
Très rapidement on doit savoir le degrés d'ouverture du LIEU, pour savoir ce qu'on a le droit d'y faire et de ne pas y faire.
Un LIEU c'est aussi une liste de ressources auxquelles on peut potentiellement accéder. Comment accéder à ces ressource, peut on les emprunter, les utiliser, etc...? Par exemple dans le bâtiment de Lille Sud Insertion il y a une armoire où l'on peut donner et prendre ce que l'on veut, cette donnerie ferait partie des ressources du LIEU.

## Différentes orgas peuvent avoir différents events dans un même lieu

## Mettre en avant les lieux ressources (pour chaque projet)
Le but ici est d'avoir un visibilité directe sur les lieux ressources d'un projet. Un lieu ressource pour un projet est un lieu qui permet de s'informer sur un projet, voire être utilisé par les personnes qui gèrent le projet. Par exemple pour communecter, on pourrait aller dans certains lieux physiques pour se renseigner sur la plateforme.

## Exemple :
Le LIEU "mairie d'Hellemmes" est un espace physique situé dans le parc François Mitterrand à Hellemmes

l'ORGANISATION "mairie d'Hellemmes" est une organisation gouvernementale présente dans de nombreux lieux, dont le LIEU "mairie d'Hellemmes"

l'ORGANISATION "conseil de quartier du centre" (qui dépend de l'organisation mairie d'Hellemmes), se réunit dans le LIEU "mairie d'Hellemmes"

# Qu'est ce qu'un type LIEU ?
Un type lieu c'est un ensemble d'éléments qu'il peut contenir :
* Latitude
* Longitude
* Adresse
* Pays
* Nom
* gestionnaire (organizationId) - non obligatoire
* ressources du lieu
** 3d, CNC, decoupe laser..
** tables, chaises, ...
** vidéo-projecteur
* description
* activités (projet, type event, )
* degré d'ouverture
** Lieu ouvert ? Fermé ? Public ? Participatif ? Comment s'investir dans le lieu ?
* réseaux de lieux ( interconnecté les lieux )
** mutalisation de lieux 
* Photos
* plan du lieu ?

# Les différents archétypes du type LIEU
Il pourrait y avoir différents archétypes pour un type LIEU pour facilement se représenter quel LIEU 
## LIEU (place)
pour contribuer directement dans le json correspondant  c'est ici 
https://github.com/pixelhumain/co2/blob/lieu/config/CO2/place.json

### Section 
* Commun > icon : fa-circle-o
* Public > icon : fa-users
* Privé > icon : fa-lock
* Rural > icon : fa-tree
* Urbain > icon : fa-building-o
* Virtuel > icon : fa-keyboard-o
....

### Type 
Tiers Lieux > icon : fa-home
* coworking > icon : fa-
* Fablab > icon : fa-cogs
* Maker Space > icon : fa- 
* Jardin Partagé > icon : fa-

### Service
* Restaurant
* Creche
* Ludotheque
* BiblioThèque 

## RESSOURCE
pour contribuer directement dans le json correspondant  c'est ici 
https://github.com/pixelhumain/co2/blob/lieu/config/CO2/ressource.json
### Section 
* Commun > icon : fa-circle-o

....

### Type 
Tiers Lieux > icon : fa-home
* coworking > icon : fa-

### Service
* Restaurant

http://fontawesome.io/cheatsheet/

Le but étant d'avoir des choix prédéfinis en fonction de l'archétype du LIEU

Par exemple si nous sommes dans un FabLab, des ressources prédéfinies seraient déjà présentes, il n'y aurait alors plus qu'à les cocher si elles sont bel et bien dans le lieu.