# Outils utilisés respectant la [[charte]] de l'organisation

_Cette partie est incomplète, il faut se mettre d'accord sur les outils qu'on utilise en interne_

- [[Discussion]] :
  + [chat](https://chat.lescommuns.org/channel/co_accueil)
  + mailing liste : ?
- [[Contenu]] :
  + contenu volatile (vidéos, images, urls) : [communecter](https://communecter.org)
  + grand public : à définir : sûrement une partie "contribuer à communecter" accessible facilement dans communecter
  + schémas : [Mindmup](https://app.mindmup.com/)
  + agenda/calendrier : [communecter](https://communecter.org)
- Planification d'actions de groupe : [framadate](https://framadate.org)
- [[Développement logiciel]] : [github](https://github.com/pixelhumain/communecter)