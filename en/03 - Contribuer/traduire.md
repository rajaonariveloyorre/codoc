# Traduire Communecter

Bienvenue dans l'espace de traduction de Communecter. Nous allons vous montrer comment rendre la plateforme accessible dans différentes langues.
La traduction ne nécessite aucune compétence technique préalable.


## Traduire
NOUVEL ESPACE DE TRADUCTION : [**weblate.communecter.org**](http://weblate.communecter.org)
[Deepl.com](https://www.deepl.com/translator) s'avère être meilleur que [Google Traduction](http://translate.google.com/).


### Rejoindre le projet
-   Rejoindre Weblate : [s'inscrire](https://weblate.communecter.org/accounts/register/)
-   Demander à rejoindre le projet pixel-humain
