# Documenter

La documentation est un pilier essentiel de tout projet open source. En contribuant vous permettez de rendre la plateforme plus facile d'accès. Merci à vous 👍.

Discussions autour de la documentation : **[communecter.org/#codocwiki](https://www.communecter.org/#page.type.organizations.id.59e888b940bb4e1a3c19e0fd)** (rejoignez la messagerie)

Il est important de documenter vos méthodes afin que d'autres personnes puissent s'en emparer. Sinon vous serez seul à faire certaines tâches, et on sera bien embêté en votre absence.

## Participer au wiki
### Édition
...

### Contenu
Tout ce qui concerne le projet COmmunecter est pertinent a documenter. Ça va de la manière dont nous discutons, au code source en passant par la gouvernance et la communication.

### Organisation
...

### Traduction
...

#### Intégration de Google Slides
Fichier > Publier sur le web > Intégrer > Publier
Copie le code et intégrez le à la page.

## Enregistrer une visioconférence
La liste de nos visioconférences est **[ici](/1 - Le Projet/videos.md)**.

### OBS Studio
Si vous n'intervenez pas ou peu (sinon regardez [cette vidéo](https://www.youtube.com/watch?v=tWxd2t1pxVA) pour séparer les voix) :

1.  [Téléchargez](https://obsproject.com/fr/download) et installez OBS
2.  Ajoutez une _Capture de la fenêtre_ dans les sources en bas à gauche
3.  Dans _mixage audio_ : réduire _Mic/Aux_ et ajustez l'_Audio du bureau_
4.  _Démarrer l'enregistrement_ en bas à gauche

Faites un test de quelques secondes au début de la visioconférence pour tester votre configuration.

Si vous avez le temps et l'envie de le faire vous pouvez ajouter un fond, des logos, textes, images, ...


### Google Hangout
Nous essayons de nous dégoogliser un maximum, mais il arrive que nous utilisions Google Hangout lorsque les alternatives à Google dysfonctionnent, ou que l'on dépasse le nombre maximum de participant·e·s et/ou lorsque l'interlocuteur nous impose cet outil.

Pour que votre conférence soit directement publiée sur Youtube il faut programmer un Hangouts en direct : **[méthode](https://support.google.com/youtube/answer/7083786?hl=fr)**.


## Enregistrer une capture d'écran
Tutoriels : [Windows](https://fr.wikihow.com/faire-une-capture-d%27%C3%A9cran-sous-Windows), [Linux](https://fr.wikihow.com/prendre-une-capture-d%E2%80%99%C3%A9cran-sous-Linux) et [Mac](https://support.apple.com/fr-fr/HT201361).

Certains logiciels vous permettent de générer des gifs à partir de ce que vous voyez sur votre écran : [Peek (linux)](https://angristan.fr/peek-enregistrer-gif-linux/).
