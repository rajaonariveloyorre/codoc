# Communication de communecter

## Plateformes utilisées
-   [Communecter (profil)](https://www.communecter.org/#page.type.projects.id.55dafac4e41d75571d848360)
-   [Mastodon](https://mamot.fr/@communecter)
-   [Facebook (page)](https://www.facebook.com/communecter/?fref=ts)
-   [Facebook (groupe)](https://www.facebook.com/groups/1706464852909714/?fref=ts)
-   [Twitter](https://twitter.com/communecter)
-   [Vimeo](https://vimeo.com/user20626453)
-   [Wordpress](http://open-atlas.org/) (Open Atlas)
-   [Youtube](https://www.youtube.com/channel/UC8i1bbM8At3nGWbkr7gVUgA) (Open Atlas)
-   [MailChimp](https://mailchimp.com/) ([newsletter](http://open-atlas.org/actualite/))
-   [PeerTube](https://videos.lescommuns.org/search?search=%5BCO%5D)
-   ~~[Google plus](https://plus.google.com/communities/111483652487023091469)~~

Aujourd'hui seuls Mastodon, Facebook et Twitter sont régulièrement utilisés. Mais si vous souhaitez animer d'autres réseaux sociaux libre à vous.

À terme nous n'utiliserons plus MailChimp, mais pour l'instant ça nous est quasi-indispensable, notamment parce qu'il y a un éditeur visuel intégré qui permet de faire des templates assez facilement. Lorsque nous aurons un beau template (grâce à vous) nos chères développeurs pourront intégrer un formulaire directement dans Communecter accessibles aux communicants du projet.

Mastodon est principalement composé de militant⋅e⋅s libristes, même s'il commence à être approprié par une diversité de personnes. Le compte de communecter suit systématiquement les personnes qui aime et partage nos publications. Comme sur Twitter l'utilisation de tags est très important.

Le groupe [Communication de Communecter](https://www.communecter.org/#@co-communication) rassemble toutes les parties prenantes à ce chantier. Rejoignez-nous sur la messagerie :)


## Outils et astuces pour communecter votre entourage
Vous souhaitez recevoir des visuels pour diffuser communecter dans votre ville ? Contactez-nous par mail : contact@communecter.org.

-   Épingler une publication sur les réseaux sociaux ([exemple](https://www.facebook.com/communecter/posts/2063123177079543))
-   Signature mail ([télécharger](https://cloud.co.tools/index.php/s/peopdPooKRSNrYe))
-   Ajouter cet encart sur votre site ou dans une publication ([télécharger](https://cloud.co.tools/index.php/s/cTAkjjGdyXdKDdR/download))
-   Plaquette de présentation de l’outil ([télécharger](https://cloud.co.tools/index.php/s/EBjN4q2Ar4YjPFg))
-   Mettre son profil dans les métadonnées de son profil mastodon
-   Parcourir son carnet d'adresse pour envoyer un petit mail pour découvrir communecter.org

![](/Images/rejoignezmoi.jpg)


## Stratégie globale
Travail en cours visible [ici](https://docs.google.com/document/d/1VksIYLqM2l9t7COTCBq78uqMGjfeA3P-hqZqgPFO25w/edit?usp=sharing).

<table id="bkmrk-%C2%A0-open-atlas-commune">
<thead><tr>
<td class="align-center" style="width: 149px;"> </td>
<td class="align-center" style="width: 150px;">
<p><strong>OPEN ATLAS</strong></p>
</td>
<td class="align-center" style="width: 171px;">
<p><strong>COMMUNECTER</strong></p>
</td>
<td class="align-center" style="width: 187px;">
<p><strong>SMARTERRE</strong></p>
</td>
<td class="align-center" style="width: 152px;">
<p><strong>PIXEL HUMAIN</strong></p>
</td>
</tr></thead>
<tbody>
<tr>
<td style="width: 149px;">
<p><strong>TYPE</strong></p>
</td>
<td style="width: 150px;">
<p><span style="font-weight: 400;">Association</span></p>
</td>
<td style="width: 171px;">
<p><span style="font-weight: 400;">SCIC</span></p>
</td>
<td style="width: 187px;">
<p><span style="font-weight: 400;">Projet sociétal</span></p>
</td>
<td style="width: 152px;">
<p><span style="font-weight: 400;">Collectif d’individus</span></p>
</td>
</tr>
<tr>
<td style="width: 149px;">
<p><strong>OBJET</strong></p>
</td>
<td style="width: 150px;">
<p><em><span style="font-weight: 400;">Aventuriers des communs</span></em> <span style="font-weight: 400;"><br></span><span style="font-weight: 400;">Au cœur du numérique et libristes</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">Un tiers lieu</span></p>
</td>
<td style="width: 171px;">
<p><em><span style="font-weight: 400;">Réseau social libre</span></em><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">Outils numérique  d’aide aux alternatives</span></p>
</td>
<td style="width: 187px;">
<p><em><span style="font-weight: 400;">Emulateur de liens territoriaux</span></em></p>
<p><span style="font-weight: 400;">Développement d’un territoire intelligent</span></p>
<p><span style="font-weight: 400;">(s’appuie sur CO)</span></p>
</td>
<td style="width: 152px;">
<p><em><span style="font-weight: 400;">Entité / concept virtuel</span></em></p>
<p><span style="font-weight: 400;">Historiquement étape nominative du projet communecter devenu tous les individus œuvrant pour les communs et les évolutions sociétales vertueuses</span></p>
</td>
</tr>
<tr>
<td style="width: 149px;">
<p><strong>VALEURS COMMUNES</strong></p>
</td>
<td style="width: 660px;" colspan="4">
<p><span style="font-weight: 400;">Libre - Utopiste </span></p>
<p><span style="font-weight: 400;">Gouvernance partagée</span></p>
<p><span style="font-weight: 400;">Lien - Intelligence collective - Collectif- Participatif</span></p>
<p><span style="font-weight: 400;">Ouvert - Inclusif</span></p>
<p><span style="font-weight: 400;">Responsable - Citoyen</span></p>
<p><span style="font-weight: 400;">Alternatif - Transformation - Innovation </span></p>
</td>
</tr>
<tr>
<td style="width: 149px;">
<p><strong>ADJECTIFS</strong></p>
</td>
<td style="width: 150px;">
<p><span style="font-weight: 400;">Neutre - Expérimental - Foisonnant</span></p>
<p><span style="font-weight: 400;">Journalistique</span></p>
</td>
<td style="width: 171px;">
<p><span style="font-weight: 400;">Neutre - Professionnel - Positif</span></p>
<p><span style="font-weight: 400;">Journalistique</span></p>
</td>
<td style="width: 187px;">
<p><span style="font-weight: 400;">Neutre - Professionnel - Structuré - Attirant</span></p>
<p><span style="font-weight: 400;">Journalistique</span></p>
</td>
<td style="width: 152px;">
<p><span style="font-weight: 400;">Engagé - Personnel - Subjectif</span></p>
</td>
</tr>
<tr>
<td style="width: 149px;">
<p><strong>PARTENAIRES</strong></p>
</td>
<td style="width: 150px;">
<p><span style="font-weight: 400;">Ariane Régulation</span></p>
<p><span style="font-weight: 400;">Comptoir du vrac</span></p>
<br><br><br>
</td>
<td style="width: 171px;">
<p><span style="font-weight: 400;">La Myne</span></p>
<p><span style="font-weight: 400;">Rocket.Chat ?</span></p>
</td>
<td style="width: 187px;">
<p><span style="font-weight: 400;">Efficacity - TCO</span></p>
<p><span style="font-weight: 400;">(expérimentation de territoire)</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">Antoine Daval (VRP COmmunecter)</span></p>
<p><span style="font-weight: 400;">Ariane Régulation</span></p>
</td>
<td style="width: 152px;">
<p><span style="font-weight: 400;">Les libristes</span></p>
</td>
</tr>
<tr>
<td style="width: 149px;">
<p><strong>CLIENTS</strong></p>
</td>
<td style="width: 150px;">
<p><span style="font-weight: 400;">/</span></p>
</td>
<td style="width: 171px;">
<p><span style="font-weight: 400;">Entités des communs :</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- Territoires et collectivités</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- Associations</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- SCIC</span></p>
</td>
<td style="width: 187px;">
<p><span style="font-weight: 400;">TCO (en tant que financeur)</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">Territoires et collectivités</span></p>
</td>
<td style="width: 152px;">
<p><span style="font-weight: 400;">/</span></p>
</td>
</tr>
<tr>
<td style="width: 149px;">
<p><strong>PROPOSITION DE VALEUR</strong></p>
</td>
<td style="width: 150px;">
<p><span style="font-weight: 400;">- Animation de filière</span></p>
<p><span style="font-weight: 400;">- Incubateur de projets</span></p>
<p><span style="font-weight: 400;">- Tiers lieu</span></p>
<p><span style="font-weight: 400;">- Kitir vrac</span></p>
</td>
<td style="width: 171px;">
<p><span style="font-weight: 400;">- Outils / réseau social / couteau suisse libre pour individus et groupes</span></p>
<p><span style="font-weight: 400;">- Documentation &amp; accompagnement au développement des outils libre </span></p>
<p><span style="font-weight: 400;">- COstum sur mesure</span></p>
</td>
<td style="width: 187px;">
<p><span style="font-weight: 400;">Vision projet :</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- outils</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- expertise d’accompagnement</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- animation de filière</span></p>
</td>
<td style="width: 152px;">
<p><span style="font-weight: 400;">- Débats et idées particulières, richesse de chaque individu unique</span></p>
</td>
</tr>
<tr>
<td style="width: 149px;">
<p><strong>RESSOURCES</strong></p>
<p><strong>HUMAINES</strong></p>
<p><strong>(au 24 juin 2019)</strong></p>
</td>
<td style="width: 150px;">
<p><span style="font-weight: 400;">- Salariés</span></p>
<p><span style="font-weight: 400;">Animateurs de filière et community manager</span></p>
<p><span style="font-weight: 400;">- Administratrice</span></p>
<p><span style="font-weight: 400;">- Stagiaires</span></p>
<p><span style="font-weight: 400;">- Bénévoles</span></p>
</td>
<td style="width: 171px;">
<p><span style="font-weight: 400;">- Sociétaires ?</span></p>
<p><span style="font-weight: 400;">- Développeurs indépendants</span></p>
<p><span style="font-weight: 400;">+ ressources partagées OPAL</span></p>
<p><span style="font-weight: 400;">- Partenaires</span></p>
<p><span style="font-weight: 400;">- Libristes (à mobiliser)</span></p>
</td>
<td style="width: 187px;">
<p><span style="font-weight: 400;">- Développeurs indépendants</span></p>
<p><span style="font-weight: 400;">+ ressources partagées OPAL</span></p>
<p><span style="font-weight: 400;">- Partenaires</span></p>
</td>
<td style="width: 152px;">
<p><span style="font-weight: 400;">/ entité, concept virtuel, chaque individu</span></p>
</td>
</tr>
<tr>
<td style="width: 149px;">
<p><strong>OUTILS DE COM.</strong><strong><br></strong><strong>UTILISÉS</strong></p>
</td>
<td style="width: 150px;">
<p><span style="font-weight: 400;">EXTERNE</span></p>
<p><span style="font-weight: 400;">- page CO</span></p>
<p><span style="font-weight: 400;">- Site Wordpress</span></p>
<p><span style="font-weight: 400;">- Charte éditoriale (à compléter)</span></p>
</td>
<td style="width: 171px;">
<p><span style="font-weight: 400;">EXTERNE</span></p>
<p><span style="font-weight: 400;">- Newsletter</span></p>
<p><span style="font-weight: 400;">- COstum campagne / nos WHY ?</span></p>
<p><span style="font-weight: 400;">- Réseaux sociaux :</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">CO, Mastodon, Facebook, Twitter</span></p>
<p><span style="font-weight: 400;">- Docs partagés (google)</span></p>
<p><span style="font-weight: 400;">- Cartes de visites</span></p>
<p><span style="font-weight: 400;">- Plaquette CO et COstum</span></p>
<p><span style="font-weight: 400;">- Windflag</span></p>
<p><span style="font-weight: 400;">--- Chat</span></p>
</td>
<td style="width: 187px;">
<p><span style="font-weight: 400;">EXTERNE</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- Charte en développement</span></p>
<p><span style="font-weight: 400;">- COstum et affiches In progress (cible territoires)</span></p>
</td>
<td style="width: 152px;">
<p><span style="font-weight: 400;">EXTERNE</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- Affiches projet originelles</span></p>
</td>
</tr>
<tr>
<td style="width: 149px;">
<p><strong>OUTILS DE COM.</strong><strong><br></strong><strong>COMMUNS</strong></p>
</td>
<td style="width: 660px;" colspan="4">
<p><span style="font-weight: 400;">INTERNE</span></p>
<p><span style="font-weight: 400;">- Chat</span></p>
<p><span style="font-weight: 400;">- Outils partagés (google, Wekan, pages CO, hack MD...)</span><span style="font-weight: 400;"><br></span><em><span style="font-weight: 400;">Développer :</span></em><span style="font-weight: 400;"> un livret d’accueil avec les infos principales </span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">pour s’y retrouver / structure + outils utilisés</span></p>
</td>
</tr>
<tr>
<td style="width: 149px;">
<p><strong>OUTILS DE COM.</strong><strong><br></strong><strong>À DÉVELOPPER</strong></p>
</td>
<td style="width: 150px;">
<p><span style="font-weight: 400;">- Stratégie globale</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- COstum adapté</span></p>
<p><span style="font-weight: 400;">- COzette ? Ici ou dans CO.</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- Kiltir Lab à développer</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- Kiltir vrac</span></p>
<p><span style="font-weight: 400;">- Ecole du libre</span></p>
<p><span style="font-weight: 400;">- Implication communs</span></p>
<p><span style="font-weight: 400;">- Com interne et </span></p>
</td>
<td style="width: 171px;">
<p><span style="font-weight: 400;">- Stratégie globale</span></p>
<p><span style="font-weight: 400;">- Bouton social &amp; démarchage supports pertinents liés</span></p>
<p><span style="font-weight: 400;">- Relations presse + ressources visuels</span></p>
<p><span style="font-weight: 400;">- Signatures mail harmonisées ?</span></p>
<p><span style="font-weight: 400;">- Demandes de chacun ?</span></p>
<p><span style="font-weight: 400;">- Charte de publication externe (clarification positionnement, aspects juridiques, 10 points de la bienveillance / ex. Hello ?) / voir lexique ?</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- Clarification / amélioration de la doc. sans destruction</span></p>
</td>
<td style="width: 187px;">
<p><span style="font-weight: 400;">- Stratégie globale</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- Possibilité de personnalisation du logo ? Interne ou externe</span></p>
<p><span style="font-weight: 400;">- COstum territoire (cible citoyens et orgas)</span></p>
<p><span style="font-weight: 400;">- Charte de publication externe (clarification positionnement, aspects juridiques, 10 points de la bienveillance)</span></p>
<p><span style="font-weight: 400;">- Plaquette</span></p>
</td>
<td style="width: 152px;">
<p><span style="font-weight: 400;">- Stratégie globale</span><span style="font-weight: 400;"><br></span><span style="font-weight: 400;">- Communauté à animer, endroit de la subjectivité et de l’individualité</span></p>
</td>
</tr>
<tr>
<td style="width: 149px;">
<p><strong>TON</strong></p>
</td>
<td style="width: 150px;">
<p><span style="font-weight: 400;">Tous azimuts, enthousiaste, expérimental</span></p>
</td>
<td style="width: 171px;">
<p><span style="font-weight: 400;">Mesuré, constructif</span></p>
</td>
<td style="width: 187px;">
<p><span style="font-weight: 400;">Structuré,  fédérateur, optimiste, institutionnel</span></p>
</td>
<td style="width: 152px;">
<p><span style="font-weight: 400;">Subjectif, libre, ouvert</span></p>
</td>
</tr>
</tbody>
</table>


## Charte éditoriale

Cette charte éditoriale régit la façon d’aborder les contenus éditoriaux au nom du projet COmmunecter et de l’association Open Atlas (site, documents diffusés, articles lorsqu’ils sont anonymes, interface, newsletter).

Elle a été votée le 19 mars 2019 et un nouveau vote est prévu en mars 2020. Des propositions d'améliorations peuvent être faites sur le [document de travail](https://docs.google.com/document/d/1v2Hr3M9jidffQG5lblf8w8iq0_M4awJeB78oWX_FRS8/edit?usp=sharing).

1.  Chaque personne au sein de COmmunecter et Open Atlas est libre d’adopter l’orthographe qu’il souhaite dans son propre espace ou lorsqu’il signe ses publications.
2.  Les pratiques de chaque personne (orthographiques essentiellement) étant différentes et la perception des nouvelles écritures (épicène, point médian) posant la question du positionnement d’Open Atlas, il est nécessaire de clarifier les règles adoptées, qu’elles soient adoptées et pratiquées par tous, lors de rédaction de contenus au nom du collectif.
3.  Il est important de favoriser l’équilibre femme-homme au sein du collectif comme dans la société et cela doit se formaliser dans ses contenus. Le questionnement quant à la meilleure forme à adopter pour parvenir à ce résultat peut faire évoluer cette charte régulièrement. On peut fixer une date d’adoption. La charte peut être revisitée annuellement au besoin, la nécessité d’avancer sur d’autres points excluant de la remettre en cause en permanence_._
4.  Le fond étant aussi important que la forme, il sera tenu comme un point important lors des points communication et la création de nouveaux supports, choix d’articles, d’envisager la question de manière équilibrée quant aux genres.
5.  En termes de formulations, on favorise l’utilisation d’une tournure neutre (pour les boutons et autres termes.
> Exemple : « Inviter vos connaissances » plutôt que « inviter vos ami.e.s »).
On favorise une double présence d’un terme au masculin et féminin dans un rédactionnel plus conséquent plutôt que d’utiliser le point médian :
> Exemple : Plutôt que « Les citoyen.ne.s sont nombreux.ses. ». On privilégiera « Le citoyens et citoyennes sont en nombres ».
6.  L’utilisation du point médian faisant débat, elle doit faire l’objet d’un vote et sera tranchée pour un an jusqu’à la révision suivante, les usages et positions de chacun pouvant évoluer.
7.  Chaque publication devrait faire l’objet d’une correction orthographique au minimum et de forme si besoin.

