ressources

# Ressources humaines :
3 développeurs seront mobilisés pour le projet, 1 graphiste, 1 chef de projet ainsi que des animateurs, community manager, facilitateurs de communauté pour transmettre et communiquer avec les communautés d’usagers finaux.  

# Ressources techniques : 
Nous bénéficions de toute la technique et l'expérience accumulées avec les années du projet COmmunecter et de l’association Open Atlas. Très active en animation de divers filières (numérique, écologie, déchet, océanographie...)  localement à La Réunion, mais aussi à Lille ou à Lyon.  A cela s’ajoute l’expérience du réseaux de partenaires mobilisés sur le projet qui apportent une belle vision, des objectifs concrets nous permettant une bonne vision des  besoins du secteur de la culture.

# Ressources financières 
L’association existe depuis 2009 et investit souvent dans des projets qu’elle considère d'intérêt général et oeuvre un peu en incubateur social et numérique, en aidant grâce à son expérience à accélérer le déploiement de projets qui ont du sens pour la société de demain. L’association est en train de passer sous forme coopérative et participe financièrement au coût financier du projet en temps homme et en hébergement des serveurs.
