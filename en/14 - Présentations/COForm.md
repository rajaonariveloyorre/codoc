#COForm
Collaborative Open Forms

- Create forms on demand and at will
- Represent complex questions 
- Multistep surveys 
- Standardized data structures
- Create surveys corresponding to your communities objectives 

(OPEN FORM SCHEMA)[https://docs.google.com/presentation/d/1jwi3pYEzL_r8IH1AEpzlJe4jTt2kG7BZS5Hyq1kZHeE/edit#slide=id.p]

(PPT)[https://docs.google.com/presentation/d/12E7zhh-Tp7J0fRcEVRIuxpXPlbPBVBP7GB3JnycPNm8/edit#slide=id.g737fb454ef_0_7]

# Technically 
a coform is based on a parentForm
possibly containing subForms

- render entrypoint defaults to 
    + survey.views.tpls.forms.formWizard 
    + can be overloaded by $parenForm["tpl"]
- This calls rendered by a wizard stepper 
    - survey.views.tpls.forms.wizard.php
- Each subForm is rendered by 
    - survey.views.tpls.forms.formSection
- Which run through the list of inputs using a FORM BUILDER process 
    - costum.views.custom.co.formbuilder
- Each input has it's own page, 
    - input templates
        + survey.views.tpls.forms
        + survey.views.tpls.forms.complex
        + survey.views.tpls.forms.costum for specific inputs
    - each input can have it's own params
## simple inputs 
## cplx inputs 


