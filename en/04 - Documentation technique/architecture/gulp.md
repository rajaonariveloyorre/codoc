# Mise en place optimisé des assets

## installation

il faut installer sur ça machine nodejs et npm, on va utiliser meteor

```sh
curl https://install.meteor.com/ | sh
```

https://gulpjs.com/docs/en/getting-started/quick-start

avec npm il faut installer gulp cli

```sh
meteor npm install --global gulp-cli
```

Ensuite il faut deplacer le contenu du dossier `pixelhumain/gulp`dans le repertoire à la racine où il y a les repertoire modules/ et pixelhumain/

```sh
meteor npm install
```

## gulp task

les task sont dans le fichier Gulpfile.js
mon test est sur le layer mainsearch.php (ce que je vois sur le site communecter)
donc j'ai recup tout les fichier js et css qui passe en chargement je l'ai mis dans un array
puis je les traite : 

### css

- je passe d'abord tout les css dans postcss > autoprefixer (fichier par fichier pour voir les erreurs d'abord)
- je concatene les fichiers dans un fichier all.css
- je minify le fichier et crée un nouveau fichier all.min.css
- je génére le sourcemap

tout est copier dans web/css
  
### fonts

je copie les fonts nécessaires qui on des chemins lier au css en général (web/fonts)


### js

- je concatene les fichiers dans all.js
- je minify le fichier (uglify)

je voulais le passer par babel mais ça passe pas pour le moment il y a une erreur avec jquery il faut que je test plus
pour comprendre

### revisions

je passe les fichier all.min.* par la revisions et crée les fichiers manifest
tout est copier dans build/

### commande

pour lancer la commande 

```sh
meteor gulp build
```

## coté php

dans yii 1 il faut utiliser

```
Yii::app()->clientScript->scriptMap
```

pour remplacer des fichiers par un autres

donc dans mainsearch.php à la fin j'ai mis 

```php
<?php
if(Yii::app()->params["gulp"]){
    $layerFile = "../../layerfile.json";
    if (file_exists(realpath($layerFile))) {
    $layerFileJSON = file_get_contents($layerFile,FILE_USE_INCLUDE_PATH);
    $layerFileJSON = json_decode($layerFileJSON,true);
    $fileName = basename(__FILE__);
    $keyLayer = array_search($fileName, array_column($layerFileJSON["layers"], 'layerName'));
    if($keyLayer!== false){
        $cssAnsScriptFilesModuleAll = array_merge($layerFileJSON["layers"][$keyLayer]["js"], $layerFileJSON["layers"][$keyLayer]["css"]);
        foreach ($cssAnsScriptFilesModuleAll as $file) {
            $extention = pathinfo($file,PATHINFO_EXTENSION);
            if ($extention == "js" || $extention == "JS") {
                if(isset(Yii::app()->params["assetsUrl"]["jsUrl"])){
                    Yii::app()->clientScript->scriptMap[basename($file)] = Yii::app()->params["assetsUrl"]["jsUrl"].'/web/js/'.$layerFileJSON["layers"][$keyLayer]["layerNameOut"].'.all.min.js?v=2.8.1';
                } else {
                    Yii::app()->clientScript->scriptMap[basename($file)] = Yii::app()->baseUrl.'/web/js/'.$layerFileJSON["layers"][$keyLayer]["layerNameOut"].'.all.min.js?v=2.8.1';
                }
            } else if ($extention == "css" || $extention == "CSS") {
                if(isset(Yii::app()->params["assetsUrl"]["cssUrl"])){
                    Yii::app()->clientScript->scriptMap[basename($file)] = Yii::app()->params["assetsUrl"]["cssUrl"].'/web/css/'.$layerFileJSON["layers"][$keyLayer]["layerNameOut"].'.all.min.css?v=2.8.1';
                } else {
                    Yii::app()->clientScript->scriptMap[basename($file)] = Yii::app()->baseUrl.'/web/css/'.$layerFileJSON["layers"][$keyLayer]["layerNameOut"].'.all.min.css?v=2.8.1';
                }
            }
        }
    }
 
?>
```

## paramconfig.php

rajouter

```json
"gulp" => true,
```