# ActivityPub & Communecter

## Informations générales
### Comment utiliser ActivityPub
Pour utiliser ActivityPub (AP) vous devrez avoir un acteur accessible en public. Le format de votre acteur doit être en JSON-LD. Votre acteur doit ressembler à ceci : [exemple acteur sur le site communecter partir développement](http://dev.communecter.org/api/activitypub/actor/id/5c73883cdd0452bf7a0da2e8).

L'id doit correspond à l'URL du document, tous les liens doit être en HTTPS.

Vous aurez besoin d'une inbox car les instances qui utilisent AP ont besoin reconnait un acteur si celui-ci possède une inbox. Chaque acteur nécessite une publicKey et une privateKey qui peut être généré avec openssl.

JSON ne prend en charge les sauts de ligne, on doit remplacer les sauts de ligne par \n (pour la partir publicKeyPEM).

Le Webfinger permet de demandé à un site web "Avez vous un utilisateur avec ce nom d'utilisateur" et de recevoir des liens de ressource en réponse. Le noeud finale d'un Webfinger se trouve toujours en /.well-known/webfinger et recevoir des requêtes tel que /.well-known/webfinger?resource=acct:name@ my-example.com

Pour pouvoir envoyé un message (par exemple avec le verbe "Create"), il est nécessaire d'avoir la inbox de la personne concerné et d'utiliser un HTTP signature qui permettra de savoir si l'acteur qui souhaite envoyer un message est le bon. Une signature HTTP s'agit d'un en-tête HTTP signé par la paire de clés RSA. Pour avoir plus d'information sur la création d'une signature HTTP, référencier vous le lien suivant partir HTTP signature : [How to implement a basic ActivityPub server([https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/](https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/)).

Liens pratique :
-   [Setting Up a WebFinger Server](https://www.packetizer.com/ws/webfinger/server.html)
-   [How to implement a basic ActivityPub server](https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/)
-   [ActivityPub W3](https://www.w3.org/TR/activitypub/)
-   [Decentralizing Social Interactions with ActivityPub](https://hacks.mozilla.org/author/dariusmozillafoundation-org/)


## Développement en cours (Tony et Pierre)
### Réalisation d'un acteur AP sur Communecter

J'ai crée une api qui permet de généré un acteur AP sur communecter, vous pouvez trouvez un exemple en cliquant [ici](http://dev.communecter.org/api/activitypub/actor/id/5c73883cdd0452bf7a0da2e8). Petit précision : la publicKey est en format brute. Comment sa fonctionne : Vous devrez indiquez dans le lien id de la personne ici : [http://dev.communecter.org/api/activitypub/actor/id/](http://dev.communecter.org/api/activitypub/actor/id/) + userId.

Fichier modifier : TranslateActivityStream.php / Rest.php . Fichier crée : ActivityPubController.php / ActorAction.php .


### Création d'un client mastodon
Nous avons crée avec Pierre un client mastodon qui permet de poster/récupéré les statuts/récupéré les followers/voir la timelines public en utilisant le [REST API](https://docs.joinmastodon.org/) et en ce basant sur cette librairie [mastodon.js par Kirschn](https://github.com/Kirschn/mastodon.js). Vous pouvez retrouvez nos codes [ici (gitlab.adullact) : mastodon_api](https://gitlab.adullact.net/emmat974/mastodon_api)


### Test d'un serveur express-activitypub en nodejs
J'ai suivi ce tutoriel là pour pouvoir envyer un message sur mamot.fr : [Decentralizing Social Interactions with ActivityPub](https://hacks.mozilla.org/2018/11/decentralizing-social-interactions-with-activitypub/). Pierre a détaillé plus en bas comment réaliser le tutoriel. Remarque constaté : Pour pouvoir voir un message sur mamot.fr, nous étions dans l'obligation de suivre notre acteur crée par express activitypub, sans cela, il nous est impossible de voir nos message dans le timeline. Ce problème est régurant quand on a tenté avec un compte pixelfed de communiqué avec mamot.fr.


### La réalisation d'une inbox (non fonctionnelle)
Je me suis basé sur le script crée par [Darius Kazemi](https://hacks.mozilla.org/author/dariusmozillafoundation-org/) sur son tutoriel [Decentralizing Social Interactions with ActivityPub](https://hacks.mozilla.org/2018/11/decentralizing-social-interactions-with-activitypub/). Malheureusement je n'arrive pas à faire fonctionné mon inbox car je n'arrive pas a encodé la signature.


### Création d'un client Mastodon
Utilisation de l'API mamot pour pouvoir afficher les timelines public, local, ajouter un post.
Afin de pourvoir acceder a le page mastodon cliquer içi [Join Mastodon](https://docs.joinmastodon.org/)
Mon code source est disponible sur gitlab [Code Source](https://gitlab.adullact.net/pierregbx974/mastodon_api)


### Ajout d'un acteur ActivityPub sur l'API de communecter
Afin d'avoir un aperçus du format d'un acteur ActivityPub en ajoutant votre id a la fin du lien [mon lien pour accéder à l'acteur](http://dev.communecter.org/api/activitypub/actor/id/)


### [Test d'envoi d'un message sur mamot](https://github.com/dariusk/express-activitypub.git)
-   Taper la commande `git clone [https://github.com/dariusk/express-activitypub.git](https://github.com/dariusk/express-activitypub.git)` dans votre terminal.
    -   Installer ngrok, en suivante les instructions [https://ngrok.com/download](https://ngrok.com/download).
    -   Taper la commande `/.ngrok/http 3000`
    -   Ensuite allez dans le dossier `express-activityPub`, modifier le dossier `config.json` dans route.
    -   Mettre un nom d'utilisateur
    -   Mettre un mot de passe
    -   Le port sera 3000, et le domain sera l'adresse en https de ngrok, mais il faut retirer le https, pour obtenir ceci exemple: `d40f1818.ngrok.io`
    -   Taper la commande `node index.js`
    -   Acceder a l'url du site en https et rajouter admin exemple: `[https://d40f1818.ngrok.io/admin](https://d40f1818.ngrok.io/admin)`
    -   Crée un compte test saisissez le nom d'utlisateur et le mot de passe, ensuite il y aura des données qui seront générés, afficher le account WebFinger url, copier le suject sans la acct:
    -   Connecter vous a [https://mamot.fr](https://mamot.fr).
    -   Rechercher le webfinger url, cliquer sur le bouton pour follow.
    -   Ensuite saisir test dans account name, Copier coller l'API Key, et saisir votre message exemple : Hello world !

Et vous pourrez voir le message envoyer sur mtps://mamot.fr
