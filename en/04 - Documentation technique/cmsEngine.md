# CMS ENGINE / COSTUM BIULDER

- costumize any element in CO 
- switch and use existing templates 
    + template selector
- save templates 
- add new sections or blockCMS 
    + section selector 
    + filter by type 
- create new blockcms 
- 

/modules/costum/views/tpls/tplsEngine.php
permet de changer de template 
par categorie 
affiche les panels template 
renderP : /modules/costum/views/tpls/blockCms/cmsEngine.php
    build and renders every child blockCms 
    interface d'ajout de section 

# schema
https://docs.google.com/presentation/d/1ZjD7D45on5is0VeKPtJaC9wU9EULmUdlbpqvFPspsK0/edit#slide=id.gbc2e05f158_0_44

# lexique
un costum est une personnalisation globale 
un template est juste pour decrire le front et permet d'unir des blockcms 
type : template 
    parent : est le createur du template
    tplUser : sont les utilisateur d'un template 
    cmsList : liste des blockCMS
type : blockCMS
    tplParent : est le template parent du block sur le template sera dans cmsList 
type : blockCMScopy

## Utiliser un template 
copy un

# TODO
- creer un nouveau costum "costumize" 
    - il est vide 
    - contient seulemetnt ajouter un block CMS
    - tester le sur 2 element (2 slug different)
    - enregistré comme template 
        - enregistre et permet de réutiliser une liste de blockCMS dans un autre contexte par un autre slug 
        - réutiliser openCostum.ifa openCostum.mira
- a partir du costum "costumBuilder" qui utilise le slug pour se basé sur un element 
    - 2 possibilités
        - faites des essaies avec 
            - https://www.communecter.org/costum/co/index/slug/comm1possible/test/costumBuilder
        - soit on utilise un costumCMS existant 
            - selecteur de template existant 
            - isTemplate true 
        - on part d'un costum vide "openCostum"
        - 
- **open Costum templates** pour des page static structuré construite d'une liste de block CMS
    - Quand on ajoute une page statique via btn et formulaire APP
    - mettre un lien vers la page 
    - ajouter le btn ajouter une section 
    - creer un section ne doit plus ecrire dans costum.tpls mais dans cms 
        - avec la réference de la page 
        - le parents
        - les parametres de la section cms 
    - objectif : selecteur de template de page statique 
        - bare de choix modele de page statique 
        - un page statique template porte les attributs
            - isTemplate:true
            - shortDescription    
        - on select > applique la modele de la page statique 
        - un btn save duplique la struture pour la nouvelle page
    - plus tard déplacer la creation de page statique dans cms
