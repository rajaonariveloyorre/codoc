# Installer Communecter sur Ubuntu

## Prérequis
Pour utiliser Communecter vous devez avoir
- Apache 2 & PHP 5.6 (Attention, ne fonctionne pas sur PHP 7)
    - `sudo apt-get install apache2 php5.6`
    - Pour passer de la version 7 à 5.6 vous pouvez suivre la solution suivante : [https://askubuntu.com/questions/761713/how-can-i-downgrade-from-php-7-to-php-5-6-on-ubuntu-16-04](https://askubuntu.com/questions/761713/how-can-i-downgrade-from-php-7-to-php-5-6-on-ubuntu-16-04)
    - Certains modules doivent être installé
        - rewrite `sudo a2enmod rewrite`
        - curl `sudo apt-get install php5.6-curl`
        - mcrypt `sudo apt-get install php5.6-mcrypt`
        - gd `sudo apt-get install php5.6-gd`
        - mongo `sudo apt-get install php5.6-mongo`
        - dom `sudo apt-get install php5.6-dom`
    - Pour vérifier votre version de PHP `php -v`
- [MongoDB 3.4](https://docs.mongodb.com/v3.4/tutorial/install-mongodb-on-ubuntu/) (Ne fonctionne pas avec la 3.6) :


```shell
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
sudo apt-get update
sudo apt-get install -y mongodb-org
```

## Install
Clone from GitHub :

```shell
mkdir ~/dev && mkdir ~/dev/communecter 
mkdir ~/dev/communecter/modules 
cd /var/www/html/
sudo ln -s ~/communecter communecter
cd ~/dev/communecter
git clone https://gitlab.adullact.net/pixelhumain/pixelhumain.git
cd modules/
git clone https://gitlab.adullact.net/pixelhumain/citizenToolKit.git
git clone https://gitlab.adullact.net/pixelhumain/co2.git
ln -s co2 network
git clone https://gitlab.adullact.net/pixelhumain/api
git clone https://gitlab.adullact.net/pixelhumain/graph.git
git clone https://gitlab.adullact.net/pixelhumain/classifieds.git
git clone https://gitlab.adullact.net/pixelhumain/ressources.git
git clone https://gitlab.adullact.net/pixelhumain/places.git
git clone https://gitlab.adullact.net/pixelhumain/chat.git
git clone https://gitlab.adullact.net/pixelhumain/learn.git
git clone https://gitlab.adullact.net/pixelhumain/survey.git
git clone https://gitlab.adullact.net/pixelhumain/interop.git
git clone https://gitlab.adullact.net/pixelhumain/cotools.git
git clone https://gitlab.adullact.net/pixelhumain/eco.git
cd ../
```

Installer les dépendances avec Composer :
```shell
sudo apt install composer
cd pixelhumain/ph/
composer update
composer install
```

Rename the dbconfig file :
```shell
cd protected/
cp config/dbconfig.exemple.php config/dbconfig.php
```

Si le fichier `dbconfig.php` n'existe pas, créer le : `nano config/dbconfig.php`
```code
<?php
$dbconfig = array(
     'class' => 'mongoYii.EMongoClient',
    'server' => 'mongodb://127.0.0.1:27017/',
     'db' => 'pixelhumain', 
 );
?>

```

Création du dossier runtime et assets
```shell
mkdir runtime
sudo chmod -R 775 runtime/
cd ../
mkdir assets
sudo chmod -R 775 assets/
cd ../../
sudo chown -R [votreUser]:www-data pixelhumain
sudo chown -R [votreUser]:www-data modules
```

## Configuration
And configure Apache vhost : edit `/etc/apache2/sites-available/communecter.conf` :

```
<VirtualHost *:80>
    ServerName domain.ext
    ServerAdmin webmaster@domain.ext
    DocumentRoot /var/www/communect/pixelhumain/ph
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    Alias "/ph" "/var/www/communect/pixelhumain/ph"
    <Directory "/var/www/communect/pixelhumain/ph">
    Options FollowSymLinks Indexes
    AllowOverride all
    Order deny,allow
    Allow from 127.0.0.1
    #deny from all
    <IfModule mod_rewrite.c>
    Options +F  ollowSymLinks
    IndexIgnore /
    RewriteEngine on
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond $1 !^(index\.php|assets|robots\.txt)
    RewriteRule ^(.*)$ /ph/index.php/$1 [L]
    </IfModule>
    </Directory>
</VirtualHost>
```

Activate the new vhost :
```shell
sudo a2ensite communecter 
sudo a2enmod rewrite 
sudo service apache2 reload
```

## Configurer MongoDB
```shell
#Making the Data folder for MongoDB
mkdir data/
mkdir data/db

```

Import les données :
```shell
cd modules/co2/data
unzip cities.json.zip
mongoimport --db pixelhumain --collection cities --file cities.json
```

Ajouter ces entrées dans la collection **applications** (lié au message d'erreur  "Missing Configs db.applications.key...") :
```
mongo
  use pixelhumain
  db.createCollection("applications")
  db.applications.insert({ "name" : "DEV Config",     "key" : "devParams",     "mangoPay" : {         "ClientId" : "communecter",         "ClientPassword" : "xxxx",         "TemporaryFolder" : "../../tmp"     } } )
  db.applications.insert({ "name" : "PROD Config",     "key" : "prodParams",     "mangoPay" : {         "ClientId" : "communecter",         "ClientPassword" : "xxxx",         "TemporaryFolder" : "../../tmp"     } } )
  exit

cd ../


```

Ajouter MongoDB au démarrage et faire le script d’utilisation : `sudo nano /etc/init.d/communecterdb`
Ajouter ceci :
```YAML
#!/bin/sh
#
### BEGIN INIT INFO
# Required-Start:    $local_fs $remote_fs
# Required-Stop:     $local_fs $remote_fs
# Provides:          unattended-upgrade-shutdown-check
# Default-Start:     2 3 4 5
# Default-Stop:      0 6
# Short-Description: Check if unattended upgrades are being applied
# Description:       Check if unattended upgrades are being applied
#                    and wait for them to finish
### END INIT INFO

```

Sauvegardez :
```shell
sudo chmod +x /etc/init.d/communecterdb
update-rc.d communecterdb defaults
update-rc.d communecterdb enable
```

Alternative : créer le script `communecterdb.sh`, le rendre exécutable et l’ajouter dans `/etc/rc.local`

## Vérifier que tous les composants sont installés
Accéder a l’url suivant : [http://127.0.0.1/ph/test](http://127.0.0.1/ph/test)
Et réglez tout les points rouges.


## C’est bon vous pouvez utiliser COMMUNECTER
Accédez à cette adresse : [http://127.0.0.1/ph/](http://127.0.0.1/ph/)


## Optionnel
### Interface Mongo
Si vous souhaitez avoir une interface pour géré MongoDB vous pouvez télécharger Robo3T : [https://robomongo.org/download](https://robomongo.org/download)

### Email service to cron
#### Add
`crontab -e` : `*/2 * * * * curl https://127.0.0.1/ph/co2/test/docron`

#### Use
Communecter is now available : [http://127.0.0.1/ph/](http://127.0.0.1/ph/)
To try the email service, go to [http://127.0.0.1/ph/co2/test/docron](http://127.0.0.1/ph/co2/test/docron)
To use in production mode, edit index.php

### Update
Create `/var/www/html/communecter/update.sh` containing :

```shell
red=`tput setaf 1`
green=`tput setaf 2`
blue=`tput setaf 0`
bgWhite=`tput setab 7`
reset=`tput sgr0`

```

Make it executable and launch it.
