----

# Liens en rapport avec le stage Laurent Després (lotik)
## Open atlas du 17/02/2020 au 31/07/2020
 
----

*Merci à l'équipe open atlas et plus particuliérement les développeurs de communecter de m'avoir accepté, et intégré pour ce stage.*

### Interop Mediawiki

Intéropérabilité avec les mediawiki, celui de la fabrique des mobilitées et de movilab ont été utiliser pour le développement.

   [Lien dépot gitlab (branche lotikdev)](https://gitlab.adullact.net/pixelhumain/interop/-/tree/lotikdev)
   
   [Changement dans module co2 pour intégration du bouton dans menu element](https://gitlab.adullact.net/pixelhumain/co2/-/commit/d83f5c454180ad0fd94d18697949cfd7fcb543a6)
   
   [Convert dans citizentollkit module (tout a la fin du fichier la fonction  convertWikiMediaToPh)](https://gitlab.adullact.net/pixelhumain/citizenToolKit/-/blob/master/models/Convert.php)
   
   [Translate avec mapping de mediawiki de fabmob le mapping est a faire pour chaque mediawiki connecter le nom](https://gitlab.adullact.net/pixelhumain/citizenToolKit/-/blob/master/models/TranslateMediaWiki.php)
   
   [Wekan](https://wekan.communecter.org/b/AxnuvRgfsA55fGQtb/objectif-interop-stage-lotik-03-2020-greater-than-07-2020)
   [DOC du stage](https://codimd.communecter.org/m1TM7HxkS3y3AbhFF25RrQ#)
   
### Bookmarks

Ajout du champs catégorie/type de liens ajouter, possibilités d'ouvrir dans communecter une video youtube, des markdown github gitlab codimd
   
   [Ajout select pour categorie du lien](https://gitlab.adullact.net/pixelhumain/co2/-/commit/9aa8144aa70207d40d3fb5f36e06b2697354d3f8)
   
   [Process d'ouverture des différente catégorie de lien dans co2/assets/js/co.js L3580](https://gitlab.adullact.net/pixelhumain/co2/-/blob/master/assets/js/co.js)
   
### Intégration docMd gitlab

Intégration du module codoc de gitlab dans communecter la liaison ce fait via le module interop, utilisation de l'api graphQl de gitlab

[GitlabController](https://gitlab.adullact.net/pixelhumain/interop/-/blob/lotikdev/controllers/GitlabController.php)

[ApiGitlab model](https://gitlab.adullact.net/pixelhumain/interop/-/blob/lotikdev/models/ApiGitlab.php)

[Vue pour l'intégration dans co2/views/docs/index.php](https://gitlab.adullact.net/pixelhumain/co2/-/blob/lotikdev/views/docs/index.php)


### Apico

Crétion d'un server en node js afin de former une api pour communecter et une couche d'authentification a l'aide de jsonwebtoken.

[Dépot avec readme pour l'installation](https://gitlab.adullact.net/pixelhumain/api2/-/tree/lotikdev)

[Doc swagger openAPI 3.0](https://gitlab.adullact.net/pixelhumain/api2/-/blob/lotikdev/openapi.yaml)

