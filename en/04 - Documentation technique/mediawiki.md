# Mediawiki & Communecter

## Introduction
Cette partie du module interop, cherche en cas de mediawiki existant à le connecter avec communecter et de permettre via un bouton a créer des éléments du type de la page du wiki, ou en cas d'élément présent de fournir le lien.

## Initialisation du mediawiki
Dans le menu d'un élément on clic sur @Mediawiki en dessous de "Points d’intérêt".

Arrivé dans **interop/MediaWiki/**
**beforeAction()** instancie **APIMediawiki** (model de relations avec le wiki) qui est enfant de **DB** (model de relations avec la bdd) cherche en base de données l’existence ou non de ce wiki, si non cela crée dans collection "mediawiki" un wiki vide de la forme:

```PHP
public function newIn($id, $name, $type)
{
$baseDataDB = [
"name" => $name,
"parent" => [
"id" => $id,
"type" => $type
],
"url" => "",
"params" => "none",
"logo" => ""
];
PHDB::insert("mediawiki", $baseDataDB);
}
```

L'action par default du controller **ActionIndex()** elle récupère les infos en base de données via le model et envoie les bonnes données dans la vue
```
{
    "_id" : ObjectId(""),
    "name" : nomduwiki,
    "parent" : {
        "id" : ,
        "type" : 
    },
    "url" : "<urlEnregistré>/api.php",
    "params" : {
        "ressources" : {
            "get" : [ 
                "[[Cat%C3%A9gorie:<catTrouvé>]]", 
...
            ]
        },
        "acteurs" : {
            "get" : [ 
                 "[[Cat%C3%A9gorie:<catTrouvé>]]", 
...
            ]
        },
        "projets" : {
            "get" : [ 
               "[[Cat%C3%A9gorie:<catTrouvé>]]", 
...
        }
    },
    "logo" : ""
}
```

## Utilisation navigation
Sur **interop.views.default.indexMediaWiki** les boutons Acteurs, Projets, Emplois s'affiche en cas d’existence.
Clic direction **interop/mediawiki/menuleft** qui appel la méthode **menu(**catégorieChoisie) de l'instance **APIMediaWiki**.
Qui va chercher la listes des pages liées créer la data pour la vue appelé par le controller qui affiche les résultats dans un menu déroulant trié par ordre alphabétique en accordéon si résultat est supérieur à 30 unités: **interop.views.menus.pages.**

Au clic sur le nom du page direction **interop/mediawiki/page** qui appel la méthode page(nomPage) de l'instance **APIMediaWiki** qui scan les propriétés disponibles de la page puis récupère les données.
Ces données sont trié et converties afin de standardiser pour la vue et les inputs préremplies de formulaires de création d'élément via
`convertWikiMediaToPh(['data' => $dataRécupérer, $nomCatégorie, $nomDuWiki)` dans le module **citizenToolKit/model/Convert.php**

Ces données sont envoyées dans la vue **interop.views.page.index** qui affiche par défault un **A propos** de la page du wiki et le menu left contient en fonction des données des boutons proposant de lister **Communautées**(sous divisé en communauté et utilisateur), **Défis, Ressources** en rapport avec la page du wiki.

Tout clic sur une entités réprésentant une autre page du wiki fait le méme chemin a partir de **interop/mediawiki/page** du controlleur **interop/MediaWiki.**


## Relations wiki et Intégration dans communecter
Dans **A propos** il ya trois boutons qui font les liens:
Un bouton **Page du Wiki** qui redirige vers la page source du wiki.
Un bouton **Site web** qui redirige vers le site de l'élement du wiki.

Le dernier bouton peu avoir deux valeurs :
**Créer dans communecter** qui ouvre un formulaire préremplie et éditable afin de créer un élément communecter.
**Page Communecter** qui redirige vers la page communecter en relation avec l'élément du wiki.
