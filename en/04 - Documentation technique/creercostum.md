# Créer un COstum

## Comment créer votre propre COstum
Vous êtes autonome et vous souhaitez créer votre propre COstum de A à Z, nous allons vous expliquer les étapes pour mettre en place votre COstum.

### Création d’un élément
Pour créer votre COstum, vous devrez le créer un élément, pour cela rendez-vous sur votre plateforme communecter installer sur votre machine et créer un élément qui doit être soit une organisation soit un projet.

Dans votre base de données, ajouter à votre élément la ligne suivante :
```JSON
costum : {
    slug : votreSlugCostum
}
```

Le slug de votre COstum doit correspond à celui que vous l’avez donné dans votre fichier json, en général le nom du slug dans costum et le même que celle de votre élément.


### Préparer le fichier json de votre COstum
Vous devrez créer un fichier json dans le dossier qui se trouve dans `costum.data`.

Le nom du fichier doit être indique à celui de votre slug élément. Nous vous conseillons de récupérer le code basant sur l’un des COstums que nous avons créée dans un premier temps puis de vous renseigner sur [https://www.communecter.org/costum/](https://www.communecter.org/costum/) pour mieux comprendre la structure de la donnée e


### Mise en place du COstum
Crée un dossier dans `costum.view.custom.nomDeVotreSlug`.

Et dans ce dossier, crée le fichier home et fait le pointer depuis votre fichier json, vous pouvez ensuite tester votre costum depuis [`http://127.0.0.1/ph/costum/co/index/id/votreNomElement`](http://127.0.0.1/ph/costum/co/index/id/votreNomElement).

Ou rendez-vous sur votre page élément et cliquer sur le bouton “Has costum".
