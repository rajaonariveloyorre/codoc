# Invitation Process 

# invite Panel on an organization
    + /co2/element/invite/type/organizations/id/555eba56c655675cdd65bf19
    + add roles 

# executes /co2/link/multiconnect 
```
parentId : "555eba56c655675cdd65bf19"
parentType : "organizations"
listInvite[invites][114105107105107105649911146116111111108115][name]   : "rikiki"
listInvite[invites][114105107105107105649911146116111111108115][mail]   : "rikiki@co.tools"
listInvite[invites][114105107105107105649911146116111111108115][msg] : ""
listInvite[invites][114105107105107105649911146116111111108115][isAdmin] : ""
listInvite[invites][114105107105107105649911146116111111108115][profilThumbImageUrl] : "/assets/d94b153d/images/thumb/default_citoyens.png"
```
    + account exists just adds links below 
    + account not exists 
        * "username" : "4cbno25x3rkt0lfhdzeymqjwvg8sui79",
        * "pending" : true,
    + in both cases creates
        * links.members on organization 
        * links.memberOf on citoyens

## receive email    
    + accepting
    + link : /co2/person/validateinvitation/user/5fbfabdc539f22a048ca81fa/validationKey/194dd0fcea150acef6448d0686f0ebc8064e969ebd5d92f258551f703095d414/invitation/1/costum/true/targetId/555eba56c655675cdd65bf19/targetType/organizations/answers/true 
        * if exists validates invitation
        * else 
            * open registration panel to finish registration

## goto orga Accept invitation on orga 
    + this removes the attribute isInviting 