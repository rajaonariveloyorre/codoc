# communecter un CHATON 

### Qu'est qu'un chaton

>TODO : s'inscrire et signer la charte

>CHATONS est le Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires.

>Il rassemble des structures souhaitant éviter la collecte et la centralisation des données personnelles au sein de silos numériques du type de ceux proposés par les GAFAM (Google, Apple, Facebook, Amazon, Microsoft).

https://framablog.org/2020/02/26/ouverture-du-mooc-chatons-internet-pourquoi-et-comment-reprendre-le-controle/
https://framatube.org/video-channels/mooc.chatons.1/videos

## Les Logiciel Libre mis à disposition et connecté avec le meme COmpte 

# Rocket chat
!@corocketChat
https://chat.communecter.org

# Wekan
!@cowekan
https://wekan.communecter.org

# NextCloud
!@conextcloud 
https://conextcloud.communecter.org

# CodiMD : collaborative markdown editor
!@cocodimd
https://codimd.communecter.org

# Documentation
!@codoc 
doc.co.tools

# SSO COmmunecter
!@cosso
https://sso.communecter.org