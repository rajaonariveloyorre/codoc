# ROCKET CHAT : COmmunecter messaging documentation

We know that effective collaboration depends on the quality of the tools you use. One of the major challenges of COmmunecter is to connect the actors of a municipality, whether they are communities, citizens, associations or companies, so that they can communicate and collaborate together to make their future better.Rocket Chat is an open source communication tool that offers many collaborative features to optimize the experience of exchanges within one or more teams.

# GOALS

This guide aims to help you to master the use of Rocket.Chat on COmmunecter and to explain the meaning and use of the different specific commands created by COmmunecter.

### We love, use and make rocketChat better
In this documentation we show you how to access COmmunecter's messaging and what other tools COmmunecter brings to make Rocket.Chat better.

# Rocket Chat inside CO ![](https://codimd.communecter.org/uploads/upload_7d345f4738b379a2a631650093559b29.png)


Inside CO, Rocket.Chat is used as a means of communication for its members so that they can participate in discussions, moderate or manage chat rooms and share a set of good practices for a more efficient use of this means of communication.

# Usage
Generally,the Rocket.chat inside COmmunecter is used with the same method as the Rocket.chat but with only a few additions to the slash command.
If you still don't know what Rocket Chat is and or how to manipulate it, I invite you to visit https://docs.rocket.chat to help you.


# CONNEXION
## Access via COmmunecter
To access your messaging via COmmunecter, go to an element (for example your association's page) and click on "Messaging".
![](https://codimd.communecter.org/uploads/upload_145f94ad05d586937e7b65f3c552a45d.png)


## Access Direct
For direct access into your chat without going through Communecter type https://chat.communecter.org. 
In this case, use the same username and password as when you connect to https://communecter.org.


# General interface
![](https://codimd.communecter.org/uploads/upload_57a4994dac9c9b3ea7bb7d4f7ef6192e.png)


#### 1. The menu bar :
The left side panel presents the menu allowing you to access your preferences, customize the display, and find or create chat rooms.

#### 2. The home page :
The right side hosts exchanges when you select one of your chat rooms. On connection, no exchange is pre-selected: the home page is displayed.


#### 3. The chat list:
This panel is reserved for the list of chat rooms in which you are registered. By default, they are sorted by discussion type (Channels, Private Groups, Private Messages) and within each type by date of last activity (most recently active at the top of the list).

<h1 style="color:blue;text-align:center" >USE OF THE VARIOUS SLASH COMMANDS </h1>

# Slash Commands
Slash commands are Rocket Chat and COmmunecter tools designed to perform a task or interoperating with CO or OCECO.

To use a slash command, start by typing /, then type the command and press Enter. Some commands may require additional arguments, such as search terms, to create interesting material.

When you typing "/",then a menu which lists all the slash commands offered by COmmunecter on your server or by the options present by default in Rocket Chat appears , choose one, and press Enter.



|Command            | setting   | short description                                    |
| --------          | --------  | --------                                             |
| /invite           | @username |  invite someone into a room                          |
| /invite-from-room | #channel  | invite all users from a room to join an another room |
| /invite-to-room   | #channel  |    invite all users of the room to join another room |

 
<h2 style="color:black;text-align:left" >CO SLASH COMMAND</h2>



| Command             | setting             | Short description                                                                      |
| ------------------- | ------------------- | -------------------------------------------------------------------------------------- |
| /co-search          | -the term to search | this command slash is used to search project present in COmmunecter                    |
| /co-search-user     | @username           | this command slash is used for search a user in COmmunecter                            |
| /co-whereiam        |                     | this command slash is used to return element url in COmmunecter of the current channel |
| /co-check-add       |                     | this command slash is used for add check message on the channel                        |
| /co-check-list      |                     | this command slash is used to show all check point history                             |
| /co-set-annonce     |                     | this command slash is used                                                             |
| /co-set-description |                     | this command slash is used to set description of the channel (for element admins only) |
| /co-set-annonce     |                     | this command slash is used to set annoucement of the channel(for element admins only)  |
| /co-set-topic       |                     | this command slash is used to set topic of the channel (for element admins only)       |
| /co-members-sync    |                     |  this command slash is used to synchronizes the co-members of the project or organization (for element admins only)                                                                                                            |
| /co-add-news        |                     |  this command slash is used for add news on the channel                                |
| /co-add-wizard-event|                     |this command slash is used to add event on the channel                                  |



## OCECO SLASH COMMAND

| Command                                 | setting  | Short description |
| --------------------------------------- | -------- | ----------------- |
| /oceco-add-action                       | #projectSlug (if you are on the projects channel , no need to specify the projectslug)actionDesc @personAssigned                           | this command slash is used to add an action to a project                    |
| /oceco-list-comment-action              | id       | this command slash is used to show the list of comments for the action      |
| /oceco-element-list-action              | #channel | this command slash is used to list the actions of an OCECO element          |
| /oceco-me-list-action                   | #channel | this command slash is used to list the actions to do in OCECO               |
| /oceco-me-finish-action                 |       id | this command slash is used to finish an action in OCECO                     |
| /oceco-me-search-acion                  |  search  | this command slash is used to search for a stock among its OCECO shares     |
| /oceco-add-comment-action               | id comment  | this command slash is used to add a comment to an action                 |
| /oceco-add-task-action                  |  id task |  this command slash is used to add a task to an action                      |
| /oceco-list-task-action                 | id       | this command slash is used to shows the list of the action's task           |
| /oceco-checked-task-action              |   id     | this command slash is used for checked action's task                        |
| /oceco-element-dashboard-action-numbers | #channel@username | this command slash is used to show the dashboard of action figures for a global oceco element or for a user |


## DOC TEchnique
pour l'intégration avec rocket chat
la declaration de rcObj
```
/d7/modules/chat/views/default/rocketchat.php
```
tu pourras trouver tout ce qui est lié à
```
Yii::app()->params['rocketchatEnabled']
```


