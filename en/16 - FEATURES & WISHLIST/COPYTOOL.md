# COPYTOOL
https://docs.google.com/drawings/d/1ACcE-wwXnlHVUvuOh0QrARfRbuImyJC6SkmoZmipJxo/edit

*COPYTOOL* est un script qui prend une liste de parametre en compte pour effectuer des copies partielles et ciblées de données : 
préparer la data d’une source origin selon des conditions (copyWhere...) 
effectuer des actions (replace...) dessus 
Copier dans une destination

## TODO : 
- *CONNECT* : Utiliser les params pour se connecter à la source et la destination
- *GET* : Faire moulinette pour le get selon les config de copyWhere
- *MODIFY* : Faire la moulinette pour chaque action de transformation ex replace
- *UPSERT / UPDATE* : appliqué sur la destination