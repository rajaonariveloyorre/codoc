# CMCAS Haut de Bretagne 
https://codimd.communecter.org/s/yr8aTsuRQ#

## Scenarios que l’on peut envisager : 

## TOTAL : 8K€

### Costum + Design 2K€
### Programme d'evennement* de type différent 2K€
- **Rejoindre un event**
    * Prise de rendez-vous et rendez- vous en en visio par un bénéficiaire.
- **Proposition d'event d'un membres**
    * Un bénéficiaire propose une activité ponctuelle : randonnée. Il dépose son projet et reçoit des confirmations de participation.
- **Agenda**
    * Un calendrier 
    * avec des rendez-vous clé
        * Dimanche matin 9h00 – éveil musculaire avec un vidéo live
        * Tous les x 20h30, lecture d’une histoire pour les petit avec une conf tel
        * Art plastique dans Y dans la salle de conférence
        * Mise en place d’action PAPYSITtING
- **Géolocalisation des bénéficiaires** pour qu’ils puissent se rencontrer. Mode déclaratif et réversible.
- **Partage d'url sur le journal** 
    + Live d’un spectacle (conte, cours de dessin, masterclass, concert).
    + via une url Vimeo
    + Film, musique en streaming.
- **Salon de chat dédié**
    + Débat en direct suite à la « projection d’un film ».
    + Débat sur un projet.
### Depot de proposition de projet + vote 1K€
- Votation sur un projet.
### Documentation  1K€
- Accès à des contenus de formation (Mooc)
- Prévention santé
### Page Thématique lié à une tag 2K€
- à la semaine comme « raconter son travail » 
- Des films en streaming qui comporte une introduction
- Une expo photo sur la thématique
- Un concert
- Un débat avec un intervenant
- Un acte éthique pour les plus jeunes…..
### De l’information
- Par les élus
- Par des partenaire comme notre mutuel
