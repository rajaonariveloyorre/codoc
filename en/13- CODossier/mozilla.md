# Mozilla Builders Incubator

>
(Link)[https://builders.mozilla.community/index.html#programs]
(Link)[https://mozilla2.forms.fm/mozilla-builders-summer-startup-studio/forms/7943/]responses/new


## Describe what your company does in 50 characters or less.
#CollectiveIntellegenceToolsForCommunity&Citizens

## Now, what is your company going to make? Describe the product. 

Our NGO developed and run a free and civil society network called COMMUNECTER.org
 
Nowadays, we are going a step further with a new product CO.OP.Tools. 
This is a 4 steps social system ( software and methodologies ) for helping or building open communities that protect , build and create commons.

1st step : *Knowledge collecting using COForm* (Collaborative Open Forms ) 
an open source form totally connected to communecter.org societal 
network.  

2d step : *Observe using CObservatory* (Collaboration Observatory)
Once data is collected it can be vizualised to help organization create common objectives and guidelines to actions 

3d step : *COmmunicate using COstum*
creates rich personnalised interfaces and specific tools for communities directly connected to COmmunecter.org and using all it’s functionalities and features.

4th step : *COllaborate with OCECO* (open collaborative tools for cooperative economies) inventing  and experimenting a set of tools to help communities create and reach common objectives, follow actions, run horizontal governance, implement multiple financing methods, and project and task management 

(Link)[https://conextcloud.communecter.org/s/PprGg7NdRLetopo]

# If you have a demo, overview, or pitch, add links here. 
- Open source societal network (COMMUNECTER.org)[https://www.communecter.org/#welcome]
- COFORM sample 
(Link)[http://qa.communecter.org/costum/co/index/slug/sommom/answer/5e7c50a98b509c29588b457a]
(Link)[documentation https://conextcloud.communecter.org/s/m6N3Jqdcj3k86AM]
- COBservatory sample 
(Link)[https://cte.ecologique-solidaire.gouv.fr/#dashboard]
- COstums samples 
(pacte-transition.org/)[pacte-transition.org/]
(CTE)[https://cte.ecologique-solidaire.gouv.fr]
and many others 
(Codeco)[https://www.communecter.org/costum/co/index/id/costumDesCostums#searchcostum]
- OCECO overview (Link)[https://conextcloud.communecter.org/s/obK3fxftEfbrpre]

# How far along are you? 

Each of the four elements are already developed, we now experiment use cases where we put them all together in different contexts

Communecter.org is in production since 2016 with around 40000 user elements(People, Organizations, Projects, Events)

COForms has been evolving since 2018, but lately in december 2019 it reached full autonomy, it’s currently used in all the experiments cited below and has a life of it’s own by it’s openness and how it can be used.

CObservatory is in production on 3 different contexts (ecology, marine biodiversity, self organization) since september 2019  and experimenting in many others specifics use cases.

COstums are in production since 2018 and deployed on 30 communities (large and small) on many different contexts with various objectives (Events, organization, education).

OCECO is the last innovation. It started alpha experimentation phase since february 2020 and is currently being tested with 10 cooperatives, NGOs and companies.


# Why does your idea matter: How will it help deliver on the full promise of the internet, or fix the internet? 

The internet is now part of society and is a mirror of it.  If we want the internet to deliver it’s full potential, we have to make society reflect the openness of the internet's fundamental promises to society.

In the same way ethical tools ((wikipedia, openstreet maps, debian, firefox,… ) revolutionized the internet by their ethics, we need to keep on building tools to revolutionize our societies with openness, collaboration and cooperative economies.


There is a lot in common in the way societies evolve and how the internet works and it’s protocols for the massive amounts of information it processes. 

The new internet will have less ecological impact, more frugal innovation thanks to more distributed P2P architectures.

There’s a huge playground for societal tools today. We still dream of something as open as wikipedia and as connected as Facebook to build autonomous, resilient, interactive territories.  

Let’s share a vision of a Human internet, where every human is a pixel of society, and the Internet connects Human Pixels together. 

We are the generation that can make the Fix that we want to see for the internet. 
So let’s give our generation the tools and the methods to do it.

(Link)[https://conextcloud.communecter.org/s/dfCcbc9e8HRwpEQ]


# Who are your users or potential users? 

Today on COMMUNECTER.ORG you can connect with :
11 131 Citizens 
9779 non governmental organisation
6064 locals business
1882 governmental organizations 
2485 projets 
4142 events
3511 point of interest

# How will you get users? 

Communecter is a network of networks. Each community invites its own members to use the tools we produce for them. So we are constantly growing since we started. Satisfied communities are onboarding new ones. And as we build projects on demand for the needs of any of these communities, and everything is reusable, every new project generates interest for other communities in an endless virtuous circle.

# Why are you working on this? Do you have domain expertise? How do you know people need what you’re building? *

We believe in autonomy, transparency, direct financing and open governance. We want and believe in open platforms to make this concrete and we still don’t have this type of product in existence, so we’ve been building our own concrete utopie.

We’ve been working and building tools for the commons such as communecter and it’s rich toolbox since 2012 and in production since 2016. We work with many communities, so we get more and more experienced in large ecosystems. 
(Link)[https://www.pacte-transition.org/assets/8d1efec/images/]siteDuPactePourLaTransition/partenaires_pacte.png

Since we started we’ve always been supported by users and financiers making the adventure a reality since 8 years. Last year, we were selected and build fully bottom up , ecology project observatory, with a multi level territorial approach (national, regional, city level) for the french ministry of ecology [for their CTE (ecology transition contract) programme https://(Link)cte.ecologique-solidaire.gouv.fr/]

Is there anything new or innovative about what you’re building? *

We are citizens who lack the tools they need to be more implicated , so our innovation will continue as long as we don”t find a way to answer these implication issues. It’s an old dream of a local participative citizen community connected for the good of a given sector (Agriculture, Culture, Education, Transport….) or just by global interest of a local town.  

Therefore we’ve been innovating both in technology and self-organization by connecting three powerful components as COForms, CObservatory and COstums on communecter.org network while experimenting OCECO with organizational coaches community.

Furthermore, our territorial approach is also innovative by a system as open as wikipedia and as connected as facebook, enabling people to build resilient, knowledge based, experimental communities and territories.

# Who are your competitors? *

We’ve been following the civic tech and alternative social network  tools since we started but few are working as open source products and have survived and grown as we have. 

This extra motivation and recognition we gain from those who share our objectives. 
The other civic tech companies who don’t do open source are holding their part of the market, but our full scale LIBRE approach is a niche we greatly believe in.

# What do you understand about your business that others don’t? *

Our innovation and understanding is systemic and has grown with our experience in working with large ecosystems who use COMMUNECTER. Each an everyone contributes in there own way, the adventure has been amazingly rich. 

Our added value comes from these years of building community tools , some faded others are still evolving and getting better and better continuously, along the open source way, where only the best and used code remains and gets better the rest disappears.  

We have reached a time where our technological stack is rich enough to be extremely mutable and adaptive to almost any context in society. Every new context and experiment we do, takes us to a new level of understanding, and insight into innovation and creativity. It’s like a puzzle constantly growing and unvailing new panels and pieces dropping in to be put together. Offcourse some pieces are mixed up and from an old broken puzzle and don’t fit anymore.

We definitely understand society as a real dynamic, interactive, co-constructive and productive ecosytem, just like the internet itself! 

# Will you make money, and if so, how? How big can this get? *

Our business model is build on contributions to the development of the commons. As all our work is available as open source, we only monetize specific requests and adaptation to new contexts. 

There are always great new ideas that make features evolve, this creates a lot of value and indirectly generated incomes. So any new features always finds financing first and then is made available for all. 

Sometimes communities also ask us to set the whole process up for them, and this generates again more income opportunities.

Our dynamic is self generating income , the more we innovate, the more income potential is created. 

This has been our model for years. Last year we generated 250,000€ based on this model.
This year we’re on the way to increase our financial result once more.

The business potential is proportional to our capacity to engage smart partnerships in the cooperative or simply in the creative process, always open to new business opportunities.


# If what you’re building is NOT a for-profit entity, explain: 
# 1. Why you made that choice? 

We are building a digital common. At the beginning it was a personal investment of the founding team, with strong ethics and open philosophy as the heart of our culture. 

Then in time we managed to create a model where our openness not only generated a confidence relationship with our clients and communities but also created a circulare open innovations dynamic and proportional investments which keeps on growing with every new community onboarding. 

This is why our ecosystem is actually turning into a european cooperative build as a rich ecosystem of territories, organizations, communities and citizens convinced by the way we have evolved.

# 2. What will drive your team to maximum ownership and urgency, and 

What we do is non profit, but nevertheless we manage to pay our diversified teams, inject part of our income into our own R&D and have a growing team of professional collaborators motivated by the ethics and the societal impact of the current project . New interested contributors constantly onboard along every year, they decide if they integrate the team or if they remain autonomous freelancers.

Our team is already very mobilised by everything going on and every project gets full focus.
New Features are reviewed by the ecosystem and distributed in the communities through fast and interactive feedback, making the iterative development highly dynamic and productive.

The interaction between the core team and the user communities brings a lot of motivation to everyone, since every creation is proudly owned and shared by the whole community. We all share the feeling of something permanently growing for a common benefit.


# 3. Why is an incubator format right for you? *

The project started in an incubator and is autonomous now.
Nowadays, it’s considered itself as a social incubator for communities and projects giving birth to a new generation of products.

We thinks that CO.OP.Tools is the next generation product we would love to develop with the Mozilla Builders incubator and its community.

Because, Mozilla respects the ethics and philosophy we’ve always believed in.  It’s a great opportunity for us to be challenged by such a like minded  context. 

Our roadmap for the project includes experiments with distributed local teams collaborating over many territories as Mozilla’s communities are.
 
And we’ve even imagined this proposal becoming a tool for the Mozilla incubator helping communities to develop.

# Who is coding on your team? Is any of the coding done by a non-founder? *


Our team is mostly made of developers with 4 core developers (Clement, Rapha, Thomas, Tibor), 2 mid core devs (Pierre, @Manut), 1 sysadmin (Thomas), 3 junior developers (Anatole, Nicolas, Laurent) and 2 graphists (Agatha, Ambre).

The code is and built on open source, so some of the coding have been previously done by non-founders of the project.


# Have you incorporated or created a legal entity? Please share the details. *

Open atlas actually has a  NGO statuts. We are transforming the NGO  into a SCE (European cooperative society) but the administrative procedure started in march have been slowed down by Covid’s crise but is still ongoing. 

Here is the link to SCE status (in french)
(Link)[https://conextcloud.communecter.org/s/2keK9MYk7rEQBGz]

# Have you taken any investments yet? If so, how much? *

We are currently in the process of submitting a BPI, French Public Bank of  investissement,  innovation loan of 200,000€. 

Describe ownership structure / cap table, as it exists or will exist? *
 
The code is open source, so belongs to the commons.
The ownership of the infrastructures supporting the platform’s tools belongs to the NGO Open Atlas. We are actually in a time of transition from associative to cooperative structure. We aspire to create a system where implication and participation opens new governance dimensions in the way  this digital common develops. 
It’s also one of the reasons why one element of CO.OP.Tools offers new organizational and governance techniques and tools applicable for a large number of participants.

Currently our first associates in the cooperative are the founders, all the collaborating freelancers and companies (Evolutio, Collab Studio, comm1Possible, La Myne, Oxamyne, Optéos,...), user organizations (many coops or NGOs), a French Region territories (TCO [Ouest Coast Territories of Reunion island]), cities (a Possession)
 
Here is the link to SCE status (in french)
(Link)[https://conextcloud.communecter.org/s/2keK9MYk7rEQBGz]

# Why are you applying to Mozilla Builders? *

We have reached a stage of development where we are recognized by institutions and  communities, so the transition to become a cooperative is an important step in scaling toward an innovative enterprise for building commons.

Mozilla builders is  for us a multi-dimensional opportunity not only for the mentor ship available but also for sharing technological innovations, generating more collaboration between open source producing communities and gaining visibility by such a great context. 

Moreover, that 75,000$ will allow us to go further in the development and deployment of our products.


# Any references / people we may know / people we can call to learn more about you?
(Contrat de Transition Ecologique )[https://cte.ecologique-solidaire.gouv.fr/]
Ministry of transistion, ecology, and social science
Project Managers : Fanny Bontemps et Sandrine Fournis 
(Chat)[https://chat.communecter.org/direct/Equipe-nationale-CTE]

(Pacte de transition)[https://www.pacte-transition.org/]
Project Managers : Aliette Lacroix
(Chat)[https://chat.communecter.org/direct/AlietteCTC]

(Marine Biodiversity Community and Observatory)[https://www.communecter.org/costum/co/index/slug/sommom#dashboard]
Project Managers : Emmanuelle Landes
(Chat)[https://chat.communecter.org/direct/aelandes]

Video introducing the team and idea.

This video is from 2017 and introduces COMMUNECTER.ORG
(Link)[https://vimeo.com/133636468]

This video is from 2014 when the project was called Human Pixel
but remains a good idea of what we are doing 
(Link)[https://vimeo.com/73771864]

# How did you hear about Mozilla Builders?

Our communities send us the information , they heard about it at Founderly.

Anything else? 
We think that being at the right time, in the right place with the right people is the best way to do the good thing.

Mozilla partnership is certainly the kind of one we dream of, those who respect privacy, develop commons and open source software and tools for freedom and open information. 

The time is really right now as many years of work pay off with CO.OP.Tools project launching and the primary NGO evolving in a cooperative society.

So Mozilla Builders incubator is clearly the right place for us to be in order to contribute to fix the internet and our societies !


