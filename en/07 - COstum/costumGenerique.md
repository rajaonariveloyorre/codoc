
# Costum Générique 
Sont des costums réutilisable et configurable 
donc on par d'un costum existant 

## cocity
rattaché à une commune, une approche ultra locale de connaissance 
- quelle thématique ou tag sont actif dans ma commune 
- creer du lien avec d'autres citoyens
- voir et référencer 
    - les organisations locale 
    - les evennements 
    - les projets
    - les compétences
    - les tags locaux 
- un journal d'activité 
- systeme de proposition locale
[doc](https://codimd.communecter.org/pswmskt0Rcew4uu5MwZ0qQ#) 

## smarterritoire 
rattaché à un territoire et à une ou plusieurs filière ce costum permet d'ouvrir des territoires qui ont l'objectifs de mobiliser des communautés autour de leur thématique d'action pour généré de la connaissance, du lien et un observatoire dynamique de ce territoire 
- s'inspire de 
    - cocity
    - filiere

## pacte
un costum fait pour une dynamique citoyen autour d'une election et des candidats

## filiere
un costum fait pour créer du lien dans une communauté
une base de connaissance autour d'une thématique 

## community 
un costum juste pour lié des gens autour d'un sujet 

## tag driven costum 
un costum qui unifie toute l'information autour d'un ou plusieurs hashtag

## aide besoin compétence 
un costum pour faire remonter les besoins aides compétences

## cv 
génère une visualisation du contenu CV d'une personne 

