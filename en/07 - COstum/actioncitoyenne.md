# Plateforme d'action citoyenne

## Objectifs
Nous permettons aux membres d’une communauté (et aux citoyens curieux) d’avoir une vision d’ensemble de ce qu’il se passe dans un réseau : où sont les membres ? que proposent-ils sur leurs territoires respectifs ? agenda commun ? …

L’objectif est également de développer l’entraide entre les membres d’une même communauté : discuter via des messageries communes, interagir dans le fil d’actualité, s’échanger du matériel et des compétences, …

Enfin l’intérêt d’avoir une plateforme dédiée à sa communauté est de pouvoir communiquer à l’extérieur, et de transmettre des informations à ces membres.


## Principes
Le COstum “Action Citoyenne” référence tous les membres de la communauté. Ensuite libre à chacun de ces membres de créer des publications, partager des évènements, déclarer ces compétences, …

Le visiteur lambda pourra ainsi accéder à ces informations :
- La carte et le moteur de recherche permettent de retrouver les membres.
- L’agenda partagé affiche les évènements du réseau.
- L’agora permet de voir les prises de décision.
- Le gestionnaire de fichiers et d’URL met en partage les documents importants.
- Le fil d’actualité affiche les publications des membres.

_Bien sûr le COstum peut ne pas afficher certaines de ces fonctionnalités._


## Exemples
### [Pacte pour la Transition](https://www.pacte-transition.org/)

Le Collectif pour une Transition Citoyenne réunis 27 mouvements partageant la même vision d’une transition écologique, sociale et humaine. Pour les municipales ils ont créé le Pacte pour la Transition (32 mesures concrètes pour des communes plus écologiques et plus justes). Le COstum [https://www.pacte-transition.org](https://www.pacte-transition.org) affiche :
- une carte des groupes locaux que l’on peut rejoindre
- la liste des mesures choisie via l’agora
- un espace “Ressources”
