# Évènements où Communecter a été présent
- Janvier
    - [FLOSSCon](https://blog.flosscon.org/)
    - [Festival des libertés numérique](https://fdln.insa-rennes.fr/)
- Mars
    - [Libre en fête](https://libre-en-fete.net/)
    - [Biennale villes en transition](http://villesentransition.grenoble.fr/)
    - [Rencontres nationales de la participation](https://www.rencontres-participation.fr)
- Avril
    - [Journées du logiciel libre](https://www.jdll.org/)
- Juillet
    - [Forum des usages coopératifs](http://forum-usages-cooperatifs.net)
- Septembre
    - [Numérique en commun](https://www.numerique-en-commun.fr/)
    - [Rencontractées](https://www.laquincaillerie.tl/tag/rencontractees/)
- Octobre
    - [Brest en commun](http://www.brest-en-communs.org)
