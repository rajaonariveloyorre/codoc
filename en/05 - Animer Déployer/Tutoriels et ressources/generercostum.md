# Générer un COstum

## Créer un élément
Rendez-vous sur la plateforme **[COstum des costums](https://www.communecter.org/costum/co/index/id/costumDesCostums).**
Puis allez dans la section “Expérimenter” et cliquer sur le bouton “Vous avez une idée de costum” qui se trouve être dans “Votre idée”.

![](/Images/costum-fiche-element.png)

Remplissez le formulaire avec les informations concernant l’élément pour votre future COstum.
Vous serez redirigé ensuite vers votre page élément.

Il est possible qu’aucune redirection ne soit faite, vérifier si votre costum se trouve bien l’annuaire des costums qui se trouve +. Dans le cas contraire, veuillez réessayer et si cela ne fonctionne toujours pas, veuillez contacter un membre de notre équipe.
![](/Images/costum-idee.png)

Vous aurez le choix entre décrire votre COstum dans le cas où vous souhaiteriez créer votre COstum par notre équipe ou de tester un COstum générique, pour tester un COstum générique, cliquer sur le bouton “Testez des COstum existants” .

Vous serez ensuite redirigé vers une page qui vous proposera tous les COstums génériques disponible.
![](/Images/costum-liste.png)

Vous pouvez tester les COstums en cliquant dans l’un des COstums proposer.
![](/Images/costum-template.png)

Si un COstum vous plait, cliquer le bouton “Save This Template” pour récupérer le COstum ou bien cliquer le bouton “Try another template” pour essayer un autre COstum générique


## Configurer votre COstum générique
Lorsque vous avez récupéré un COstum générique, une nouvelle interface va apparaît en haut de votre COstum. Vous aurez le choix entre 4 boutons:
![](/Images/costum-outils.png)

-   Preview : Avoir un aperçu de votre COstum , cela fera disparaît la barre de configuration, si vous souhaiterez pouvoir accéder à la barre, recharger la barre de votre COstum .
-   Element : Affiche la page de votre élément, cela vous sera pratique pour modifier son logo ainsi que sa bannière et des informations qui seront propres.
-   CSS : Le menu de personnalisation de couleur de votre COstum .
-   App : Le menu pour activer de vos modules d’application ou bien créer vos pages statiques


### Personnalisation CSS
Pour changer les couleurs de votre COstum, cliquer sur le bouton “CSS” qui se situe en haut de votre COstum, un menu va apparaître vous proposant le choix de vos couleurs ainsi que son emplacement.
![](/Images/costum-config-css.png)

-   Menu Top : Change la couleur du Menu Top (noté que cela change aussi la couleur des autres barres dans le menu COstum générique Candidat) ;
-   Loader ring 1 : La couleur de la barre chargement supérieur ;
-   Loader ring 2 : La couleur de la barre chargement inférieur ;
-   Menu App : La couleur de la barre des menus affichés à gauche de l’écran (ne fonctionne pas sur le COstum générique Candidat)
-   Menu App Button : Couleur des boutons
-   Progress : La couleur de la barre de progression

De nouvelles fonctionnalités concernant le CSS sont en cours de développement et arriveront prochainement.


#### Personnalisation des modules applications

Vous pouvez choisir de désactiver ou non certains modules d’application.
![](/Images/costum-config.png)

-   Activer la recherche : Permet d’activer la recherche
-   Activer le live : Permet d’activer le fil d’actualité
-   Activer les annonces : Permet d’activer les annonces
-   Activer l’agenda : Permet d’activer l’agenda
-   Activer le module sondage : Permet d’activer les sondages
-   Activer la map : Permet d’activer la carte
-   Page statique(s) : Permet de créer une ou plusieurs pages statiques

Si vous souhaitez activer un module l’application, vous n’avez qu’a cliqué sur “Oui”, les modules d’application se basent sur des “pré-définis” basant sur ce les COstums de base.

Vous pouvez créer votre page statique en lui donnant un nom (dans la section Titre) et d’une icône (dans la section icon). Les icônes sont gérées par le plugging fontawesome 4, vous pouvez retrouver les noms des icônes sur [Font Awesome](https://fontawesome.com/v4.7.0/icons/).

Vous pouvez à tout moment modifier les modules d’application en cliquant sur le bouton “App”.


## Personnaliser votre page statique
![](/Images/costum-static-page.png)

Rendez-vous sur votre page statique et cliquer le bouton créer du contenu.
![](/Images/costum-cms-data.png)

-   Titre de la section : Le titre de la section qui sera en rapport avec votre contenu
-   Who is carry this point : ne pas toucher
-   Description longue : Le contenu de la section, vous pouvez utiliser du markdown (plus d’informations sur le markdown en cliquant [ici](https://michelf.ca/projets/php-markdown/syntaxe/))
-   Document associé : Ajouter des documents, vos documents doivent faire au maximum 5 mo, la liste des documents que vous pouvez importer : `pdf,xls,xlsx,doc,docx,ppt,pptx,odt,ods,odp,csv,png,jpg,jpeg,gif,eps`
-   Structure et Hierarchie (parent ou parent.enfant) : ne pas toucher

Vous pouvez ajouter autant de section que vous voulez.
![](/Images/costum-accueil.png)

Vous pouvez modifier le contenu à tout moment en cliquant sur le bouton “Modifier” ou bien supprimer le contenu en cliquant sur l’icône “Corbeille”


## Modifier votre logo/bannière et autres informations
Vous pouvez modifier votre logo et bannière depuis la fiche élément, rendez-vous sur la page de votre fiche élément en cliquant sur “Element”.
Pour ajouter une bannière, cliquer sur “Ajouter une bannière”.
![](/Images/costum-header-element.png)

Pour ajouter un logo, cliquer sur “Ajouter une photo”
![](/Images/costum-ajout-photo.png)

Vous pouvez modifier d’autres informations concernant votre fiche élément depuis l’onglet “A propos” si vous souhaitez apporter plus de précision pour la plateforme COstum des costums et Communecter.


## Connecter votre COstum générique à un DNS (à venir)
Prochainement, vous allez pouvoir vous connecter votre COstum à un DNS.
Si vous souhaitez connecter un COstum à un DNS, veuillez nous contacter.


## Signaler un bug sur mon COstum générique
Si vous remarquez un dysfonctionnement sur votre COstum générique, vous devrez nous contacter rapidement depuis le canal [#cobugs](https://chat.communecter.org/channel/cobugs) et de nous décrire le bug afin que notre équipe puis le corriger le plus rapidement possible.
