# Cartopartie
## Exemple de présentation
> _Communecter est à la fois un moteur de recherche territorial, un réseau social et un outil de gouvernance horizontale. Il permet de recenser toutes sortes d’évènements, de petites annonces ou de réseaux d’entraides dans une dimension locale. Les utilisateurs indiquent un ou plusieurs territoires (ville, département ou région) ce qui permet d’avoir une vision localisée du contenu : agenda local, fil d’actualité local, annuaire local, petites annonces locales, etc._

[_+ de détails sur l'outil_](https://linuxfr.org/news/communecter-outiller-et-federer-les-initiatives-locales-e777ecb8-6265-47d2-afc6-fe7a3a87fb48)

Nous proposerons aux visiteurs de la RMLL de cartographier leur écosystème sur toute la durée des RMLL (accès libre). Nous souhaitons projeter en direct le résultat de cette cartographie afin de valoriser les initiatives locales, et faire découvrir leur territoire de manière dynamique grâce à cet outil libre.

Nous proposons également aux organisateurs de réfléchir à la manière dont on pourrait valoriser cette RMLL. Pour Alternatiba Péi nous avions mis l'ensemble du programme sur Communecter, et géolocaliser tous les points importants (espace associatif, concert, toilettes, compost, merchandising, etc...).

Résultat ici : [Alternatiba Péi 2017](https://www.communecter.org/#@alternatibaPei2017.view.directory.dir.events)

Exemple de cartographie indépendante et intégrable en iframe sur n’importe quel site : [villages Alternatiba](https://www.communecter.org/network/default/index?src=https://gist.githubusercontent.com/Drakone/f796739e9a08fe335c7d200b4b6b8ccd/raw/villagesalternatiba.json)


## Organiser une cartopartie
### Exemple de OpenStreetMap
<iframe src="//www.youtube.com/embed/nwHlAw6R5GM" width="830" height="465" allowfullscreen="allowfullscreen"></iframe>
