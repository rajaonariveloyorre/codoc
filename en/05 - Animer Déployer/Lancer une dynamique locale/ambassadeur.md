# Rôle d'ambassadeur de Communecter

L'outil COmmunecter se construit grâce à l'implication de nombreuses personnes : développeurs, communicants, testeurs, donateur·e·s... Ils font grandir cet outil permettant de nous mettre en réseau localement. Mais ce travail ne serait rien sans le travail effectué par nos ambassadeurs de terrain.

> La communication massive joue un rôle important dans l'appropriation de l'outil, mais les ambassadeurs auront toujours plus d'impact que la plus belle newsletter du monde.

En relation direct avec les acteurs du territoires, ils proposent, discutent et font remonter les besoins des citoyen·ne·s de nos communes.

L'association Open Atlas met en réseau et accompagne toutes les personnes souhaitant utiliser COmmunecter localement. N'hésitez pas à vous présenter dans le [**groupes dédié**](https://www.communecter.org/#page.type.organizations.id.5ac7509a40bb4e0823174499) aux ambassadeurs.
