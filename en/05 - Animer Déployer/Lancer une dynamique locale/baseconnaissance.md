# Une base de connaissance locale

En construction
- Introduction / Pourquoi ?
    - Mettre en open data des données qu'ils ne le sont pas
    - Réutilisation par des acteurs de la recherche ou des collectivités
- Trouver des données qui ont de la valeur
    - Travaux de recherche
    - Communauté des communs
        - Possibilité d'évaluer
- Importer des données (listing d'asso)
- Organiser une cartopartie
    - Dans un EPN
    - Dans le cadre d'un évènement
    - Gamifier
        - Vidéoprojeter
        - Concours ?
    - Convivialité
        - À manger et à boire
- Bases de données ouvertes - open data
    - Interopérabiliser

Alimenter une base de données c'est bien mais encore faut-il que les habitants s'en empare et la mette à jour eux-même.
