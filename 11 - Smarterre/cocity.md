# COCity
- Un Territoire citoyen et connecté
- Un Costum générique localisé (commune) portail de mouvement citoyen (un notre monde, solaris,...etc)
  + interconnect les mouvement entre 
  + une personne peut etre dans plusieurs mouvement 

## Docs
- CODI https://codimd.communecter.org/pswmskt0Rcew4uu5MwZ0qQ?edit
- PPT https://docs.google.com/presentation/d/1e-ozTx2dV3QXTiqwWk83xnZqNdHSm_N4quUAUj6DEtk/edit#slide=id.g45d7c51b41_0_146

## V0.1
- [x] pouvoir ouvrir une pour une ville 
- [x] pouvoir créer des éléments pour la ville 
- [x] voir l'annuaire localiser sur la ville des éléments(orga, projet, events, ressource, besoin, service , compétence)
- [x] pouvoir créer une filière
- [x] thématique rpédéfinie
- [x] thématique libre permettra de connecté les tags des orga et projet existant 

## V0.11
- [x] analyser la DB de prod pour voir tout les ville qui existe avec plus de 10 éléments
- [x] DatamigrationController::analyseByPostalCode en php , avec des parametres , ex : create cocityOrganization (qui est un ssaveElement)
dans la foulée on pourra connecter les organizations existante et les projets, et les tags pour initialiser les filières avec toutes la data existante 
- [x] générer leur CoCity costum 
- [x] préparer les parametres à envoyer à Element::Save pour générer une organisation pour une ville en fonction des address.postalCode issus de l'analyse

## V0.2
- [ ] voir les compétences et les sujets des acteurs locaux 
- [ ] coform de connaissance locale 
- [ ] cobservacity cobservaterritory
    - voir l'activité de la 
    - nuage de tout les mots clefs locaux par habitant 
    - les filières en chiffres
    - chercher des sources de données ouvertes
        - https://data.rennesmetropole.fr/explore/?sort=modified
        - https://ourworldindata.org/co2-and-other-greenhouse-gas-emission
idée inspiration 
- https://crater.resiliencealimentaire.org/diagnostic.html?idTerritoire=C-19272
![](https://codimd.communecter.org/uploads/upload_915aed68a60a370c390ff71256d4240c.png)
- https://xd.adobe.com/view/85fc35ce-ac4e-4ecc-8b0f-b58a3df5709e-d0af/?fullscreen


## v0.25
- [ ] choose template on create cocity
    - WTF : https://xd.adobe.com/view/85fc35ce-ac4e-4ecc-8b0f-b58a3df5709e-d0af/?fullscreen
    - un notre monde 
- [ ] Intégré les quartier dans le moteur
 https://docs.google.com/document/d/12bO0BGOy-YBboe2AXSTwJen7iNp6y4EGDa3fRkfoUk4/edit#
- [x] coCountry : la view pays connectant tout les cocitys d'un meme pays ensemble 
- [ ] coCityForm : 
    - Liste de Form actif 
    - https://xd.adobe.com/view/85fc35ce-ac4e-4ecc-8b0f-b58a3df5709e-d0af/screen/53636bb5-bc2f-4c29-bf48-a4cfc3b79ae0/?fullscreen

## v0.3
- [ ] enregistrer un favoris (tag list)
    - [ ] etre informer sur l'activité de celui ci 
- [ ] préparer un dynamique d'information pour informer les habitant de la cocity 
    - recolte des besoins
    - des faiblesses 
    - des projets
    - bouton d'aide 
- [ ] systeme d'invitation (de voisins) virale gamifier
- faire lel lien avec une structure type de site de la ville 
    - https://www.saint-leu-la-foret.fr/lecole-de-musique.htm
- [ ] cocity join : link
      ask where are you > location ?
      interest : select categories ? 


# WISHLIST
- [ ] coNNect : permet de selectionner et connecté une liste de cocity
- COzette 
- [x] pouvoir utiliser un autre cocity comme modèle de template 
  
## Diagnostic 
### ecologique local 
- co form pour analyser 
    - les projets 
    - les ambitions (CTE) 
    - les actions locales (CTE)
    - les engagements politiques locaux (PACTE)
        - https://ourworldindata.org/co2-and-other-greenhouse-gas-emissions#current-climate-policies-will-reduce-emissions-but-not-quickly-enough-to-reach-international-targets
### filière local
- CoSINLUI 
    - Liste des acteurs principaux
    - Diag Citoyen : Parcours Citoyen
        - je sais faire 
        - j'aimerais faire
        - je reve de faire 
    - Diag de la Situation actuelle par les acteur
    - Diag Employeur 
        - satisfait 
- Evaluation neutre et/ou anonyme
# comm
  [ ] diffusion de pair à pair 
    [ ] vos amis 
    [ ] vos collègues
    [ ] les faiseurs : autour de vous ceux qui font des choses
    [ ] vos concitoyens
  [ ] coambassadeur 
  [ ] Groupe Pacte 
  [ ] France Inter , Freedom
  [ ] Partager c sympas 
  [ ] Boite à lettre locale
  [ ] Affiche 
  [ ] contacter toute les plateformes à se joindre  
    [ ] plateforme add system, request interoperability
    ensemble créer un virus sociétal
  [ ] [[https://www.facebook.com/groups/2584954295126995/permalink/2711025099186580/]]
  [ ] [[https://empreintes-citoyennes.fr/]]

## La Plateforme Politics Innovated
  :laPlateforme: :politics: un projet de société utopique 
  [ ] slogan
    - take back society 
    - we don't serve a party, we are the people
    - 250years of human destruction, it's time we took care of eachother, connect, discover and make better 
    - togeterre we are smarterre
  [ ] no more old school politics, just people and a platform
  [ ] constant decisions, move the decision process and money distribution
  [ ] everything digital project calls 
  [ ] full transparency 
  [ ] real holoptism and organized group movements for action not for power
  [ ] a structurer ... ??
  nous ne sommes pas un parti mais une plateforme 
  tout nos decisions passe par un processus démocratique 
  avec tout les outils 
  :complement: {{tour de l'ile politique}}


# Section valorisation /activité locale
trouver un design 

## mettre en avant
- events
- appel à bénévolat 
- les projets phares
- propositions
- les besoins
- les compétences
- les service

## notification
- enregistrer un favoris (tag list)
- une organisation
- un territoire 
- une filiere 
- une section ci dessus

# slogan
- Quels élus sont pret à expériementer la transparence totale
- recipro-city : capacity of a city to create for the commons
- proxicity : capacity of a city to create local links 
- dispositif Viral pour un impacte local 
- citoyens tous maire !!
- agir en commun
- une commune a t elle vraiment besoin d'élu ? 
- une commune, géré par des citoyens connectés, est ce possible ? experimentons !!!
- créons ensemble les raccourcis qui iront plus vite et plus loin
- ce que vous ne voulez plus 
- partager ce que vous aimeriez 
- cocity : collaborative, connecté, consciente, connaissance 
- Faire émerger des solutions collectives, permettant de conduire des transitions ecologiques et sociales, vers des territoires durables, flexibles, resilients et inclusifs. 
- pour ceux qui aurait aimer reflechir au premiere regle. 
- pourquoi négocier nos modes de vie, vivons une alternative
- vers un référendum d'initiative citoyenne issu d'une Constitution sociale et sociétale  populaire

# Slideshow , Phrase clefs
- démarrer votre cocity en 1 click 
- innovons la facon de vivre et participer au fonctionnement de nos communes 
- construisez votre commune connecté, transparente, interactive et souveraine 
- augmentez la connaissance par des questionnaire permanent 
- base de connaissance apprenante
- partagez vos savoir faire, partagez les localement et entre territoire
- soutenez ou questionnez les élus sur leurs projets
- organiser les services, compétences, besoins et domaine d'expertise de votre commune 
-  connecté tout les filières entre les commune 
-  si toute COCity alors CORegion 

# features
- People 
- Mes Service 
- Mes Competence 
- Mes Ressource 
- Entraide 
  - Mes craintes, mes probelemes
  - Mes Besoins
  - Mes solutions
- J'ai une organisation Local ou un group d'interet partagé
- J'ai un Projet
- Je fais un Event  
- Coszette : Journaliste actu local 
  [ ] actu participative :form:
  [ ] regrouper les bookmarks d'une actu dans gallery bookmark
- outil d'Entraide -> Classifieds 
  [ ] production local
[ ] Creer une zone locale 
  url unique par zone.slug RE_07427_Lagune :form:
  - container activity
    organisation Local
  - creer une orga 
  - easy invite :input:
[ ] Easy Viral Invite your neighbour :btn:

* vision globale local
  [ ] tout les projets, events, orga
  [ ] tout les services et les sujets surlesquels ya deja en mvt
  [ ] présentation de la COzette 
  [ ] voir les bookmarks du groupe 

# Btns
- outil d'impacte local 
- projet pourri


# Cocity Doc == smarterre
Fiche apprenante pour faire des territoires intelligents  
Veille permanente d'actions locales et internationales

# bla bla
en partenariat avec RENCONTRE ENTRE TERRITOIRES EN TRANSITIONSOu comment se travaille la coopération entre communes et intercommunalités, entre communes et société civile?
Comment faire évoluer nos modes de vie et porter les transitions «là où nous vivons»? Quelle nouvelles formes d'ingénieries se dessinent? 
En réponse auxenjeuxécologiques et démocratiques, sanitaires et sociaux,nous allons devoir «négocier nos modes de vie»: nos manières d’habiter, de manger, de se déplacer et, plus généralement, de produire et  de  consommer.  Si  certaines  dispositions  relèvent  du  niveau  national  et  européen,  de  nombreuses réponses peuvent être apportées au niveau régional, départemental, intercommunal et communal.
Le rôle des territoires locaux apparaît donc de plus en plus important pour porter ces transitions.  Mais  ce  portage  est  lui-même interrogé par la difficulté de coopérer, que ce   soit   entre   les   territoires   eux-mêmes (communes/intercommunalité)   ou   entre collectivité  et  société  civile(entreprises, agriculteurs,  associations),  et  ce  dans  un contexte de forte  défiance  envers  notre démocratie représentative, nos institutions, voire de plus en plus «l’autre»

# Desc generique
Votre territoire est un espace d'interaction Locale (citoyens, la biodiversité, acteurs socio-économiques)
~~La collectivité constitue le liant de tous ces éléments et se positionne en tant que facilitateur des interactions.~~~~La collectivité constitue le liant de tous ces éléments et se positionne en tant que facilitateur des interactions.~~~~La collectivité constitue le liant de tous ces éléments et se positionne en tant que facilitateur des interactions.~~
C'est un espace où on peut (se) poser des questions librement et construire une vision partagée afin d'apporter des solutions en commun.

~~Pourquoi diviser une commune par un système de vote?~~
Soutenir les élu.e.s dans leur rôle de facilitateur.rice des dynamiques locales qui font converger les acteurs vers des objectifs communs
~~Pourquoi donner le pouvoir à un roi local ?~~
Accompagner les grands enjeux de résilience des collectivités en facilitant l'adhésion et l'action de chacun à travers la visibilité de l'impact local de ses activités.
~~Innovons la facon de vivre et participer au fonctionnement de nos communes 
Partagez vos savoir faire, partagez les localement et entre territoire ~~

un système vivant où toute vie y est pleinenemnt respecter


Votre territoire est un espace d'interaction Locale (citoyens, la biodiversité, acteurs socio-économiques)

La collectivité constitue le liant de tous ces éléments et se positionne en tant que facilitateur des interactions. 
C'est un espace où on peut (se) poser des questions librement et construire une vision partagée afin d'apporter des solutions en commun.

Soutenir les élu.e.s dans leur rôle de facilitateur.rice des dynamiques locales qui font converger les acteurs vers des objectifs communs
Accompagner les grands enjeux de résilience des collectivités en facilitant l'adhésion et l'action de chacun à travers la visibilité de l'impact local de ses activités.

un système vivant où toute vie y est pleinenemnt respecter

---

# cocity 
## la commune par les communs 
identifions tout ce qui est coConstruit 
Construisons en communs et en partage pour le respect de l'homme et de la nature.
Expérimentons nos communes autrement 
Proposons, Décidons et Financons en transparence et surtout mesurons les résultats






--- 

Notre confiance repose sur une election de qlqun qu'on ne connait pas 
et qui in fine fait tout ce qu'il veut avec les moyens qu'on lui donne 
il faudrait à minima un test psychologique d'intégrité, d'humanité, d'empathie 
quel est la marge inverse 



                        ------------------------
                        <                      >
                        <  Tranquilité au      >
                        <  Travail Perso et    >
                        <  collaboration libre >
                        <  sans contrainte     >
                        <                      >
---------------------   < -------------------- > -----------------------------
<                       <                      >                             >
<     Collaboration     <                      >   Organisation Horizontal   >
<     Échange           <                      >   en commun et Réciprocité  >
<                       <                      >                             >
------------------------------------------------------------------------------

-----------------------                          -----------------------------
++ certitude, division                           ++ lacher prise, liberté, 
-- d'ecoute, patience,                           -- attention, de suivi, discipline
    prendre le Temps
    serendipité
-----------------------                          -----------------------------

syst election          |                        | valorisation local par les habitant 
confiance de l'inconnu |                        | des like dans la durée
                       |                        | tirage au sort






---

objectif : créer un territoire citoyen 



                        ------------------------
                        <                      >
                        <                      >
                        <                      >
                        <                      >
                        <                      >
                        <                      >
---------------------   < -------------------- > -----------------------------
<                       <                      >                             >
<                       <                      >                             >
<                       <                      >                             >
<                      <                      >                            >
------------------------------------------------------------------------------


-----------------------                          -----------------------------
++ dé responsabilisation                          ++ Tout à réimaginer, rééduquer 
++ division locale                                ++ bcp à faire 
-- liberté                                        -- On Manque de temps pour etre citoyen
-- d'espace ouvert pour l'inplication             -- 
-----------------------                          -----------------------------


collectivité           |                        | Territoire citoyen 
élu locaux             |                        | smarterre (collèges d'experts et votation permanente)