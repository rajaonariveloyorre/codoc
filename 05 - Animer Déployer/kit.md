# Kit ambassadeur

## Documents
-   [Plaquette de présentation de COmmunecter](https://cloud.co.tools/index.php/s/Lyf3tBGi3PCmPWi)
-   [Présentation de COstum](/6 - COstum/costum.md)
-   [Cartes de visites](https://cloud.co.tools/index.php/s/dDkDJxZ6xcm9XjL)

## Goodies
Envoyez-nous un mail pour en recevoir : [contact@communecter.org](mailto:contact@communecter.org).
-   T-shirt
-   Petits encarts

## Ressources complémentaires
-   [Le logiciel libre, comment ça marche ?](https://www.april.org/depliant-le-logiciel-libre-comment-ca-marche)
