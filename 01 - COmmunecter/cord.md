# Le Collectif CORD

Ce collectif informel CORD promeut l’intelligence collective et l’initiative citoyenne en militant pour la conjugaison de l’éthique et de l’efficacité, autour de l’économie numérique et en œuvrant dans le principe de développement soutenable, durable et solidaire.

CORD est à l'origine de : 
- COmmunecter et assure l'évolution et la survi du commun numérique.
- des COstum
- de OCECO
- des COForms et CObservatoires
- de SmarTerre et SmarTerritoire
- de COCity
- de la dynamique COEUR 

C.O.R.D (CO & Open R&D) regroupe des acteurs du numérique (web, mobile, design, objets connectés). Elle est spécialisée dans les modes de communication innovants et défend depuis toujours l’innovation et la philosophie Open Source comme vecteurs de Collaboration Massive.

## Savoir Faire

CORD regroupe une communauté de talents et d'expertises, bénéficiant de retours d’expérience éprouvés. Son objet est d'accompagner les entreprises et associations dans leur démarche d'innovation. Un clusters dédié à la recherche et au développement sous une forme décentralisé aux multiples talents :

-   Développement de solutions numériques WEB
-   Création Graphique (logo, direction artistique)
-   Animation : Barcamp / Atelier collaboratif + créatif (Brainstorming, Serious game, Hackathon…)
-   Formations et Tutoras: outils collaboratifs, organisation projet, développement web, design…
-   Hébergement (hosting cloud ou dédié)
-   Animation Vidéo (design, motion...)
-   Installation et animation digitale (design intéractif)
-   Conseil en innovation, collaboration, outillage filière
-   Cartographie (SIG : Système d’information géographique)

## Nous construisons

Nous construisons une boite à outils citoyens (CTK Citizen Tool Kit) systémique avec plusieurs objectifs et impact positif :
- structuration territorial
- l'action citoyenne
- faciliter la création de filière 
- dynamique de gouvernance horizontale
- l'emergence de la connaissance dynamique et observable
-   SIT : Système d'information territorial

## Philosophie
- Open Source Everything 
- (Open System)[codesocial.md]
- Agilité
- Honeteté
- Toujours Évoluer 
- Joie et s'amuser
- Transparence 
- Permaculture Digitale
- Innover
- Diversité 
- Collaborer
- Equilibre Vie Travail
