# L'association Open Atlas

En savoir + : [site internet](http://open-atlas.org/)
Depuis sa création en 2008, l'association Open Atlas travaille sur des projets liés aux biens communs, à la cartographie et à la démocratie participative. C’est une association locale qui œuvre au développement territorial à La Réunion.


## Présentation
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTT2ROnz906h3L0WoRosAi-VpT3VrP98wOLv2dQYiuu8lEt8ZU8h6HU08QCbcGjzQ/embed?start=false&amp;loop=false&amp;delayms=3000" width="830" height="495" frameborder="0" allowfullscreen="allowfullscreen"></iframe>


## Activités
-   Développement et formation en logiciel libre
-   Conseil en développement territorial
-   Aide et soutien aux organisations à impact positif
-   Cartographie de filière
-   Hackathon
-   ...


## Organisation
### Bureau
L’association est composée d’un Conseil d’Administration qui a élu parmi ses membres :

- Un Président : ...
- Un Secrétaire : ...
- Un Trésorier : ...

Des salariés gèrent au quotidien la vie et les projets en expansion de l’association. Une équipe de développeurs indépendants mettent en œuvre ces projets afin d’atteindre les objectifs fixés dans les statuts.

### Équipe de travail
-   4 Développeurs indépendants confirmés et expérimentés
    -   Tibor KATELBACH, Développeur, Gérant bénévole (St-Pierre)
    -   Clément DAMIENS, Développeur (Métropole)
    -   Raphaël RIVIERE, Développeur (La Possession)
    -   Thomas CREPEAU, Développeur (Itinérant)
-   2 Développeurs junior en acquisition de compétences :
    -   Pierre GOUBEAUX, Développeur (St-Pierre)
    -   Tony EMMA, Développeur (St-Pierre)
-   2 Graphistes parallèlement en formation continue à l’ILOI :
    -   Agatha CONRAUX, Graphiste Designer (La Possession)
    -   Ambre COTONEA, Graphiste Designer (La Possession)
-   1 Animateur réseau local : Tom BAUMERT (Métropole)
-   1 Community Manager : Jeanne HENRY (St-Pierre)
-   1 Assistante Administrative : Marie-Jean BEAUPAIN (St-Pierre)

Nous accueillons régulièrement plusieurs stagiaires en développement logiciel, graphisme, techniques commerciales… Nous proposons également des contrats Service Civique afin de promouvoir la filière numérique et la professionnalisation des jeunes.

### Lieux
Nous disposons de 2 bureaux :
-   Le KiltirLab situé au 466 chemin Badamier Bois d’Olive, 97410 St-Pierre
-   Ansellia – TCO situé au 15 rue Bois Joli, 97419 La Possession

## Réalisations
### Développement de COmmunecter
-   Espace coopératif
-   Chat de discussion
-   Albums photos et librairie
-   Traduction anglaise et allemande
-   Nouvelle interface
-   Moteur de recherche unifié
-   Visualisation en graphe
-   Module d’entraide
-   Sondages et propositions multi choix

L'internationalisation permet à Communecter d’être utilisé partout dans le monde. Le nombre d’utilisateurs est en hausse et plusieurs réseaux commencent à s’approprier l’outil.

### CObus
Découvrir ce projet sur COmmunecter : [CObus](https://www.communecter.org/#page.type.projects.id.596db02e40bb4e6b297b23c7)

Bus connecté : un véhicule acquis par l’association permet à l’équipe de se déplacer sur l’île
dans le cadre de ces actions de lutte contre la fracture numérique.

### Contrat de Transition Écologique
Mise en place d’une plateforme collaborative pour le déploiement des contrats de transition
écologique : information, dialogue entre les territoires, réception des candidatures.
Cette plateforme comprend :

-   une page avec un formulaire pour la réception des candidatures aux CTE (inscription, formulaire, base de données candidatures)
-   une page répertoire des informations sur les CTE (CTE signés, documents, ressources, parcours utilisateurs…)
-   forum de discussion


### SmarTerre
SmarTerre est un dispositif territorial, numérique et scientifique au service de la transition écologique sous toutes ses formes : développer l’économie, soutenir le développement humain et préserver l’environnement.

Rencontre entre 3 initiatives :

-   COmmunecter : plateforme collaborative pour rendre les territoires intelligents et interconnectés.
-   Efficacity : hub scientifique et réseau national de living labs urbain pour doter la France d’un outil de recherche-action au cœur des territoires.
-   TCO : territoire porteur d’un des premiers CTE et de l’Ecocité Tropicale et Insulaire. Ouvrir un territoire d’expérimentation à tous les acteurs de l’île et démontrer la faisabilité d’un territoire tropical durable.

### Plateforme Collaborative Filière Numérique

Les objectifs de ce projet sont les suivants :

-   Proposer une plateforme collaborative et innovante dédiée à l’animation du Comité de filière numérique.
-   Donner une visibilité de la filière et des différentes actions du Comité de Filière auprès des acteurs du numérique, des demandeurs de transition du numérique et du grand public.
-   Structurer une offre de numérique centralisée sur la Réunion
-   Utiliser le numérique comme outil territorial afin de visualiser les liens entre les acteurs, les dynamiques collectives, faciliter les échanges et la co-construction de la filière.

### Site de La Possession
Création du [COstum](/6 - COstum/costum.md) de La Possession comprenant :

-   L’import de toutes les informations pertinentes locales
-   Sondage avec un questionnaire personnalisé

### COzette

Projet de média libre donnant la parole à tout le monde pour construire ensemble nos territoires de demain. Ce projet se matérialise sous 3 formes :
-   Une publication papier trimestrielle
-   Une radio libre avec des émissions thématiques, des podcasts et une grande émission de lancement pour chaque numéro papier
-   Une version numérique hébergée sur la plateforme COmmunecter

## Statuts officiels
### PRÉSENTATION DE L’ASSOCIATION
#### Article 1 : Constitution, dénomination, siège social et droit applicable
1.  Il est fondé entre les adhérents aux présents statuts une association ayant pour dénomination : « **OPEN ATLAS** » aussi appelée : « **OPAL** ».
2.  Le siège social est une dénomination postale fixée dans l’ouest de la Réunion (974). Il pourra être fixé et modifié par simple décision du bureau.
3.  L'association est régie par la loi du 1er juillet 1901 et le décret du 16 août 1901.


#### Article 2 : Objet
L'association a pour objet d'être un lieu multidisciplinaire d'échanges permettant de **développer des projets** œuvrant dans des domaines de l**’intérêt général**, notamment autour de l’**économie de l’information et de la communication**.

D’intérêt social et solidaire, l’association s’intéresse à **tous les territoires, du local à l’international.**

Elle œuvre, en toute diversité, dans les champs du **développement humanitaire, culturel, artistique, de la communication de services public ou privé, de la cartographie virtuelle, de la connectivité et la mise à disposition de l'information, de la communication** (au sein d'un groupe). **L’association se donne également pour objectif d’être un laboratoire et une cellule active de recherche et de développement, en toute transversalité, défendant le décloisonnement et le partage des compétences, ainsi que la liberté de penser et d’agir**

Open Atlas promeut **l’action et l’intelligence collective** **et l’initiative citoyenne**, militant pour la conjugaison de l’éthique et de l’efficacité, autour de l’économie numérique, œuvrant dans le principe de **développement soutenable, durable et solidaire**.

L’association concourt à la conception et à la mise en œuvre d'outils et de systèmes d’information et de gestion permettant d'atteindre les objectifs qu’elle se fixe dans les domaines précédemment mentionnés.

L'association inscrit ses projets dans une dimension d’intérêt général en s’ouvrant à tous les publics, notamment les plus fragiles, en préservant à ses activités **un caractère non lucratif,** laïque et apolitique. En toutes circonstances, l'association garantit un **fonctionnement démocratique et transparent** et préserve le caractère désintéressé de sa gestion.

#### Article 3 : Moyens d’action
1.  Les moyens d’action de l’association sont notamment :
-   les publications, les conférences, les réunions de travail ;
-   l'organisation de conventions et événements;
-   l’organisation de diverses manifestations et toute initiative pouvant aider à la réalisation de l’objet de l’association ;
-   Tout autre moyen permettant l'accomplissement de son objet.

#### Article 4 : Durée de l’association
1.  La durée de l’association est illimitée.

### COMPOSITION DE L’ASSOCIATION
#### Article 5 : Composition de l’association
1.  **Membres adhérents** : Personnes morales ou physiques intéressés par l'objet de l'association et adhérant aux statuts et à son règlement intérieur. Les membres adhérents ne s'acquittent pas d'une cotisation annuelle et n’ont pas le droit de vote à l’Assemblée Générale
2.  **Membre donateur** : Personnes morales ou physiques intéressés par l'objet de l'association et adhérant aux statuts et à son règlement intérieur. Les membres adhérents s'acquittent d'une cotisation annuelle fixée par l'Assemblée Générale. Ils sont, de droit, membres de l'Assemblée Générale avec voix délibérative.
3.  **Membres bienfaiteurs** : Personnes morales ou physiques intéressés par l'objet de l'association et adhérant aux statuts et à son règlement intérieur. Les membres bienfaiteurs s'acquittent d'une cotisation annuelle supérieure aux membres adhérents et fixée par l'Assemblée Générale. Ils sont, de droit, membres de l'Assemblée Générale avec voix délibérative.
4.  **Membres d'honneur** : Personnes morales ou physiques nommées par le Conseil d’Administration en remerciement de leur soutien ou de leur aide. Ils disposent d'une voix consultative à l'Assemblée Générale.

#### Article 6 : Admission et adhésion
1.  **L’association est ouverte à tous**. Pour les mineurs de moins de 16 ans, une autorisation parentale ou d’un tuteur sera demandé. Pour faire partie de l’association et se joindre aux activités, il faut en faire la demande. Le bureau statue, lors de chacune de ses réunions, sur les demandes d’admission présentées. Tout refus doit être motivé.

#### Article 7 : Perte de la qualité de membre
1.  La qualité de membre se perd par :
-   l'exclusion, prononcée par le Bureau, pour infraction aux statuts ou au règlement intérieur, motif portant préjudice aux intérêts moraux et matériels de l'association ou tout autre motif grave.
-   la démission du membre adressée par courrier électronique à l’adresse électronique de l’association ;
-   le décès ;
-   la radiation prononcée par le Bureau pour non paiement de la cotisation après mise en demeure préalable ;
2.  Nul ne peut se voir exclu de l'association ou privé de l’accès à ses activités sans avoir pu défendre ses droits ou pour des motifs non légitimes.

### ORGANISATION ET FONCTIONNEMENT
### L'Assemblée Générale
#### Article 8 : Composition de l'Assemblée Générale

1.  L'Assemblée Générale comprend tous les membres donateurs et bienfaiteurs à jour de leur cotisation.
2.  Les membres d'honneur sont des auditeurs de droit et ont voix consultative.
3.  Les agents rétribués de l’association peuvent être appelés par le président à assister, avec voix consultative, aux séances de l’assemblée générale.

#### Article 9 : La convocation
1.  L'Assemblée Générale se réunit au moins une fois par an. Elle peut être convoquée soit par le président soit par des membres de l'association représentants au moins un dixième des voix à l'Assemblée Générale.
2.  Quinze (15) jours au moins avant la date fixée, les membres sont convoquées par écrit ou électroniquement et l’ordre du jour, comprenant date, heure et modalités techniques de déroulement de l’assemblée, est mentionné dans les convocations
3.  Les convocations contiennent également l'ensemble des documents (sous leur forme électronique) afférant aux questions qui seront soumises aux délibérations.

#### Article 10 : Les délibérations
1.  La participation d'au moins un cinquième (1/5) des membres à jour de cotisation est nécessaire pour que l'Assemblée Générale puisse valablement délibérer. Les délibérations de l’Assemblée Générale peuvent se tenir dans le cadre de la réunion physique de ses membres ou bien à distance, l’association garantissant par des moyens techniques appropriés la transparence et le caractère démocratique des débats ainsi que la sécurité du scrutin.
2.  Les procurations ne sont pas autorisées.
3.  Chaque membre dispose d'une seule voix à l'Assemblée Générale. Un membre n'a pas le droit de vote lorsque la délibération concerne la conclusion d'un acte juridique entre lui et l'association
4.  Les décisions de l’Assemblée Générale sont prises, sauf dispositions contraires des statuts, à la majorité des voix exprimées. Le règlement intérieur organise la sécurité du processus électoral et si nécessaire l’anonymat des débats et des votes au sein de l’assemblée tenue à distance. Dans tous les cas, pour les élections au Conseil d'Administration, l’anonymat des votes est préservé.
5.  Le Président et le Secrétaire de l'association forment le bureau de l'Assemblée Générale. Le Président assure la police de l'audience et veille au respect de l'ordre du jour. Le Secrétaire rédige un procès-verbal de la séance signé par lui- même et contre-signé par le Président.
6.  En cas d'absence du Président et du Secrétaire de l'association, l'Assemblée Générale désigne un président de séance ainsi qu’un secrétaire de séance parmi les membres présents.

#### Article 11 : Les attributions
1.  L'Assemblée Générale se prononce annuellement sur le rapport moral et sur les comptes de l’exercice financier. Elle pourvoit à la nomination ou au renouvellement des membres du Conseil d'Administration lorsque des élections doivent avoir lieu. Elle fixe aussi le montant de la cotisation annuelle et, de manière générale, peut modifier le règlement intérieur.
2.  L'Assemblée Générale délibère sur les orientations à venir et la politique générale de l'association. Elle peut prendre toute décision concernant l'objet de l'association.
3.  L'Assemblée Générale est compétente pour examiner tous les points qui ne sont pas de la compétence du Conseil d'Administration et du Bureau.

### Le Conseil d'Administration
#### Article 12 : Composition
1.  L'Assemblée Générale élit un Conseil d'Administration de 6 membres pour 6 ans.
2.  Pour le premier renouvellement (2 ans après la constitution de l'association), 2 postes au conseil d'administration parmi les 6 sont tirés au tirés au sort. Pour le second renouvellement (4 ans après la constitution de l'association), 2 postes parmi les 4 n'ayant pas encore fait l'objet d'un renouvellement sont tirés au sort. Pour le 3ème renouvellement, les 2 postes restants (ceux n'ayant pas encore fait l'objet d'un renouvellement) sont renouvelés.

Lorsqu'un poste a fait l'objet d'un renouvellement, la durée de mandat de 6 ans devient effective.

3.  Les candidats sortants peuvent être candidat à leur succession.
4.  En cas de poste vacant, il est procédé au remplacement provisoire du membre jusqu'à l'Assemblée Générale la plus proche. Les pouvoirs des membres remplaçants s’achèvent au moment où devrait normalement expirer le mandat des membres remplacés.

#### Article 13 : Règles d'éligibilité
1.  Pour être éligible au poste d'administrateur, il faut :
-   être membre donateur ou bienfaiteur à jour de cotisation ;
-   être membre de l’association depuis plus d’un an ;
-   être majeur ou mineur émancipé à la date de l'élection ;

#### Article 14 : Les délibérations
1.  Le Conseil d'Administration se réunit chaque fois qu'il est convoqué par le président ou le tiers au moins des administrateurs, à défaut au moins 1 fois par trimestre. La convocation sous forme écrite ou électronique doit être adressée à tous les membres du Conseil d'Administration au moins 15 jours avant la réunion.
2.  Les délibérations du Conseil peuvent se tenir dans le cadre de la réunion physique de ses membres ou bien à distance, l’association garantissant par des moyens techniques appropriés la transparence et le caractère démocratique des débats ainsi que la sécurité du scrutin.
3.  La présence de la majorité des membres du Conseil d'Administration est nécessaire pour qu'il puisse valablement délibérer.
4.  Le vote par procuration est interdit. Le règlement intérieur organise la sécurité du processus électoral et l’anonymat des débats et des votes au sein du Conseil tenu à distance. En cas d'égalité, la voix du président est prépondérante.
5.  Un membre du Conseil d'Administration n'a pas le droit de vote lorsque la délibération concerne la conclusion d'un acte juridique entre lui et l'association.
6.  Les agents rétribués de l’association peuvent être appelés par le président à assister, avec voix consultative, aux séances du conseil d’administration
7.  Un compte rendu de chaque réunion sera produit et diffusé aux membres du Conseil d’Administration.

#### Article 15 : Attributions du Conseil d'Administration
1.  Le Conseil d'Administration est chargé :
-   de la mise en œuvre des décisions de l'Assemblée Générale ;
-   de la préparation des bilans, de l'ordre du jour et des propositions de modification du règlement intérieur présentés à l'Assemblée Générale ;
-   de la préparation des propositions de modifications des statuts présentés à l'Assemblée Générale extraordinaire ;
-   de la gestion administrative quotidienne de l’association ;

#### Article 16 : Gestion désintéressée
1.  Les fonctions d’administration et de direction de l’association sont bénévoles ; l’association préserve en toutes circonstances un caractère désintéressé à sa gestion.
2.  Lorsqu’ils prennent part aux activités, les membres dispensés de cotisation s’engagent à acquitter le prix des produits et services rendus par l’association. Les membres ont droit au remboursement des frais exposés pour les besoins de l’association, sur justificatifs.

### Le Bureau
#### Article 17 :_ Composition du Bureau
1.  Le Conseil d'Administration élit pour 2 (deux) ans un Bureau de 3 membres. Le Bureau est composé de :
-   un secrétaire ;
-   un président ;
-   un trésorier ;
2.  En cas de poste vacant, le Conseil d'Administration procède au renouvellement immédiat du poste.

#### Article 18 : Pouvoir des membres du Bureau
1.  Le président a la charge de représenter l'association et d'organiser les réunions du Bureau. Il préside de plein droit l'Assemblée Générale. Il doit tenir à jour le Registre Spécial de l'association et le garder à disposition de toute autorité administrative ou judiciaire qui souhaiterait le consulter. Il organise et préside les réunions du Conseil d’Administration.
2.  Le secrétaire est chargé de la gestion administrative de l’association. Il veille aux convocations des membres aux différentes Assemblées Générales.
3.  Le trésorier a la charge de la gestion financière de l’association.

### RESSOURCES DE L’ASSOCIATION
#### Article 19 : Ressources de l’association
1.  Les ressources de l’association se composent
-   du bénévolat ;
-   des cotisations ;
-   des subventions de l’état, des collectivités territoriales et des établissements publics ;
-   les mécénats privés ;
-   du produit des manifestations qu’elle organise ;
-   des intérêts et redevances des biens et valeurs qu’elle peut posséder ;
-   des rétributions des services rendus ou des prestations fournies par l'association (quêtes, conférences, tombolas, réunions, spectacles, tout autre produit créé, autorisés au profit de l’association) ;
-   de dons ;
-   les legs
-   de toutes autres ressources autorisées par la loi, notamment, le recours en cas de nécessité, à un ou plusieurs emprunts bancaires ou privés.

#### Article 20 : Transparence financière
1.  L’association établit des comptes sur une base annuelle, conformément aux prescriptions du règlement comptable n° 99-01 adopté le 16 février 1999, comportant les documents suivants : compte de résultat, bilan, annexe et le cas échéant le compte d’emploi des ressources.
2.  Les comptes sont tenus à la disposition des membres, sur simple demande, adressé au siège et consultables également sur le site de l’association.
3.  L’association communique aux autorités compétentes et aux dispensateurs de subventions dans les meilleurs délais les comptes arrêtés, approuvées ou certifiés conforme ainsi que les documents exigés par la réglementation.

### RÈGLEMENT INTÉRIEUR
#### Article 21 : Règlement intérieur
1.  Le Conseil d'Administration pourra établir un règlement intérieur fixant les modalités d’exécution des présents statuts ainsi que l’organisation interne et pratique de l’association.
2.  Ce règlement intérieur sera soumis à l’approbation de l’Assemblée Générale ainsi que ses modifications ultérieures.

### MODIFICATION DES STATUTS ET DISSOLUTION
#### Article 22 : Modification des statuts
1.  Les statuts ne peuvent être modifiés que par une Assemblée Générale réunie à cet effet, sur proposition du Conseil d'Administration ou du tiers des membres. Une convocation accompagnée d'une date, d'un lieu, d'une heure, modalités techniques de déroulement de l’assemblée et d'un ordre du jour à point unique détaillant la proposition est adressée à tous les membres au moins un (1) mois avant la réunion de l'Assemblée Générale.
2.  Le vote par procuration n’est pas autorisé. Les modalités de vote sont les mêmes que pour les Assemblées Générales détaillées dans l’article 10 des présents statuts.
3.  L'Assemblée Générale ne peut modifier les statuts que si la moitié au moins des membres sont présents. Si l'Assemblée n'atteint pas ce quorum, une nouvelle Assemblée Générale est convoquée au moins quinze jours après. Elle peut alors valablement délibérer quel que soit le nombre de votants.
4.  Dans tous les cas, les statuts ne peuvent être modifiés qu'à la majorité des trois cinquièmes (3/5e) des membres présents.

#### Article 23 : Dissolution de l'association
1.  L'association ne peut être dissoute que par une Assemblée Générale réunie à cet effet, sur proposition du Conseil d'Administration ou du tiers des membres. La convocation se fait selon les formes prévues à l’article des présents statuts.
2.  Le vote par procuration est interdit.
3.  L'Assemblée Générale ne peut dissoudre l'association que si la moitié au moins des membres sont présents. Si l'Assemblée n'atteint pas ce quorum, une nouvelle Assemblée Générale est convoquée au moins quinze jours après. La dissolution ne peut être votée qu'à la majorité des trois quarts (3/4e) des membres présents.
4.  En cas de dissolution, l'Assemblée Générale désigne un ou plusieurs liquidateurs qui seront chargés de la liquidation des biens de l’association et dont elle détermine les pouvoirs.
5.  Les membres de l’association ne peuvent se voir attribuer une part quelconque des biens de l’association. L’actif net subsistant sera attribué à une ou plusieurs associations poursuivant des buts similaires désignés par l’Assemblé Générale.
