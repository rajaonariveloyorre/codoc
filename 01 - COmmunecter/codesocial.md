# Code social de COmmunecter
Ce travail a été réalisé début 2016 par [Maïa](https://www.communecter.org/#page.type.citoyens.id.56744d4edd045264697b23d1).

Un travail de mise à jour est en cours. Vous pouvez y contribuer [ici](https://docs.google.com/document/d/1k1XmVgh4e5vKQozFxxEzYBL_0uWISbJCX9m0cZUhvp4/edit?usp=sharing).


### Ressources
-   [Modeler un code social](http://contributivecommons.org/modeliser-code-social/)


### Introduction
Le “#CodeSocial” de Communecter présente les différents fondements, principes et modèles qui fondent nos actions, sur les plans social et Humain, économique, écologique, technologique, juridique et financier, artistique et culturel.

Il est construit sur la complémentarité du contexte théorique et historique et de leur mise en application concrète à travers le développement du projet Communecter. C'est cette dualité qui différencie ce #CodeSocial d'une charte ou d'un manifeste.

L'idée du #CodeSocial est d'explorer la notion “d'API Juridique”. Il repose sur l’analogie au fameux « Code Logiciel » plus souvent appelé « code source » d’un programme informatique ou d’un langage destiné au web. (voir l’article publié à ce sujet par la structure auteure de ce concept : [http://cheznous.coop/codesocial-2/](http://cheznous.coop/codesocial-2/) )

Parce que le collectif qui porte Communecter est une organisation évolutive, le présent #CodeSocial est naturellement en constante évolution, c'est un document à jardiner par la communauté. Le processus de modification de ce #CodeSocial reste à expliciter (il est souhaitable qu’il repose sur une dynamique légitimée par le collectif).

Le projet Communecter étant en cours de transition entre deux structures juridiques (association Open Atlas vers SCIC non dénommée à ce jour), le présent #CodeSocial présente le projet (et porte son nom) pour tenter d’illustrer ce passage au mieux. Néanmoins, à terme, il a vocation à être celui de la future structure porteuse.

Ce code social a été validé par l’assemblée générale du ...

### L’histoire du projet
Communecter est un projet actuellement porté par l’association Open Atlas mais dont les racines se trouvent dans une quinzaine d’années d'expérimentations, d’essais, de pertes de motivation, d’espoirs retrouvés, de rencontres, d’amitiés et d'innovation permanente pour l’ensemble des acteurs qui ont rejoint peu à peu l’aventure jusqu’à aujourd’hui.

Depuis sa création en 2008, l'association Open Atlas travaille sur des projets liés aux biens communs, à la cartographie et à la démocratie participative. C’est une association locale qui oeuvre au développement territorial à La Réunion.

-   2002 - Application et Site web Open Atlas - projet open source de Service Géolocalisé réalisé par 3 développeurs à Limoges dans le cadre d’un incubateur.
-   2008 - Association Open Atlas - création de l’association loi 1901 à La Réunion (suite au déménagement du porteur de projet). Son objectif initial était de tester l’outil Open Atlas sur le territoire réunionnais selon un modèle pensé pour être distribué. Malgré un bon démarrage avec quelques milliers d’utilisateurs, le projet s’est essoufflé et l’association s’est tournée vers d’autres activités.
-   2008 - Worldsouk - projet de E-commerce d’artisanat équitable avec redistribution du profit pour construire des puits à Madagascar.
-   2012 - Pixel Humain - Communauté et écosystème de concepteurs et de projets orientés vers l’innovation sociétale, amélioration continue, efficience d’un territoire. Ateliers et boite à outils citoyens
-   2013 - Ateliers citoyens sur toutes l’île. Expérimentation sur le terrain. Création du Tool Kit Citoyen (boite à outils pour construire des applications Citoyennes).
-   2014 - FabLab.re - communauté de Fablabeurs dans le sud de la Réunion dont le 1er projet est un réseau Smart Citizen sur le territoire réunionnais, en collaboration avec le FabLab de Barcelone.
-   2015 - Le Forum des Communs - pendant le Temps des communs, l’association a organisé une journée dédiée aux rencontres et présentations d’acteurs et de projets oeuvrant pour le bien commun à la Réunion.
-   2014-2016 - Communecter - Réseau Social Citoyen Libre pour créer un territoire connecté d’acteurs et d’activité locale.
-   2017 - Projet de SCIC - Le projet Communecter s’apprête à prendre son envol dans le cadre d’une SCIC. L’objectif est de le sortir du cadre de l’association pour en faire un bien commun.


### Raison d’être, carte d’identité
#### L’activité de Communecter
Communecter est un réseau social citoyen libre : www.communecter.org.

Communecter est le résultat de plusieurs années de réflexion sur la manière de se connecter à sa commune (et aux communs) grâce aux outils numériques.

La plateforme Communecter que nous avons créée est un outil pour nos communes, nos associations, nos entreprises et bien sûr pour nous, pour nos voisins, nos amis et tout ceux qui ont quelque chose à proposer : des idées, des projets, des choses à distribuer, à donner, à partager... C’est un bien commun à partager.

C'est un réseau sociétal indépendant. Dès que nous serons assez nombreux à y contribuer il rayonnera et permettra de créer une autre forme de participation collective, celle qui permet à chacun de s’exprimer, de participer au débat citoyen et de fait à l’avenir de notre cité. Favoriser l'émergence d'autres possibles aujourd'hui pour demain.

#### Objectifs et raison d’être de Communecter
L'objectif de la plateforme est de permettre à tous les acteurs de la cité de ne plus agir chacun de leur côté mais de se réunir sur une plateforme commune. Cartographier le réseau autour de soi, découvrir ceux qui partagent les mêmes centres d'intérêt, apprendre, échanger... voila concrètement ce qu'apporte Communecter.

Dialogue en grand nombre et création de groupes de discussion, prises de décisions démocratiques orientées consensus, vote citoyen géolocalisé, participation collaborative à des événements, Communecter est un outil Open Data et Open source. Il s'agit de donner à chacun le même niveau d'information sans aucun privilège ni prix à payer, en totale liberté.

##### POUR LES CITOYENS
Être Acteur, participer à la vie de la cité, apprendre, échanger, découvrir ceux qui partagent les mêmes centres d'intérêt que moi. Créer de la valeur en participant au débat citoyen, favoriser l’émergence d'autres possibles aujourd'hui pour demain.

##### POUR LES ASSOCIATIONS
C'est un moyen fantastique de se faire connaître et d'avoir une vraie visibilité. Recruter de nouveaux membres, trouver des ressources, de l'aide, promouvoir un évènement... Économie solidaire, Fablabs, jardins partagés, biens communs. L'innovation sociale c'est aussi ça.

##### POUR LES COMMUNES
Donner du sens au mot lien social, reconnaître ses administrés, comprendre leurs attentes et leur donner les moyens de bâtir le futur. Quand les citoyens communiquent et agissent librement en partenariat avec les collectivités. La ville est un organisme vivant auquel nous pouvons tous nous connecter.

##### POUR LES ENTREPRISES
Être un acteur local au sens vrai du terme, se faire reconnaître comme un ressource en terme de service au citoyen avec un vrai respect de la qualité, quelque soit son métier. Donner de la visibilité à son activité par la force et à la richesse de la plateforme communecter.

##### UN Réseau pour tous
Communecter réunit et fédère les principaux acteurs de la vie locale pour valoriser le territoire et le bien commun.

Voir ce document pour une liste plus détaillée des objectifs : [https://docs.google.com/document/d/1eH1efbnvITa8pnloHawW-vbyQ9KSTRIL7KzAvfzSykw/edit](https://docs.google.com/document/d/1eH1efbnvITa8pnloHawW-vbyQ9KSTRIL7KzAvfzSykw/edit)


### Modèle social et humain : les relations humaines, les responsabilités, l’inter-être
#### Valeurs
Les pivots de notre fonctionnement sont la liberté, l’autonomie et l’ouverture. Pour nous, la communauté est au coeur du fonctionnement, et les communs sont la base de nos échanges et productions. Nous souhaitons véhiculer et incarner des valeurs démocratiques, de respect des individus dans une dynamique pair à pair, de réciprocité dans les échanges. Le bien-être et la qualité de vie sont également au coeur de nos préoccupations. Nous faisons en sorte de nous montrer bienveillants les uns avec les autres. Gouvernance La gouvernance se voudra toujours ouverte, polycentrique, et œuvrant pour le bien commun, c'est une des fondation du projet. Nous sommes une assemblée libre.

Ce mode de gouvernance est soutenu par l’outil Loomio dans lequel les décisions ont seulement valeur consultative. Chaque action proposée est portée par la personne qui la propose. Celle-ci est libre d’avancer dans son idée y compris si le vote est négatif. Si un vote est bloquant, nous privilégions la discussion pour obtenir un consensus.

Pour un certain nombre de questions que nous souhaitons le moins nombreuses possibles mais qui engageraient la responsabilité de l’association Open Atlas, la décision doit passer par son conseil d'administration.

#### Animation et communication
L’animation interne de la communauté se fait via des outils comme : Google Drive (partage de documents), GitHub (développement) Gitter (discussions), Asana (gestion des différentes tâches du projet), Skype ou Hangout ou Jitsi (scrum quotidien, apéros virtuels).

L’animation externe est portée par le site internet lui-même ainsi que par le site Facebook (page publique et groupe pour les membres impliqués), un compte Twitter et Gitter.

La communication à propos de Communecter est collective. Chaque membre de la communauté est libre de communiquer comme il le souhaite. Les documents de communication tels que les communiqués de presse ou les visuels sont construits collectivement dans le consensus.

Entrée dans le collectif : théoriquement, une demande d’entrée est étudiée lors d’un Conseil d’administration (voir les statuts de l’association Open-Atlas). Un questionnaire basé sur le #CodeSocial est proposé au postulant, puis une première validation est effectuée avec la commission d’entrée. Le membre entrant doit s’engager à respecter le présent #CodeSocial.

Dans le fonctionnement réel actuel, le conseil d’administration, la commission d’entrée, et les différents organes ne sont pas formalisés. A ce jour, un membre du collectif est avant tout une personne engagée à faire avancer par des actions concrètes (administrativement, techniquement, commercialement, financièrement) le collectif bientôt formalisé en tant que SCIC et le produit Communecter. Le degré d’implication d’une personne dans les contributions le rend d’office membre de la commission d’entrée par exemple. Le groupe fonctionne sur la confiance et l’accueil bienveillant des contributions.

### Modèle artistique et culturel
#### Sources d’inspiration culturelle
-   l’open source
-   les communs
-   le communalisme
-   l’anarchisme
-   la liberté d’expression,
-   le droit de choisir
-   l’autonomie
-   la résilience
-   le do it yourself
-   la reproduction massive

#### Valeurs de la plateforme Communecter
-   Open Source
-   Pas de pub
-   Données protégées
-   Open Data
-   Linked Data
-   Territoire connecté
-   Intelligence collective
-   Gratuit
-   Société 2.2.main (après le web 2.0, la société de demain)
-   Biens communs
-   Permaculture : Système en adaptation et développement permanent

#### Place de l’art dans l’organisation
“Art is the heart of our culture” Tout système libre peut être considéré comme de l’art , dans le sens où c’est un produit traduit et composé par des esprits libres dans un objectif pas forcément défini ou connu à l’avance et le chemin pour y arriver passe par beaucoup de recherche, d'expérimentation et d’adaptation.

#### La place de l’art dans la société selon nous
L’art est essentiel dans le développement de la société au même titre que les sciences. L’art est un des process qui innove constamment et traduit la pensée, la rend tangible et transmissible. Une société ouverte à l’art pourrait se développer à son image, évoluer librement, sans règle stricte, en total stigmergie avec des objectifs nourrit par le contexte, toujours mutant, évolutif, critique et adapté. https://www.youtube.com/watch?v=cHJUhuiqmU0

### Modèle économique
#### Rémunération
Les rémunérations se font e méritocratie pour les travailleurs du projet et en reversement au commun. Pour le moment, seul les travaux tangibles ont été rémunérés. Nous sommes en train d’élaborer le modèle adapté et générique de gestion d’une organisation totalement ouverte.

Voici une première tentative de modélisation de ce modèle. ce document (encore embryonnaire ) produit par @simonsarazin est une vision qu'il avait conçu pour Imagination for People (délaissé à l'époque), revue avec l’expérience et les composantes de nos projets œuvrant pour les communs : [https://docs.google.com/document/d/1-hJtXRtnPHaIv3Iqi6tIZvFSbeIzT5N2Su-G6oLl2nY/edit](https://docs.google.com/document/d/1-hJtXRtnPHaIv3Iqi6tIZvFSbeIzT5N2Su-G6oLl2nY/edit)

#### Flux financiers
L’utilisation de la plateforme est gratuite.

A force de frapper aux portes des collectivités (région Réunion et incubateur, fin 2014) et grâce à la confiance d’associations partenaires (Granddir, mi 2015), nous avons décroché un budget de 6500 € pour commencer à travailler sur le développement d’une application. Communecter est né.

En 2016, nous lançons une campagne de crowdfunding sur KissKissBankBank pour développer des fonctionnalités supplémentaires (version mobile, module de discussions) à hauteur de 20.000€ (but visé : 40.000€).

Le projet sera ensuite basé sur le modèle économique suivant :
-   Adhésion des communes
-   Commandes de services en Logiciel libre
-   Dons (via HelloAsso pour le moment, mais nous cherchons une solution plus ouverte)
-   Une utopie financière : le système se nourrit de lui même. il fera naître des besoins qui seront soutenus et financés par les utilisateurs.
-   Si nécessaire, si on a satisfait assez de monde, on a pensé a faire un abonnement en prix libre avec une indication de 12€ (1€ / mois) par utilisateur

#### Partenaires économiques
Le projet s’inscrit dans un réseau d’écosystèmes complexe, avec de multiples partenariats, à différents degrés.
Nos clients : les communes et les bénéficiaires de nos de services en Logiciel libre
Nos fournisseurs / nos charges :
Prise en compte des flux immatériels et non financiers : nous portons une attention particulière à ce que les acteurs du projet se sentent accueillis et reconnus dans leurs compétences avec bienveillance.
Nous travaillons en réseau en privilégiant une ambiance agréable (musique,...) et en veillant à ce que chacun se sente satisfait.
Sans tenir un décompte précis des temps passés par chacun, nous envisageons des rétributions rétroactives dès que le financement du projet le permettra.

### Modèle Technologique
#### Les technologies utilisées par le projet
web, php, html5, mongo, API REST, openstreet map, leaflet ,api, meteor et des dizaines de librairies jquery, sémantique (schema.org, ActivityStream)

Dans un premier temps, le projet respecte les fondements d’un site web classique avec la 0.1.

La 0.2 prévoit l’interopérabilité (avec Encommuns et OpenAgenda) et la mutualisation de l’ODB par la contribution de multiples interfaces (front end) contribuant à une même base de données : l’ODB ou Open Data Base qui sera un premier point de convergence de toutes nos applications. Il y a d’autres surprises en 0.3 mais… chaque chose en son temps...

#### Notre positionnement par rapport à l’OpenSource
Communecter est opensource depuis le début et pour toujours.

#### La place de l’humain vis à vis de la technologie
La technologie vise à faciliter, transmettre et à s’adapter aux besoins des humains, qui doivent eux-mêmes être constamment éduqués pour un bon usage de cette technologie.

### Modèle écologique
#### Les impacts environnementaux de Communecter
De nos jours un système qui prétend améliorer la fonctionnement de la société ne peut passer à coté de l’aspect environnemental vu l'état des choses. On a atteint un summum consumériste et l’impact de l’humain sur la nature et beaucoup trop destructeur et trop peu contributeur.

Nous souhaitons valoriser les actions et les acteurs des biens communs pour leur donner plus de visibilité, améliorer leur respect, et facilité l’innovation pour les communs.

Pour nous, la biodiversité doit également être valorisée dans tous ses aspects. Nous prônons le référencement des espèces , des pratiques et des acteurs pour faciliter le métissage, l’association, la mise en relation. Nous faisons la promotion de toutes les bonnes pratiques écologiques et environnementales.

### Modèle Juridique et Financier
#### Organisation juridique
Raison sociale : Association Open Atlas Fondateurs : Tibor Katelbach, Sylvain Barbot, Jérôme Gontier, Stéphanie Lorente, Tritan Goguet, Clément Damiens, Raphael Rivière Président : Luc Lavielle

Voir les statuts de l’association qui sont publics : [https://docs.google.com/document/d/12VZUkNerrE14sz6POwv7UXL9XSr__UjsMaWm8HH_5sE/edit#heading=h.2k3gmtjfbgrl](https://docs.google.com/document/d/12VZUkNerrE14sz6POwv7UXL9XSr__UjsMaWm8HH_5sE/edit#heading=h.2k3gmtjfbgrl)

Pour le moment O.R.D (Open R&D : collectif de développeurs) assure la production de code.

Afin de créer une structure démocratique et collaborative, Open Atlas est à l’initiative de la création d’une SCIC (Société coopérative d'intérêt collectif). Cette SCIC regroupera les acteurs du projet (Salariés, Partenaire, Client Partenaire, Investisseur, Collectivité... ) et permettra l’ouverture à la communauté pour y construire le projet de bien commun “Communecter”. Cet outil propose de faciliter l’échange et les décisions démocratiques sur un territoire : l’ambition de la SCIC est de l’utiliser pour sa propre gestion et ainsi appliquer les mêmes règles d’intelligence collective.

Les données de Communecter sont déclarées à la CNIL au nom de l’association Open Atlas. Voir le récépissé de déclaration : [https://drive.google.com/drive/u/0/folders/0B0BMYNdshloMSERqejAtQ0NReGs](https://drive.google.com/drive/u/0/folders/0B0BMYNdshloMSERqejAtQ0NReGs)

Le nom de domaine www.communecter.org est déposé au nom de Tibor Katelbach, organisation Oceatoon

#### La marque Communecter
Le problème de la marque c’est que dès qu’elle existe on vous le reproche comme étant antinomique avec la notion de commun. Nous essayons de construire un commun que chacun peut s’approprier tout en contribuant réciproquement au commun initial, un peu à l’image de Wikipedia, mais où l’on demande aux utilisateurs de devenir acteur du système.

Pour le moment, cette marque n’est donc pas déposée. Elle bénéficie juste de l’antériorité du DNS et d’un espace sur GitHub.

Le projet n’a aucun brevet déposé. Nous nous intéressons au concept de trademark de la Wikimedia Foundation [https://wikimediafoundation.org/wiki/Trademark_policy](https://wikimediafoundation.org/wiki/Trademark_policy)

### Organisation financière
Nos comptes sont publiés en temps réel dans un tableur accessible aux membres de l’association Open Atlas. Nous envisageons de les rendre publics après le lancement officiel de la plateforme.

Les flux financiers sont actuellement dirigés vers le compte bancaire de l’association Open-Atlas. Cela devrait changer avec la mise en place de la SCIC.

Open Atlas est devenu un client du collectif O.R.D depuis peu. Dans ce collectif, tous les développeurs sont indépendants, le client paye chaque intervention séparément.

## Code social d’un OpenSystem
### “Écosystème ouvert”
### Document de travail
### Contexte général

Le monde connaît des bouleversements majeurs qui se traduisent sur les plans écologique, économique, énergétique, numérique, social,… et dont l'issue est, par essence, incertaine. Dans ce contexte, de nouvelles structures sociales émergent, basées sur une réorganisation complète des rapports sociaux et des fonctionnements structurels. A l'image d'un phénomène plus vaste que lui, l'écosystème s'inscrit dans ce mouvement, et chacun de ses membres est particulièrement lucide sur le rôle actif que nous pouvons et souhaitons jouer dans la transformation de la société. Notre réseau se veut être un des acteurs qui transforment la société en l'impactant pour qu'elle s’oriente naturellement vers un fonctionnement plus collaboratif et participatif par la mise en pratique concrète de notre intelligence collective. L'écosystème incarne ainsi un élan collectif de mise en commun et de création de communs comme nouveau modèle sociétal.

Pour la plupart des acteurs impliqués, l’idée d’écosystème s’inscrit dans une longue histoire aux racines profondes dont on peut trouver des traces sur le net (comme dans cette vidéo ou dans ce Github). Des affinités intellectuelles et amicales de longue date, des partages joyeux et complices, et des chemins parallèles sont venus nourrir un processus de gestation d’environ deux ans, au cours duquel une convergence a été tissée et a fait émerger la volonté de mettre en synergie les aventures individuelles.

La plupart des membres de l’écosystème se sont retrouvés en ligne échangeant sur des thématiques qu’ils avaient en commun, il en a émergé un espace de rencontre, de reliance et de co-création autour de thématiques et de projets liés à la transition.

Le présent code social entend donc initialement brosser les grandes lignes d’un projet destiné à grandir dans le temps et à s’opérationnaliser sous diverses formes correspondant à la créativité des individus qui composent l’écosystème. Aussi, nous considérons que les règles ne sont pas inscrites au coeur du commun, mais dans le coeur de chacun, pas à pas, au contact des autres et de l'action contributive, de manière à ce qu'elles soient édictées, intériorisées, adoptées, appropriées par chacun au fur et à mesure.

### Faisceau de qualités
#### Les objectifs

L'écosystème est un laboratoire d'expérimentation de nouvelles relations sociales et entrepreneuriales fondées sur la réciprocité et la bienveillance comme fondements d'une autonomie distribuée et organisée. Il vise à (dé)montrer qu'un autre monde est possible non pas au nom d'une utopie dogmatique mais par une mise en actes permettant un constat pragmatique direct d'efficience, de robustesse et de pérennité emprunté notamment au biomimétisme.

Pour cela, les membres de l'écosystème se proposent de relever ensemble les défis suivants : augmenter notre efficience et avoir de l'impact grâce à la mutualisation proposer une vision incarnée des possibilités émancipatrices du numérique démontrer la pertinence sociale et économique d'une forme d'entreprenariat construit autour des communs créer les conditions d’un agir ensemble, sur les projets de chacun, répondre à des appels d'offres en commun, créer de nouveaux projets ensemble. proposer des prestations autour des communs, soutenir des communs, produire des communs essentiels au développement des communs en général mettre en oeuvre concrètement une interopérabilité sociale réelle, agile par les acteurs eux-mêmes et illustrée par le présent code social. oeuvrer à l’émergence d’un monde plus juste, plus soutenable, plus durable tout en utilisant la productivité du numérique.

L’un des objectifs de l’écosystème est de permettre une coopération forte entre les différents membres de l’écosystème. Nous concevons cet écosystème comme un espace de pleine confiance permettant à ses membres de s’ouvrir pleinement à la coopération.

A cette fin, nous expérimentons un certain nombre d’outils dont l’objectif est de permettre à cet écosystème :

```
De travailler ensemble efficacement
D’interagir de manière fluide (Système d’information ouvert)
De collaborer de manière agile et décentralisée (stigmergie, LEAN, AGILE)
De partager nos offres et opportunités (Catalogue contributif)
De répartir équitablement la valeur collectivement générée (Matrices de richesses, Revenu de base)
De mélanger nos offres pour innover en continu
De dépasser la rentabilité pour aller vers le plaisir, le partage et la créativité
De créer un système autonome et résilient à la crise économique (de la ZAD à la ZAN : Zone Autonome Numérique)
De développer l’intelligence collective au service de l’innovation et des bien communs
De ne jamais oublier les contextes déconnectés (solutions déconnectées)
De garder une place importante à la chance, par tirage au sort
Que toutes activités et l’innovation de l’écosystème passent par le partage et des décisions démocratique (par exemple sur Loomio)
D’innover par notre capacité d’ouverture et de partage

```

Rejoindre l’écosystème, c’est souscrire à l’idée qu’on peut créer ensemble un système libre, infiniment duplicable, plus intelligent et efficient, valorisant diverses techniques et technologies du moment pour éliminer les contraintes, réduire l'impact, remplacer les système pyramidaux et descendants par des organismes aux fonctionnements équilibrés, fluides, inversés et libres.

L’ecosystème souhaite être une expérimentation à lui tout seul de ce que pourrait être une société 2.2.main. Les outils produits doivent l’être pour le plus grand nombre. Les règles et méthodologies doivent rester simples, intuitives, fortement éthiques et abordables à tous. On veillera donc à ce qu’elles soient souples, créatives, adaptées au contexte et établies collectivement pour faire en sorte qu’elles soient appliquées volontairement par les membres de l’écosystème, ainsi que le préconise Elinor Ostrom.

Dans le même esprit, les membres de l’écosystème s’engagent à veiller à une interopérabilité technique et sémantique, notamment en utilisant autant que possible du vocabulaire et un langage compréhensible par tous, tant au niveau des développements informatiques que des échanges humains.

### Les missions
#### Missions internes

Produire un cadre, notamment à travers un code social, et créer les conditions de la collaboration entre les membres sur des logiques de mutualisation et de complémentarité ;
-   Mettre en œuvre un fonctionnement interne bienveillant et "permaculturel" : on s’adapte constamment aux particularités locales, aux contextes, aux collaborateurs...
-   Créer concrètement les conditions d'une interopérabilité humaine et technologique ;
-   Œuvrer à la mise en place d'un langage commun ;
-   Se former en pair à pair sur ce que nous faisons les uns les autres et permettre le développement de fertilisations croisées entre les acteurs et projets de l’écosystème ;
-   Proposer un catalogue d’offres en liens direct avec les valeurs de l’écosystème ;
-   Développer massivement des instances de l’ODB (Open Data Base), une base de données partagée et ouverte à la contribution. A l’image de Wikipedia, cette base de données est ouverte à la contribution et sécurisée, chacun choisit les limites d’ouverture de la donnée qu’il apporte ;
-   Modéliser le fonctionnement d’écosystèmes et d’organisations ouvertes ;
-   Créer un réseau de tiers lieux camps TIC, où n’importe qui peut découvrir un membre de l’écosystème, où l’écosystème peut * se réunir, où on peut rediffuser et partager une activité d’un autre tiers lieux.

En créant ces conditions en interne, nous posons une base éthique et efficiente pour impulser un changement de société. Nous donnerons ainsi une assise aux missions externes par auto-légitimation ("c'est parce que ça a marché pour nous que nous transmettons cette expérience").

#### Missions externes
La mission générique de l’écosystème est de mettre en oeuvre la recherche, le développement, l’expérimentation, la documentation, et la diffusion autour de l'ingénierie des écosystèmes trans-organisationnels. Cette mission peut se décliner comme suit :

Accompagner les acteurs publics, privés et les citoyens dans leurs transitions (énergétique, écologique, démocratique, etc...) vers les communs ;

```
Offrir des opportunités aux entreprises, associations, collectivités, élus, citoyens, de s'impliquer en faveur des communs (outils, dynamique,...) et leur proposer des dispositifs de capacitation ;
Développer, distribuer, former sur les communs et les solutions libre et opensources
Partager les éléments de réussite et d'échec de l'expérience.

```

### Faisceau de valeurs
#### Imaginaire, sources d'inspiration
Parce qu’il fait interagir une pluralité d’acteurs ayant chacun une histoire propre, l’écosystème se nourrit d’une diversité d’imaginaires, de sources d’inspiration, ce qui en fait sa richesse, sa spécificité. Parmi ses sources d’inspiration, nous pouvons notamment citer :

-   les notions d’autonomie, de liberté, de fraternité et de solidarité. Les pensées et dynamiques qui se sont développées autour de la démocratie directe, de la démocratie liquide, de la souveraineté ‒ qu’elle soit individuelle ou collective ‒ de la citoyenneté, de l’opengov.
-   Le monde de l’open-source ainsi que celui des communs dont on peut considérer qu’ils ont et sont en train de très concrètement changer le monde en permettant le développement de projets aussi beaux et fous que le web ou wikipedia.
-   Le peer to peer tel que défini par Michel Bauwens, la théorie des systèmes ‒ complexes ‒ qui ont permis un nouveau regard sur le monde, à travers les notions de combinatoire, d’auto-organisation, d’émergence,....
-   Le web sémantique, le W3C, le web qui ont consacré les notions de standards, d’interopérabilité, de neutralité.
-   Les nouvelles formes d’économie, collaboratives, circulaires, numériques, agiles, fablabs, DIY, DIWO, Hackerspaces, et celles émergentes qui, comme les DAO (Distributed Autonomous Organisations), permettent d’entrevoir des changements profonds à grande échelle quand à l’architecture du monde social.
-   Le vivant, qui pour beaucoup d’entre nous, constitue un objet d’intérêt, tant il est complexe, tant les milliards d’années qui ont conduit à sa physionomie et à sa dynamique actuelles sont riches d’enseignement dans tous les domaines (économie, politique, technique notamment - #biomimétisme) ; les penseurs de la complexité et des systèmes émergents, et notamment le romancier Arthur Koestler qui a proposé le concept de holon.
-   L’écologie et la permaculture qui à travers la profondeur de leurs questions et la pertinence de leurs réponses, nous donnent une raison de plus d’agir et d’espérer changer le monde.
-   Le jeu, l’élan vital, l’hédonisme, l’épicurisme, la joie, et toutes les choses de la vie qui procurent aux êtres sensibles que nous sommes, la sensation d’être vivant, heureux, insouciants, en forme, en harmonie avec soi, les autres et le monde.
-   La communication bienveillante, et de manière générale les bienfaits d’une meilleure connaissance de soi pour se donner la capacité de développer des relations sociales harmonieuses.

#### Vivre ensemble c’est faire ensemble
Les relations humaines, que ce soit dans le cadre de l’écosystème ou vis-à-vis de l’extérieur, s’inscrivent dans une perspective de bienveillance, d’autonomie, de transparence, de réciprocité et d’ouverture à la reliance. L’écosystème souhaite offrir l’expérience d’un réel inter-être, où l’individualité de chacun est pleinement respectée.

Un écosystème commence par travailler ensemble pour créer des alternatives, des hackathons, des visions en communs. Le produits de ces créations seront la base commune de l’écosysteme. Sans co-construction il n’y a pas d’écosystème, juste de la consommation de l’écosysteme. Ces actions communes donneront encore plus de richesse au collectif que celles déjà apportée indépendemment par chacun des acteurs.

### Faisceau de droits
#### Type de structure
Non Lucratif, associatif et coopératif. Dès que deux acteurs de l’écosystème coopèrent, l’ensemble des pairs de l’écosystème doivent avoir connaissance de l’existence de ce projet pour nourrir l’envie, la motivation et l’engagement des autres membres. Une coopération est un exemple à suivre pour motiver d’autres coopérations.

#### Modalités d’entrée
L’écosystème est une structure ouverte dans laquelle entre toute personne ou projet qui se reconnaît dans le présent code social. On entre dans un écosystème par un de ses noeud existants, c’est à dire que le résultat de l’interaction avec ce ou ces premiers noeuds permet d’interagir avec un ou plusieurs acteurs du système. Cet acte d’échange validé par un élément du système donne accés directement à la totalité de l’écosystème.

#### Mode de gouvernance
Toutes idées, propositions, doivent se faire sur un systeme de décision collaboratif et communautaire ouvert tel que Loomio, avec pour objectif l’amélioration continue. Chaque acteur peut contribuer avec des idées et challenger l’écosysteme. Il faut nourrir l’esprit créatif et concurrentiel à l’interieur de l’écosysteme pour sortir des sentiers battus et pour trouver un chemin ensemble. Les bons chemins d’hier ne le sont plus aujourd’hui et ceux d’aujourd’hui ne le seront plus pour longtemps. L’écosystème est mutant en fonction de son contexte, du temps et des pays.

### Faisceau de richesses
#### Matrices de richesse
Les membres de l’écosystème ont conscience que les richesses échangées ne se limitent pas à l’argent. Les autres formes de richesses en circulation (temps, travail, gratitude, échanges humains,...) sont reconnues et valorisées.

#### Types de licences
L’écosystème est dédié à la création de biens communs, open source, logiciel libre, open hard ware, open gouvernance… partagé par tous et transmis massivement. L’open source ne s’applique pas qu’aux solutions logicielles mais à toute création de l’esprit (matérielle et intellectuelle).

Le fonctionnement ouvert de la communauté est une condition de réussite, pour inspirer sur son chemin d’autres initiatives oeuvrant pour les communs. C’est ce que font déjà un grand nombre de projets du Libre, ils se nourrissent les uns des autres et avancent avec trés peu de contrainte.

On suivra la sagesse de Gandhi qui dit que tout ce qui n’est pas partagé sera perdu. Pour être efficients, élaborons en communauté de nouvelles licences à réciprocité, rétribuant tous les acteurs d’un processus, pour sortir d’un système fatigué, totalement individualiste et orienté vers la rentabilité.

L’objectif de l’écosystème est de créé un organisme vivant , plus riche que la somme de ses parties et vers lequel chaque partie peut se tourner pour s’inspirer, se ressourcer, se rassurer, se confronter, se reposer....
