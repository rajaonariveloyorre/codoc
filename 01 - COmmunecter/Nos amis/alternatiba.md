## Faisons Alternatiba toute l'année !

Nous partageons le constat, la philosophie et les moyens mis en œuvre par Alternatiba. Plusieurs pixels humains sont membres d'Alternatiba et COmmunecter est partie prenante de plusieurs villages en France.

## Utiliser COmmunecter pour son groupe local
<p id="bkmrk-"><iframe src="https://pixelhumain.github.io/reveal.js/slideshows/Alternatiba.html#/" width="830" height="600"></iframe></p>

Nous sommes un outil au service de toutes les personnes souhaitant valoriser les alternatives et créer des réseaux locaux résilients.

##### Avant un village

-   Communiquer localement
-   S'organiser

##### Pendant

-   Structurer sa communauté
-   Valoriser les alternatives non présente et montrer le maillage territorial des alternatives (présenter la cartographie)

##### Après

-   Continuer les échanges
