# Modèle économique de Communecter

Vous pouvez nous aider à [Rechercher des financements](/3 - Contribuer/financement.md).

![](/Images/modeleeconomique.png)

Le projet Communecter possède un plan de financement social afin d’assurer la viabilité et la pérennité du projet. Nous prévoyons également de créer une SCIC ([en savoir +)](https://docs.google.com/document/d/1ath12F-XPupfS1pd_A69pI-j5ZoBoU7B8q9JSz_Gp5o/edit?usp=sharing) pour assurer davantage l’assise économique du projet. Communecter est un outil d’intérêt général qui se construit en co-financement et partage des coûts avec différentes sources de revenu :

-   Bénévolat : le bénévolat demeure une partie importante de l’apport au projet dans sa phase actuelle
-   Financement public : le projet étant d’intérêt général et porté par une association, nous faisons appel à des subventions et financements régionaux, nationaux et européens
-   Appels à projets ([liste](/3 - Contribuer/financement.md))
-   Prestation de services auprès de nos partenaires, notamment l'adaptation et la personnalisation pour des collectivités et des collectifs citoyens (en savoir + sur l'offre [COstum](/6 - COstum/costum.md))
-   Financements participatifs ([2016](https://www.kisskissbankbank.com/communecter-se-connecter-a-sa-commune), [2019](https://www.helloasso.com/associations/open-atlas/collectes/communecter))
-   Cotisations des adhérents à l’association Open Atlas ([HelloAsso](https://www.helloasso.com/associations/open-atlas/))
-   Des dons publics et privés ([HelloAsso](https://www.helloasso.com/associations/open-atlas/), [Lilo](http://www.lilo.org/fr/?utm_source=communecter))


## Bilans financiers
-   2018 ([bilan](https://drive.google.com/open?id=1m0JQGtdIb1tgBOq_LnLtfR2PsXFKekv8))
-   2017 ([bilan](https://drive.google.com/open?id=1flO_dGAZXq1IllDdpA7MYGPLVEASVWfE))
-   2016 ([bilan](https://drive.google.com/open?id=1AD7gwalxAlru0HmEhDssZ0JNd6yAhI7M)
-   2015 ([bilan](https://cloud.communecter.org/index.php/s/0EMYZNR8OuVpT1R), [compte de résultats](https://cloud.communecter.org/index.php/s/GeDd7vxCh1EdYpD))
-   2014 ([bilan](https://drive.google.com/open?id=0B0BMYNdshloMbGUwcUpEeHhCdVE))
-   2013 ([bilan](https://drive.google.com/open?id=0B0BMYNdshloMcTVJLTdTcWstczg))
-   2012 ([bilan](https://drive.google.com/open?id=0B0BMYNdshloMdWpSRnkwb1NZQTg))
