# Observatoire local
## Objectifs
- Évaluer et valoriser les dynamiques sectorielles
- Définir des critères d’évaluation représentatifs et en visualiser les résulats
- Faciliter la lecture d’une analyse sectorielle
- Mesurer l’impact d’un dispositif incitatif (animation de réseau, financement, réglementation, …)


## Principe
Nous pouvons valoriser l’ensemble de l’activité d’une thématique , d’une compétences de collectivité territoriales, d’une filière économique, à différentes échelles géographiques, et en extraire des grilles d’analyses pertinentes pour en mesurer les externalités positives ou négatives. A partir de la connaissance et de la caractérisation des acteurs, de leurs activités, de leurs interactions et de leurs impacts sur leur territoire, nous pouvons réaliser un véritable observatoire qui tient compte des évolutions et dynamiques relatives à des thématiques variées (transition écologique, mouvements citoyens et démocratiques, politiques publiques, …) en temps réel. Nous nous appuyons sur notre méthodologie pour élaborer et modéliser avec les partenaires l’ensemble des projets qui font partie du secteur ciblé, les interactions entre acteurs, leurs actions, les parties prenantes en présence pour mettre en exergue les effets sur leur territoire.


## Exemple
### [Observatoire national des CTE](https://cte.ecologique-solidaire.gouv.fr/#dashboard)
Ce site propose une grille d’analyse quantitative (nombre de CTE signés, d’actions mises place, montant des financements publics et privées, quantité de ressources naturelles préservées, …) et qualitative (la chronologie du programme, les domaines d’actions, les objectifs visés et atteintes, les acteurs en présence et leurs rôles, …) qui permet de mesurer l’impact réel du programme du ministère de la Transition écologique et solidaire.
