# Créer un réseau social

## Objectifs
Le but est d'avoir un site web où les inscrits pourront publier des messages ou des évènements mais aussi référencer des organisations. À cela s'ajoute des fonctionnalités permettant de suivre, commenter et améliorer les informations.


## Principes
Lors de la création du COstum il suffit de choisir les particularités de votre réseau social :
- thème (biodiversité, social, déchets, emploi, …)
- territoire (ville·s, département, région ou pays)
- fonctionnalités (agenda, moteur de recherche, messagerie, …) -> [liste complète](/2 - Utiliser l'outil/Fonctionnalités/liste.md)
- charte graphique (logo, couleurs, …)
- lien

Le but n’est pas de créer un “silo” mais, au contraire, d’avoir un site web interconnecté et contributif :
- Vous pouvez afficher tout ou partie des évènements, projets et organisations présents sur [communecter.org](http://communecter.org) sur votre plateforme.
- Le référencement effectué sur votre site améliore automatiquement le contenu disponible sur [communecter.org](http://communecter.org)


## Exemples
### [FilNum](https://www.communecter.org/costum/co/index/id/coeurNumerique)
...
