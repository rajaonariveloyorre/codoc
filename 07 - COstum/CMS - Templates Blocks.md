# Documentation sur les Templates existants

## open Costum géré comme un CMS 

## créer des pages statiques

## Qu'est ce qu'un template ?

Afin d'accélérer la production de vos COstums, nous avons mis en place des templates avec des fonctionnalités pré-définir personnaliser ? Vous aimez le design de la carto sur Alternatiba et vous souhaitez le ré-utiliser sur votre COstum, utilisez nos templates.

Vous pouvez aussi créer votre propre template afin qui soit réutilisable partout par les autres développeurs mais aussi sur les COstums générique

## Comment utiliser un template

Au début de la page sur laquelle vous souhaitez ajouter des éléments templates mettre ce code.

```
<?php $params = [
    "tpl" => "aCompleter",
    "slug"=>$this>costum["slug"],
    "canEdit"=>$canEdit,
    "el"=>$el ]; 
echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true ); ?>
```



| nom  | type | initialiser | description |
| ---- | ---- | ----------- | ----------- |
| tpl     |  string    |    non         |   Le nom d'élèment de COstum, elle devrait être identique à celle de votre fichier JSON          |
|   slug   |  string    |    oui         |    Ne pas toucher         |
|  canEdit    |  boolean    |    oui         |   Permet administrateur de pouvoir l'administrer en mode COstum générique, si vous souhaitez pas mettre en false          |
| el |  array    | oui        | el (array) : Permet de récupérer les informations propre à l'élément        |

Vous êtes prêt a utiliser un template a l'endroit que vous désirez


<h1 style="color:blue;text-align:center" >Templates sur les évènements</h1>
Retrouvez l'ensemble des templates sur les évènements

## Bloc des 3 dernières évènements
### blockEventDescription.


    

![](https://i.imgur.com/67QyQKr.png)
*Basé sur le COstum Alternatiba.*

```
<?php $params = array("canEdit"   =>    $canEdit);
echo $this->renderPartial("costum.views.tpls.events.blockEventDescription", $params); ?>
```

|nom | type | description |
| -------- | -------- | -------- |
| canEdit     | boolean     | Permet d'activer la personnalisation du bloc en mode COstum générique, si vous souhaitez pas que l'administrateur puis l'éditer, initialiser la en false    |

### blockEventCommunity.


![](https://i.imgur.com/67QyQKr.png)
*Affiche les 3 derniers évènements da la communauté d'un costum.*

```
<?php $params = array("canEdit"   =>    $canEdit);
echo $this->renderPartial("costum.views.tpls.events.blockEventCommunity", $params); ?>
```

|nom | type | description |
| -------- | -------- | -------- |
| canEdit     | boolean     | Permet d'activer la personnalisation du bloc en mode COstum générique, si vous souhaitez pas que l'administrateur puis l'éditer, initialiser la en false    |

 
### blockEvent
 (Refaire le screen)
 *Basé sur le COstum Coeur Numérique.*
 
```
<?php $params = array("canEdit"   =>    $canEdit);
echo $this->renderPartial("costum.views.tpls.blockevent", $params); ?>
```
|nom | type | description |
| -------- | -------- | -------- |
| canEdit     | boolean     | Permet d'activer la personnalisation du bloc en mode COstum générique, si vous souhaitez pas que l'administrateur puis l'éditer, initialiser la en false    |


## Bloc caroussel
### eventCarousel
![](https://i.imgur.com/JsZklv7.png)
*Affiche le nombre d'événements souhaité.*
```
<?php $params = array("canEdit"   =>    $canEdit);
echo $this->renderPartial("costum.views.tpls.eventCarousel", $params); ?>
```
|nom | type | description |
| -------- | -------- | -------- |
| canEdit     | boolean     | Permet d'activer la personnalisation du bloc en mode COstum générique, si vous souhaitez pas que l'administrateur puis l'éditer, initialiser la en false    |

### blockEventSlide.
![](https://i.imgur.com/brWJtWj.png)
*Affiche les 6 derniers évènements du costum dans un carousel.*

```
<div style="margin-top: 6vw" class="no-padding carousel-border" >
    <div id="docCarousel" class="carousel slide" data-ride="carousel">
        <?php $params = array("canEdit"   =>    $canEdit); ?>
         echo $this->renderPartial("costum.views.tpls.events.blockEventSlide", $params); ?>`
    </div>
</div>
```

|nom | type | description |
| -------- | -------- | -------- |
| canEdit     | boolean     | Permet d'activer la personnalisation du bloc en mode COstum générique, si vous souhaitez pas que l'administrateur puis l'éditer, initialiser la en false    |

<h1 style="color:blue;text-align:center" > Templates sur la  communautée </h1>

## Bloc Carousel
### communityCarousel
![](https://i.imgur.com/HQzpEgC.jpg)
*Affiche la communauté d'un costum dans un carousel.*

```
<?php $params = array("canEdit"   =>    $canEdit);`
echo $this->renderPartial("costum.views.tpls.communityCarousel", $params); ?>`
```
|nom | type | description |
| canEdit     | boolean     | Permet d'activer la personnalisation du bloc en mode COstum générique, si vous souhaitez pas que l'administrateur puis l'éditer, initialiser la en false    |
| roles | string | Permet de récupérer par les organisations/personnes par rapport à leurs rôles, le rôle doit être unique |


### blockCommunity
![](https://i.imgur.com/DVen7zZ.png)
*Basé sur hubMednum, affiche les 3 derniers*

```
<?php $params = array("canEdit"   =>    $canEdit); ?>
echo $this->renderPartial("costum.views.tpls.blockCommunity", $params); ?>
```
|nom | type | description |
| canEdit     | boolean     | Permet d'activer la personnalisation du bloc en mode COstum générique, si vous souhaitez pas que l'administrateur puis l'éditer, initialiser la en false    |


<h1 style="color:blue;text-align:center" > Templates sur les articles </h1>


## Bloc app articles
### blockArticles
![](https://i.imgur.com/LdbiD2C.png)

affiche les 3 derniers POI avec la category article.

```
<?php $params = array("canEdit"   =>    $canEdit); ?>
echo $this->renderPartial("costum.views.tpls.article.blockArticle", $params); ?>
```

### blockArticlesCommunity
![](https://i.imgur.com/LdbiD2C.png)

affiche les 3 derniers POI de la communauté d'un costum avec la category article.

```
<?php $params = array("canEdit"   =>    $canEdit); ?>
echo $this->renderPartial("costum.views.tpls.article.blockArticleCommunity", $params); ?>
```


<h1 style="color:blue;text-align:center" > Templates sur la map </h1>
## Bloc app map
### basic

(screen).

affiche la map basic.

```
<?php $params = array("canEdit"   =>    $canEdit); ?>
echo $this->renderPartial("costum.views.tpls.map.basic", $params); ?>
```

### mapCommunity

(screen).

affiche la map avec la communauté d'un costum.

```
<?php $params = array("canEdit"   =>    $canEdit); ?>
echo $this->renderPartial("costum.views.tpls.map.mapCommunity", $params); ?>
```
## Bloc App basé sur COmmunecter
### Ressource
(screenshot ressource)

```

```
|nom | type | description |
| canEdit     | boolean     | Permet d'activer la personnalisation du bloc en mode COstum générique, si vous souhaitez pas que l'administrateur puis l'éditer, initialiser la en false    |
| limit     | int     | Indique la limite des résultats a afficher   |

### DDA
(screenshot dda)

```

```

|nom | type | description |
| canEdit     | boolean     | Permet d'activer la personnalisation du bloc en mode COstum générique, si vous souhaitez pas que l'administrateur puis l'éditer, initialiser la en false    |
| limit     | int     | Indique la limite des résultats a afficher   |

### News
(screenshot news)

```

```

|nom | type | description |
| canEdit     | boolean     | Permet d'activer la personnalisation du bloc en mode COstum générique, si vous souhaitez pas que l'administrateur puis l'éditer, initialiser la en false    |
| nbPost     | int     | Indique de posts a afficher   |

### Projets
(screenshot projets)

|nom | type | description |
| canEdit     | boolean     | Permet d'activer la personnalisation du bloc en mode COstum générique, si vous souhaitez pas que l'administrateur puis l'éditer, initialiser la en false    |
| limit     | int     | Indique la limite des résultats a afficher   |

### Map
(screenshot map)

|nom | type | description |
| canEdit     | boolean     | Permet d'activer la personnalisation du bloc en mode COstum générique, si vous souhaitez pas que l'administrateur puis l'éditer, initialiser la en false    |
| type     | array     | Permet de savoir quel type d'élèment vous souhaitez récupérer   |
| latitude     | float    | Récupère latitude pour affichage de la carte   |
| longitude     | float     | Récupère la longitude pour affichage de la carte   |
| country     | string     | Permet à la géolocalisation d'un pays    |