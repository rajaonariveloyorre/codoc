# Présentation des COstum

## Votre COmmunecter personnalisé
![](/Images/costum-title.png)

<iframe src="https://drive.google.com/file/d/1lg9D2XL1wUmnq8K7D-2zOM50HUf83_Dj/preview" width="810" height="1000"></iframe>

COstum est conçu pour vous permettre de créer un COmmunecter spécifique à vos besoins.
Par défaut nous hébergeons votre COstum, mais il est bien sûr possible d'[installer communecter sur votre propre serveur](/4 - Documentation technique/Installer Communecter).

Vous bénéficiez de toutes les nouveautés développées pour Communecter.org, tout en ayant la main sur de nombreux paramètres permettant de personnaliser votre plateforme :
- **thème** du réseau (biodiversité, social, déchets, emploi, ...)
- **territoire** (ville·s, département, région ou pays)
- **fonctionnalités** proposées (agenda, moteur de recherche, messagerie, ...) -> [liste complète](/2 - Utiliser l'outil/Fonctionnalités/liste.md)
- **charte graphique** (logo, couleurs, ...)
- **lien** personnalisé

Les possibilités sont nombreuses. Voici des exemples de réseaux qu’il est possible de créer :
- Réseau social local et thématique : **[Emploi à la Ville du Port](https://www.communecter.org/custom?el=city.97407)**, **[Numérique à La Réunion](https://www.communecter.org/costum/co/index/id/coeurNumerique), [Haute Vallée de l'Aude](https://www.portailhva.org/)**
- Plateforme de consultation citoyenne : **[Pacte pour la Transition](https://www.communecter.org/custom?el=p.pactePourLaTransition)**
- Moteur de recherche et cartographie de filière : **[Tiers-Lieux du Nord](https://www.communecter.org/costum/co/index/id/laCompagnieDesTierslieux), [Meuse Campagnes](https://www.communecter.org/costum/co/index/id/meuseCampagne)**
- Plateforme d'appel à projets et de valorisation des acteurs pour un dispositif déployable dans plusieurs territoires : **[Contrat de Transition Écologique](https://cte.ecologique-solidaire.gouv.fr/)**
- Liste pour les municipales : **[Mayenne Demain](https://www.mayenne-demain.fr/)**, [**Tepoz en Comùn**](https://www.communecter.org/costum/co/index/id/tepozencomun)
- Jeu de contribution **: [Chtitube](https://www.chtitube.fr/)**
- Tiers-lieu : **[Raffinerie](https://www.communecter.org/costum/co/index/id/laRaffinerie3)**
- Journal : **[Insoumis de Chambéry](https://www.journal-insoumis-chambery.com)**

Vous trouverez d'autres exemples de COstum sur notre plateforme **COstum des COstums**. 

![](/Images/costum-leport.png)


## Solution 1 - Générer un COstum à partir d'un exemple
Un COstum générique est un COstum qui est déjà pré-configuré et de le personnaliser à votre image.
Importer vos couleurs et vos images, utiliser des modules d’applications qui vous avez besoin et créer de nouvelles pages statiques.
Il existe 3 COstums génériques :
- Filière : qui se base sur le COstum filière numérique
- Candidat : qui se base sur le COstum Mayenne Demain
- TCO : qui se base sur le COstum smarterritoire

Découvrez comment générer un COstum dans **[la documentation dédiée](https://doc.co.tools/books/5---d%C3%A9ployer/page/g%C3%A9n%C3%A9rer-un-costum)**.


## Solution 2 - Demander la réalisation de votre COstum
Vous pouvez envoyer votre projet à l’équipe d’[Open Atlas](/1 - Le Projet/openatlas.md). Un devis vous sera ensuite envoyé.


## Solution 3 - Créer son COstum de A à Z
Vous avez des compétences en développement web et souhaitez créer votre propre COstum de A à Z, nous expliquons les étapes pour mettre en place votre COstum : **[lire la documentation technique](/4 - Documentation technique/creercostum.md)**.



# Export your costum 
allows you to save to disk any costum configs to install and reuse the costum on another machine. Can be practical to work on the same costum as someone else without having to rebuild all the configs. 
[more details](/home/oceatoon/d7/modules/codoc/04 - Documentation technique/process/export.md) about the export process.

## Run export script 
https://www.communecter.org/costum/co/export/slug/laRaffinerie3

### save to modules/costumexport
https://www.communecter.org/costum/co/export/slug/laRaffinerie3/mode/json/save/1

### request a commit and push the result to share on gitlab/costumexport

### update costumexport 
if you dont have the module installed 
git clone https://gitlab.adullact.net/pixelhumain/costumexport
otherwise 
cd yourWorkingFolder/modules/costumexport
git pull 

### install on your machine 
http://communecterLocal/costum/co/export/slug/laRaffinerie3
click `check install`
or goto http://communecterLocal/costum/co/export/slug/laRaffinerie3/install/1
Click to install or update each data type 