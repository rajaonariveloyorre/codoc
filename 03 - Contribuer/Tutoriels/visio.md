# Organiser une visioconférence
> La [Liste des visioconférences](/1 - Le Projet/videos.md) recense les vidéos de nos anciennes visio.


## Avant
- Framadate : attention aux décalages horaires
- Créer doc collaboratif (doc type ci-dessous) pour que chacun puisse mettre les points à aborder
- Durée : 1h c'est bien

### Exemples de ressources à mettre
- Documents utiles à lire avant la visio
- Liens vers les sites internets concernés


## Pendant
- Maître du temps : indique lorsqu'on dépasse le temps fixe et met le timecode dans l'ordre du jour lorqu'on change de sujet
- On valide l'ordre du jour collectivement
- On fixe un temps à chaque point
- On allume sa caméra uniquement lorsqu'on parle ou que l'on souhaite parler
- Chat du Hangout : tout ce qui est dedans sera perdu
- Rocket.chat : ce que l'on souhaite pouvoir retrouver (ex : partage de ressource)


## Après
- Timecode dans la description de la vidéo
- Transcrire les tâches dans l'espace co


# Intitulé de la visioconférence
Date : [lien vers le framadate] Lien vers la visio : [URL du hangout]


## Liens utiles
- Document de la dernière session : [lien 1]
- [Lien 2]
- [Lien 3]


## Rôles à pourvoir
- Lancer la visio :
- Maître du temps :
- Rédacteur·ice :


## Ordre du jour
- [ ] Introduction - [Durée] - [Qui présente ?]
- [ ] Point n°1 - [Durée]
- [ ] Point n°2 - [Durée]
- [ ] Point n°3 - [Durée]
- [ ] Résumé des actions à faire - [Durée] - [Qui présente ?]

## Résumé

## Tâches
