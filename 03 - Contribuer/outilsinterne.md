# Outils internes
Les coTools sont les outils utilisé par la communauté pour interagir entre nous, s'organiser et développer le projet. Rejoignez la communauté pour en discuter : [#cotools](https://communecter.org/#@cotools).

Certains d’entre eux ont vocation a être intégré dans COmmunecter comme nous l'avons fait pour Rocket.Chat.

Ces outils sont mis en ligne et maintenu par des bénévoles. Si vous avez des compétences en administration système nous avons constamment besoin de coup de main : installation, mise à jour, ...


## Outils
<table id="bkmrk-logiciel-description" style="border-collapse: collapse; width: 100%; height: 376px;" border="1"><tbody>
<tr style="height: 43px; background-color: grey;">
<th style="width: 15.1544%; vertical-align: middle; height: 43px;">
<p><span style="color: #ffffff;"><em><strong>Logiciel</strong></em></span></p>
</th>
<th style="width: 18.4568%; vertical-align: middle; height: 43px;"><span style="color: #ffffff;"><em><strong>Description</strong></em></span></th>
<th style="width: 32.0371%; vertical-align: middle; height: 43px;"><span style="color: #ffffff;"><em><strong>Lien<br></strong></em></span></th>
<th style="width: 29.1975%; vertical-align: middle; height: 43px;"><span style="color: #ffffff;"><em><strong>Comment se connecter ?</strong></em></span></th>
</tr>
<tr style="height: 20px;">
<td class="align-center" style="width: 15.1544%; height: 70px; vertical-align: middle;">
<p><strong>Rocket.Chat</strong></p>
</td>
<td style="width: 18.4568%; height: 70px; vertical-align: middle;"><a href="https://doc.co.tools/books/2---utiliser-loutil/page/chat-de-discussions">Messagerie instantanée</a></td>
<td class="align-left" style="width: 32.0371%; height: 70px; vertical-align: middle;"><a href="https://chat.communecter.org" target="_blank" rel="noopener"><strong>chat</strong>.communecter.org</a></td>
<td style="width: 29.1975%; height: 70px; vertical-align: middle;">Mêmes identifiants que communecter.org</td>
</tr>
<tr style="height: 20px;">
<td class="align-center" style="width: 15.1544%; height: 43px; vertical-align: middle;">
<p><strong>BookStack</strong></p>
</td>
<td style="width: 18.4568%; height: 43px; vertical-align: middle;"><a title="Documenter" href="https://doc.co.tools/books/3---contribuer/page/documenter">Documentation</a></td>
<td class="align-left" style="width: 32.0371%; height: 43px; vertical-align: middle;"><a href="https://doc.communecter.org" target="_blank" rel="noopener"><strong>doc</strong>.communecter.org</a></td>
<td style="width: 29.1975%; height: 43px; vertical-align: middle;">Inscription libre</td>
</tr>
<tr style="height: 20px;">
<td class="align-center" style="width: 15.1544%; height: 62px; vertical-align: middle;">
<p><strong>CodiMD</strong></p>
</td>
<td style="width: 18.4568%; height: 62px; vertical-align: middle;">Éditeur de texte collaboratif</td>
<td class="align-left" style="width: 32.0371%; height: 62px; vertical-align: middle;"><a href="https://codimd.communecter.org/" target="_blank" rel="noopener"><strong>codimd</strong>.communecter.org</a></td>
<td style="width: 29.1975%; height: 62px; vertical-align: middle;">Mêmes identifiants que communecter.org</td>
</tr>
<tr style="height: 20px;">
<td class="align-center" style="width: 15.1544%; height: 62px; vertical-align: middle;">
<p><strong>Wekan</strong></p>
</td>
<td style="width: 18.4568%; height: 62px; vertical-align: middle;">Gestion de projet kanban</td>
<td class="align-left" style="width: 32.0371%; height: 62px; vertical-align: middle;"><a href="https://wekan.communecter.org" target="_blank" rel="noopener"><strong>wekan</strong>.communecter.org</a></td>
<td style="width: 29.1975%; height: 62px; vertical-align: middle;">Mêmes identifiants que communecter.org</td>
</tr>
<tr style="height: 20px;">
<td class="align-center" style="width: 15.1544%; height: 43px; vertical-align: middle;">
<p><strong>Nextcloud</strong></p>
</td>
<td style="width: 18.4568%; height: 43px; vertical-align: middle;">Cloud collaboratif</td>
<td class="align-left" style="width: 32.0371%; height: 43px; vertical-align: middle;"><a href="https://nextcloud.communecter.org" target="_blank" rel="noopener"><strong>nextcloud</strong>.communecter.org</a></td>
<td style="width: 29.1975%; height: 43px; vertical-align: middle;">Mêmes identifiants que communecter.org</td>
</tr>
<tr style="height: 20px;">
<td class="align-center" style="width: 15.1544%; height: 53px; vertical-align: middle;">
<p><strong>Weblate</strong></p>
</td>
<td style="width: 18.4568%; height: 53px; vertical-align: middle;"><a title="Traduire la plateforme" href="https://doc.co.tools/books/3---contribuer/page/traduire-la-plateforme">Traduction</a></td>
<td class="align-left" style="width: 32.0371%; height: 53px; vertical-align: middle;">
<p><a href="https://weblate.communecter.org/"><strong>weblate</strong>.communecter.org</a></p>
</td>
<td style="width: 29.1975%; height: 53px; vertical-align: middle;">Inscription libre</td>
</tr>
</tbody></table>

### Non utilisé
-   **Dialoguea** - _[dialoguea.co.tools](https://dialoguea.co.tools)_ - Plateforme de débat


## Nextcloud
[Le manuel utilisateur](https://fr.wikibooks.org/wiki/Manuel_utilisateur_Nextcloud#Qu.27est-ce_que_Nextcloud_.3F) propose une présentation plus complète. Nous avons choisi cet outil car il est libre, open source et s'appuie sur des standards ouverts.

> Nextcloud est exclusivement destiné à la collaboration pour le commun. Pour un usage personnel nous vous invitons à héberger vous-même une instance de cet outil.


### Usages
Par défaut vous avez accès à un certains nombre d'informations partagées (dossier "Communecter", groupes de contacts "Écosystème, etc.). Par contre, _si vous créer des fichiers ou des contacts en dehors de ces zones personne d'autres y aura accès_.

Si vous souhaitez, par exemple, rendre accessible un fichier que vous avez créer en dehors d'un dossier partagé, cliquez ici :
![](/Images/nextcloud-partage.png)

Vous pourrez alors choisir de créer un lien de partage, ou de le rendre accessible au groupe.


## Wekan
### Inviter
![](/Images/wekan-inviter.png)

