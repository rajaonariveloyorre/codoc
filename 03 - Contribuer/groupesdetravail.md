## ACCOMPAGNEMENT / DOCUMENTATION
- **[Les rédacteurs fous](https://www.communecter.org/#@codocwiki)** : ici nous documentons le plus possible les travaux passés, futurs, et en cours, afin d'en permettre l'appropriation facile par tout le monde. On partage librement les compétences et connaissances liées à ce qu'il se passe au sein de l'asso et sur la plateforme !
- [Communecter pour les nuls](https://www.communecter.org/#@commentSeCommunecter) : accompagnement à l'usage. Tu viens de découvrir Communecter et tu n'y comprends pas grand chose ? Pas de panique, rejoins nous dans ce groupe de travail ! Tu connais déjà la plateforme et veux filer un coup de main aux nouvelles têtes ? Rejoins nous aussi !


## COMMUNICATION EXTERNE
- **[Communication de Communecter :](https://www.communecter.org/#@co-communication)** rassemble les personnes désirant participer aux projets et travaux de communication autour de l'outil COmmunecter (réseaux sociaux, site internet, événementiel, charte graphique et rédactionnelle, ...)
- [Ergonomie de Communecter](https://www.communecter.org/#@codesign) : le design de CO est un croisement d'idées, d'esthétique , de fonctions et d'innovations, en perpétuelle évolution grâce aux contributions des utilisateurs.
- [Radio Pixel-Humain](https://www.communecter.org/#@radio-ph) : tu veux donner de la voix pour le Libre, pour les communs ? Viens réfléchir avec nous à la mise en place de notre radio ! (groupe de travail en lien avec le groupe de travail COzette ci-dessous)
- [Cozette](https://www.communecter.org/#@cozette) : _l_e futur média de Communecter ! Version papier, numérique, radiophonique, si tu veux contribuer, rejoins le groupe de travail pour Cozer avec nous


## COMMUNAUTÉ CONTRIBUTEURS
- **[COmmunecter.org](https://www.communecter.org/#@communecter)** : votre réseau sociétal libre : un bien commun à partager sans modération !
- **[Open Atlas](https://www.communecter.org/#@openAtlas)** : pour tous les sujets relatifs à l’entité juridique (asso 1901) Open Atlas et à ses membres
- **[Pixel Humain](https://www.communecter.org/#@pixelhumain)** : pour tous les sujets relatifs au mouvement coopératif des Pixels Humains ou de Communecter
- [COnnections](https://www.communecter.org/#@connections) : les gens, les projets, les organisations que nous rencontrons et avec qui nous coopérons
- [coTranslate](https://www.communecter.org/#@cotranslate) : do u speak english ou une autre langue ? Do u want to translate CO ? We need you ! Ici aussi en Allemand : [Traduction allemande de Communecter](https://www.communecter.org/#@germanTranslationOfCommunecter)
- [Légalité pour COmmunecter](https://communecter.org/#@co-legal) : Licences, CGU, mentions légales, RGPD,... autant de sujets joyeux sur lesquels nous avons toujours besoin d'un petit coup de pouce par des personnes compétentes
- [CO-bar](https://www.communecter.org/#@cobar) : notre bar virtuel. Un petit espace où l'on parle de tout, et de rien !


## FINANCES
- **[Ensemble vers une SCIC Communecter !](https://www.communecter.org/#@coop)** Gros chantier de notre actualité, nous sommes en train de monter une Société Coopérative d'Intérêt Collectif, pour expérimenter l'innovation sociétale tous ensemble. Et ce n'est pas une mince affaire ! Rejoignez-nous dans cette aventure.
- **[Piliers de Communecter](https://www.communecter.org/#@cofinanceur)** : Groupe des co-financeurs du projet Communecter. **Merci** à chacun et chacune d'entre vous de faire vivre ce commun !
- [Crowdfunding](https://www.communecter.org/#@crowdfunding) : Groupe de travail sur nos campagnes de financement participatif (don récurent, don ponctuel sur certains projets)


## GROUPES LOCAUX D'UTILISATEURS
- [Développer Communecter sur la commune de **Besseges**](https://www.communecter.org/#@developperCommunecterSurLaCommuneDeBesseges) : Comment développer l'outil sur une commune de 3 000 habitants ? _"L'outil Communecter répond vraiment à ce que tente de développer l'association Ekomundi. Celle ci se propose donc de l'utiliser pour mener une action de développement local avec une part significative de la population du village des Cévennes ou elle est installée._"
- [Cartographie des communs :](https://www.communecter.org/#@cartographieDesCommuns) A **Toulouse**, projet de cartographie des communs
- [GLU Communecter **Saint Denis**](https://www.communecter.org/#@gluCommunecterSaintDenis) **(La Réunion)**: "Se regrouper pour échanger autour du bien commun qu'est l'outil Communecter, créer du lien entre les utilisateurs de l'outil, bénéficier d'un canal de communication organisé localement pour transmettre des nouvelles à nos utilisateurs : voilà l'objectif du GLU Communecter ! Amis de Saint Denis : alons retrouv a nou pou kosé"
- [Créer un débat Zéro Déchet sur communecter pour Lyon](https://www.communecter.org/#@creerUnDebatZeroDechetSurCommunecterPourLyon) : "Avec des partenaires de **Lyon** et de **Montpellier**, nous souhaitons créer un cas pratique d'utilisation de Communecter. Et c'est le sujet du zéro déchet qui a été choisi :) L'idée de mettre en réseau les acteurs locaux et ainsi développer une synergie citoyenne."
- [Contributeur Communecter Lille](https://www.communecter.org/#@contributeurCommunecterLille) : "Dans ce groupe sont réunis toutes les personnes qui contribuent à Communecter et qui se sont données pour but de faire de CO l'outil privilégié des personnes à la recherche de ce qui se passe dans leur localité" (à **Lille**) en lien avec [Evénement Communecter lillois :](https://www.communecter.org/#@evenementCommunecterLillois) organisation d'événements pour les Communecteurs Lillois
- [Communectez vous les Montreuillois !](https://www.communecter.org/#@communectezVousLesMontreuillois) Xavier cherche des co-mmunecteurs et donne le défi d'intégrer 100 personnes et 50 projets/organisations de **Montreuil** sur CO !
- [Faciliter la découverte et l'usage de communecter dans le Diois](https://www.communecter.org/#@aide-communecter-diois) : Outil de partage et collaboration pour le **Diois** : des questions, des retours c'est ici !
- [**Bretagne** Communecter](https://www.communecter.org/#@bretagneCommunecter) : Groupe pour fédérer les utilisateurs bretons de Communecter : allons Breizhcommunecter !
- [Développer l'usage de communecter sur la commune de **Kingersheim**](https://www.communecter.org/#@developperLusageDeCommunecterSurLaCommuneDeKingersheim)


## AMBASSADEURS
- **[Ambassadeurs de Communecter](https://www.communecter.org/#@coambassadeurs)** : Les ambassadeur·ice·s locaux développent l'usage de Communecter sur leur territoire ou au sein d'une communauté. Partage d'expériences, conseils, idées d'actions, ...
- [Ambassadeurs et ambassadrices Communecter Grand Est](https://www.communecter.org/#@ambassadeursEtAmbassadricesCommunecterGrandEst)
- [Ambassadrices et ambassadeurs Communecter Normandie](https://www.communecter.org/#@ambassadricesEtAmbassadeursCommunecterNormandie)
- [Ambassadrices et ambassadeurs Communecter Hauts de France](https://www.communecter.org/#@ambassadricesEtAmbassadeursCommunecterHautsDeFrance)


## DÉVELOPPEURS
- [coDev](https://www.communecter.org/#@codev) : Espace dédié aux développeurs de CO
- [Open Hackers](https://www.communecter.org/#@openHackers) : Coding party and so on !
- [CORD Collaborative Open R&D](https://www.communecter.org/#@cordCollaborativeOpenRd)


## BUGS / AMÉLIORATIONS
- [coBugs](https://www.communecter.org/#@cobugs) : dédié aux problèmes des utilisateurs. Tu remarques quelque chose de bizarre sur la plateforme et tu veux le signaler aux dev ? Tu te demandes si telle réaction est un bug ou si c'est normal ? C'est par ici que ça se passe !
- [Communecter Testeurs](https://www.communecter.org/#@cobaye) : Groupe de testeurs de CO depuis le groupe facebook "l'exode"


## PROJETS OPEN ATLAS (keskonfé à part CO?)
- [COBus (AFNIC)](https://www.communecter.org/#@cobus) : Un bus sponsorisé par l'AFNIC (merci!) pour aller dans les zones déconnectées pour y valoriser les acteurs et les activités locales (974)
- [Kiltir Lab : À](https://www.communecter.org/#@kiltirlab) Saint Pierre (974) ou ailleurs, tiers-lieu, coworking, jardin partagé, fablab, conseils citoyens, évènements culturels, et bien d’autres.
- [Comobi](https://www.communecter.org/#page.type.projects.id.59a123b040bb4e4e5ffca350) : Une application mobile pour Communecter !
- [Smarterre](https://www.communecter.org/#page.type.projects.id.5979951740bb4e293fe836cb) : Territoires intelligents et innovation sociétale : Smarterre est la boussole qui guide nos actions
- [L.I.L.A.](https://www.communecter.org/#@arianeLila) : méthodologie numérique d'accompagnement et de structuration destinée aux individus, et aux organisations
- [OPALO](https://www.communecter.org/#@opalo) : Open Atlas s'Organise ! Projet de COstum et de cartographie dynamique pour rendre plus lisible en interne et en externe le fonctionnement d'Open Atlas. Cas pratique pour le projet L.I.L.A
- [Tiers lieux connectés](https://www.communecter.org/#@tiersLieuxConnecte) : Créer grâce à Communecter un outil commun et open source de partage d'expériences et de connaissances, de gestion, et d’animation des Tiers Lieux
    

## ORGANISATION D'ÉVÈNEMENTS
- [Apéros Communecter](https://www.communecter.org/#@aperosCommunecter) : "Une fois par mois on se retrouve dans un lieu ressource tel un café citoyen par exemple. On ramène chacun un pc et on ajoute du contenu sur communecter (c'est plus sympa de bosser ensemble sur un même sujet que tout seul). En plus de ça, ça nous permet de mettre en commun nos expériences utilisateurs pour faire des demandes directs aux développeurs." Concept initié à Lille, à essaimer partout !
- [Le tour de France des Communecteurs](https://www.communecter.org/#@leTourDeFranceDesCommunecteurs) : Objectif : répandre l'utilisation de l'outil Communecter auprès des acteurs du territoire (citoyens, associations, collectivités etc) grâce à la mutualisation et au partage
- [Proposer un stand au forum des usages coopératifs de Brest pour Communecter](https://www.communecter.org/#@proposerUnStandAuForumDesUsagesCooperatifsDeBrestPourCommunecter) : Comment organiser un stand Communecter au sein d'un autre événement ?
- [Libre en fête Réunion](https://www.communecter.org/#@libreEnFeteALaReunion) : organisation d'une journée de rencontre autour du libre sur l'île de La Réunion


## DIVERS
- [Analyse des usages de Communecter](https://www.communecter.org/#@analyseDesUsagesDeCommunecter) : comprendre l'utilisation (ou non) de Communecter
- [URL des pantoufles](https://www.communecter.org/#@urlDesPantoufles) : Toutes nos url, délirantes, critiques, démocratiques, utopiques
- [PADI : Pensées Art Dicton Idées](https://www.communecter.org/#@padi) : partage de ressources ;)
