# replicabilité

>Description du caractère diffusable et/ou réplicable du service ou du produit :Une approche sectorielle et territoriale 

Testée sur des territoires d’expérimentation (La Réunion, Toulouse...)
Produire des méthodes et des outils réplicables
L'appropriation est essentielle à la réussite du projet, c’est ce qui assurera en partie le remplissage, la diffusion et la réplicabilité. 
Les acteurs co-construisent le réseau, se l’approprient et le partagent aux différentes cibles du projet 
Garanti par la nature des licences de logiciel libre, l’outil ne fera qu’évoluer et s’améliore par l’usage et proportionnellement aux demandes et propositions qui seront faites par sa communauté qui est déjà présente et mobilisée autour de celui-ci. Ce projet est le point de départ d’une dynamique de création de valeurs dédié à l’animation et au soutien du secteur culturel 
Le projet COmmunecter est documenté et continuera à l'être pour le contexte du projet culture, pour permettre une réutilisation pour d’autre contexte, et le code étant ouvert il pourra être ré-adapté pour d’autres usages.
