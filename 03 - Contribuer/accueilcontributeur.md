# Accueil contributeurs

### Construisons en commun le futur de nos territoires
Le réseau sociétal libre [Communecter](https://www.communecter.org) est un projet porté par l'association à but non lucratif [Open Atlas](http://open-atlas.org/), et une communauté de contributeur·ice·s bénévoles répartie dans le monde entier.

Le projet est structuré en groupe de travail : **[voir la liste](/3 - Contribuer/groupesdetravail.md)**.


### Le coup de pouce du colibri
Mis à jour le 11 mai

-   **[Mise à jour du Code Social](https://docs.google.com/document/d/1k1XmVgh4e5vKQozFxxEzYBL_0uWISbJCX9m0cZUhvp4/edit?usp=sharing)**
-   **[Coopérative](https://docs.google.com/document/d/1ath12F-XPupfS1pd_A69pI-j5ZoBoU7B8q9JSz_Gp5o/edit?usp=sharing)**

Pour s'organiser nous avons mis en place divers outils qui ont chacun une fonction précise. Rocket.Chat nous permet de discuter, Nextcloud de partager des ressources, HackMD nous aide à écrire la documentation collaborativement, etc... En savoir + : **[Utiliser les outils internes](/3 - Contribuer/outilsinterne.md)**.

Si vous devenez contributeur régulier, vous pouvez rejoindre **[le groupe des contributeurs de Communecter](https://www.communecter.org/#@communecter.view.directory.dir.contributors)**. Vous pourrez alors accéder à **[l'espace de gouvernance du projet](https://www.communecter.org/#page.type.projects.id.55dafac4e41d75571d848360.view.coop.room.59ccc34a40bb4e7e50991bb3)**.
