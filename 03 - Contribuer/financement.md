# Rechercher des financements
En savoir + : [Modèle économique](/1 - Le Projet/modeleeconomique.md).

Aujourd'hui, Communecter n'est pas maintenable exclusivement par une équipe bénévole. Pour pouvoir continuer à améliorer la plateforme nous surveillons les appels à projets et proposons divers partenariats.

Les glaneur·euse·s font un travail de prospection auprès des entités possédant de l'argent pour soutenir la plateforme dans son évolution. Cela peut être des particuliers, des collectivités locales, des services publics, des fondations, des associations, des entreprises, ...


## Surveiller
-   Appels d'offres de collectivités : [boamp](https://www.boamp.fr/)
-   Subventions au titre du contrat de ville : tapez "subvention contrat de ville [nom de votre ville]" dans un moteur de recherche
-   [Budgets participatifs](https://fr.wikipedia.org/wiki/Budget_participatif)


## Répondre à un appel à projets
La méthode actuelle consiste à ouvrir un Google Docs dans [le dossier "Demandes de FINANCEMENT"](https://drive.google.com/drive/folders/0B0BMYNdshloMTDh0N3dhcThvZ1k) afin de rédiger la réponse collaborativement.


### Dossiers déposés
<table id="bkmrk-date-limite-intitul%C3">
<thead><tr>
<th>Date limite</th>
<th>Intitulé</th>
<th>Notre réponse</th>
<th>État</th>
</tr></thead>
<tbody>
<tr>
<td>15/10/18</td>
<td><a href="https://fondationlafrancesengage.org/concours-outre-mer/">La France s'engage outre mer</a></td>
<td><a href="https://docs.google.com/document/d/1ZNr-bwqpNsgB7WF0gFEi63i1OLEV_4F4FMmGW2mVPrQ/edit">Document</a></td>
<td>Refusé</td>
</tr>
<tr>
<td>26/09/18</td>
<td><a href="http://www.lesprix-ess.org/">Prix de l'ESS</a></td>
<td><a href="https://drive.google.com/open?id=1qfKpW80Hr3ZZAx9APrggQMMhk-xAatDR1yfmOlQMBlE">Document</a></td>
<td>Refusé</td>
</tr>
<tr>
<td>24/08/18</td>
<td><a href="https://www.letransformateur.fr/7e-appel-a-projets-special-reunion" target="_blank" rel="noopener">Le Transformateur</a></td>
<td><a href="https://docs.google.com/document/d/19o4S-NMjOxEdmkWLyVkgUTJktE16-PeGNE8f3IDeLDw/edit?usp=sharing" target="_blank" rel="noopener">Document</a></td>
<td>En cours de co-écriture</td>
</tr>
<tr>
<td>27/06/18</td>
<td><a href="http://www.latitudes.cc/appel-a-projets" target="_blank" rel="noopener">Latitudes</a></td>
<td><a href="https://docs.google.com/document/d/1uYLJTMC8IukIHemPfezeWUBZKI46diYLmmm3Cwd6aqg/edit?usp=sharing" target="_blank" rel="noopener">Document</a></td>
<td><strong>Accepté</strong></td>
</tr>
<tr>
<td>22/06/18</td>
<td><a href="https://budgetcitoyen.pasdecalais.fr/" target="_blank" rel="noopener">Budget participatif du Pas-de-Calais</a></td>
<td><a href="https://docs.google.com/document/d/1q6NShUvzrFPcg5XfN2iQd-4qRwSitPv5M-p34sBhFA0/edit?usp=sharing" target="_blank" rel="noopener">Document</a></td>
<td>Fini</td>
</tr>
<tr>
<td>31/05/18</td>
<td><a href="https://www.fondationdefrance.org/sites/default/files/atoms/files/appel_a_projets_emplo_et_activite_2018_.pdf" target="_blank" rel="noopener">Emploi et activité : des solutions innovantes et solidaires pour une société numérique intégrante</a></td>
<td><a href="https://docs.google.com/document/d/1zWXGVPU9XxNg1XgP1WGTTaLKsL9aNYYqxuVU0fYwa4I/edit" target="_blank" rel="noopener">Document</a></td>
<td>Envoyé</td>
</tr>
<tr>
<td>31/05/18</td>
<td><a href="http://www.reportersdespoirs.org/bicentenaire/" target="_blank" rel="noopener">Prix du bicentenaire</a></td>
<td> </td>
<td>Refusé</td>
</tr>
<tr>
<td>27/05/18</td>
<td><a href="https://www.fondation-free.fr" target="_blank" rel="noopener">L'intelligence collective au service de la transition numérique du territoire</a></td>
<td><a href="https://docs.google.com/document/d/1AR33eorx6u9-TsJ2mppWcGXE3CC1iSGuwVaQnRwo5k0/edit?usp=sharing" target="_blank" rel="noopener">Document</a></td>
<td><strong>Accepté</strong></td>
</tr>
<tr>
<td>13/05/18</td>
<td><a href="http://www.economie.grandlyon.com/actualites/prix-de-linnovation-le-monde-smart-cities-2018-appel-a-candidatures-2584.html" target="_blank" rel="noopener">Prix de l'innovation Le Monde Smart Cities 2018</a></td>
<td><a href="https://drive.google.com/drive/folders/1sSxAWOU4gg9PQd-lJH0ZfIY_hzKIOueC?usp=sharing" target="_blank" rel="noopener">Document</a></td>
<td><strong>Accepté</strong></td>
</tr>
<tr>
<td>22/04/18</td>
<td><a href="http://reunion.dieccte.gouv.fr/APPEL-A-PROJET-Accompagnement-au-deploiement-du-Comite-Strategique-de-Filiere" target="_blank" rel="noopener">CSFR Numérique</a></td>
<td><a href="https://docs.google.com/document/d/1pALaL8QUURWV8MI7Atr5xvl1YVZKBJGF2bE3L4jn1bM/edit?usp=sharing" target="_blank" rel="noopener">Document</a></td>
<td>Refusé</td>
</tr>
<tr>
<td>30/03/18</td>
<td>Création d'une interface numérique de diffusion de l'emploi et de la formation (Le Port)</td>
<td><a href="https://docs.google.com/document/d/1Pze8xfiI5VwxMlNyrO6gymH1c6xV3D35SXIBIAyT1o4/edit?usp=sharing" target="_blank" rel="noopener">Document</a></td>
<td><strong>Accepté</strong></td>
</tr>
<tr>
<td>28/02/18</td>
<td><a href="https://www.monprojetpourlaplanete.gouv.fr/" target="_blank" rel="noopener">Mon projet pour la planète</a></td>
<td> </td>
<td>Non envoyé</td>
</tr>
<tr>
<td>31/01/18</td>
<td><a href="http://share-it.io/" target="_blank" rel="noopener">ShareIT.io</a></td>
<td><a href="https://docs.google.com/document/d/10BMqKl2NIs7NGJRTmtMzrNM0_2vCIXHgKjVltFX8QYw/edit?usp=sharing" target="_blank" rel="noopener">Document</a></td>
<td>Refusé</td>
</tr>
<tr>
<td>21/01/18</td>
<td><a href="http://www.fondationlafrancesengage.org/les-concours/le-concours-national/" target="_blank" rel="noopener">La France s’engage</a></td>
<td><a href="https://docs.google.com/document/d/1iJy8SBu6Y2WJ-pQGeaREz7bQmheAME-2-hF0VFEGGlE/" target="_blank" rel="noopener">Document</a></td>
<td>Refusé</td>
</tr>
<tr>
<td>22/09/17</td>
<td><a href="https://www.assisesdesoutremer.fr/project/concours-projets-outre-mer/collect/depot-des-projets" target="_blank" rel="noopener">Concours Projets Outre-mer</a></td>
<td><a href="https://docs.google.com/document/d/1BGXTafKx3p3dG7LH2hbSIZrYT2U05OZbxOQpb6rOv2o/edit" target="_blank" rel="noopener">Document</a></td>
<td>Refusé</td>
</tr>
<tr>
<td>17/01/18</td>
<td><a href="https://api-site-cdn.paris.fr/images/96372" target="_blank" rel="noopener">Innovation publique et démocratique (ville de Paris)</a></td>
<td><a href="https://docs.google.com/document/d/1vTry4B_UhdjbrzE4-2ntM5Zp6aB2GIGYZqNofrJEtKE/edit" target="_blank" rel="noopener">Document</a></td>
<td>Refusé</td>
</tr>
<tr>
<td>27/10/17</td>
<td><a href="https://entrepreneur-interet-general.etalab.gouv.fr/defi/2017/09/26/socialconnect/" target="_blank" rel="noopener">SocialConnect</a></td>
<td><a href="https://docs.google.com/document/d/1x0rEzCyQabTpfQFQijzAEq2oTcdiFDiWUuLTqD_1RFw/edit" target="_blank" rel="noopener">Document</a></td>
<td>Refusé après pré-selection</td>
</tr>
<tr>
<td>08/10/17</td>
<td><a href="https://www.systeme-d.co/postuler" target="_blank" rel="noopener">Système D</a></td>
<td><a href="https://docs.google.com/document/d/1OC7jEZoQx_G6GVHj8pMjsaUAO7l2ws5TPRNUzu2ZZJ4/edit" target="_blank" rel="noopener">Document</a></td>
<td>Refusé</td>
</tr>
<tr>
<td>22/09/17</td>
<td><a href="http://prixfondation.cognacq-jay.fr/" target="_blank" rel="noopener">Prix Fondation Cognacq-Jay</a></td>
<td><a href="https://docs.google.com/document/d/1DP10CUkMNPUkOPtsorNKpfe9tgiBZo9abrgh4oNlk14/edit" target="_blank" rel="noopener">Document</a></td>
<td>Refusé</td>
</tr>
</tbody>
</table>
