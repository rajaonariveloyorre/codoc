# Search inside CO



http://dev.cte3.communecter.org/co2/search/globalautocomplete 
runs through de 
co2 module > search controller > GlobalAutoCompleteAction action
- $res = SearchNew::globalAutoComplete($searchP);
    - $results = self::querySearchAll($post, $fields);
            all query params are put together inside 
            $searchParams
```
searchTypeOrga 
searchType 
options
ranges
startDateUTC
onlyCount
countType
indexMin
indexStep
initType
countResult
searchType
searchTypeOrga
fields
sortBy
```
self::getQueries
    builds the query, adds up all requested parameters needed for the search 
self::getResults
    applies the search queries and finds the data 
    possible to convert the Results for a given context
    $results = Costum::sameFunction($convertMethodName, $results);
        ex : convertDataForProposals

filters.js


ex : home search page territoire
http://127.0.0.1/costum/co/index/slug/ctenat#territoires 
ladd ajax http://127.0.0.1/co2/app/search/page/territoires
view /co2/views/app/search.php 
    #search-content

ex : filter ou app territoire
```
name    "reunion"
locality    ""
searchType[]    "cities"
searchBy    "ALL"
indexMin    "0"
indexMax    "30"
costumSlug  "ctenat"
```

ex : filter by domaine d'action
http://dev.cte3.communecter.org/co2/search/globalautocomplete 
```
searchType[]    "projects"
indexMin    "0"
initType    ""
count   "true"
countType[] "projects"
indexStep   "30"
filters[category]   "cteR"
costumSlug  "ctenat"
```



ex : cte admin search 
http://dev.cte3.communecter.org/co2/search/globalautocompleteadmin
```
name    "tco"
searchType[]    "projects"
indexMin    "0"
initType    ""
count   "true"
countType[] "projects"
indexStep   "30"
filters[category]   "cteR"
private "true"
costumSlug  "ctenat"
```


# Admin Searching

sur une page ou une view comme 
http://127.0.0.1/costum/co/index/slug/ctenat#admin.view.territory 
charge http://127.0.0.1/costum/default/adminpage/ #content-view-admin
construit dynamiquement la view : **/custom/default/newgroupAdmin**
qui lance 
```
filterAdmin = searchObj.init(paramsFilter);
filterAdmin.search.init(filterAdmin);
```

/var/www/dev/modules/co2/views/admin/index.php
qui charge /js/default/admin.js


## var adminPanel = {
qui peut etre complété ou surchargé pour les costum
$adminConstruct=$this->appConfig["adminPanel"];
adminPanel.init()

## var adminDirectory = {

L'admin est faite de tableau de recherche drivé par des configurations
title : titre de la page
types : la ou les collection à chercher
table : les champs à dessiner 
paramsFilter : les filtres disponible sur le tableaux

## adminPanel.views 
lists all possible views that can be opened in the navigation 

## adminDirectory.values
are functions to get the value of any field in the e (current document data map) json map

## adminDirectory.actions
used in the actions dropdown 

## adminDirectory.events
are real posted requests send by potential actions

```
adminPanel.views.territory = function(){
    var data={
        title : "Gestion des Territoires",
        types : [ "projects" ],
        table : {
           ...
        },
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
                ...
            },
            filters : {
                ...
            }
        }
    };
```

