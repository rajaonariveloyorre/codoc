-   [CO aka COmmunecter](https://communecter.org)
-   [COMOBI on play store](https://play.google.com/store/apps/details?id=org.communecter.mobile&hl=fr)
- **[Installer Communecter](/4 - Documentation technique/Installer Communecter)**
- **[Introduction au code](/4 - Documentation technique/introcode.md)**


## MODULES
-   [MOTEUR DE RECHERCHE TERRITORIAL](https://www.communecter.org/#search)
-   [AGENDA](https://www.communecter.org/#agenda)
-   [NEWS](https://www.communecter.org/#live)
-   [CLASSIFIEDS](https://www.communecter.org/#annonces)
-   [CHAT](http://chat.communecter.org) ([doc](/2 - Utiliser l'outil/Fonctionnalités/chat.md))
-   [API](https://www.communecter.org/api) ([doc](/4 - Documentation technique/api.md))
-   [NETWORK](https://www.communecter.org/network) ([doc](/2 - Utiliser l'outil/Fonctionnalités/network.md))


### Modules exist in CO but direct links are coming soon to
-   PEOPLE
-   ORGANIZATIONS
-   EVENTS
-   PROJECTS
-   POINTS OF INTEREST
-   DDA : Discuss Decide and Act ([doc](/2 - Utiliser l'outil/Fonctionnalités/espaceco.md))
-   SOCIAL BOOKMARKING
-   PLACES NEEDS SERVICES COMPETENCE (soon)
-   GLOOTON : GENERIC IMPORT TOOL
-   BABELFISH : GENERIC Semantic translation and conversion


## INSTANCES
Basé sur le code de Communecter
-   [GRANDDIR.re](http://www.granddir.re)
-   [NOTRAGORA](http://www.notragora.com)
-   Et bientôt plein de petits **CoPi** ([doc](/4 - Documentation technique/copi.md))


## OPEN SOURCE TOOLS WE USE ([doc](/3 - Contribuer/outilsinterne.md))
-   [WEKAN](http://wekan.communecter.org)
-   [ROCKET CHAT](http://chat.communecter.org)
-   [NEXTCLOUD](http://cloud.co.tools)
-   [WIKI](/)
-   [CODIMD](https://codimd.communecter.org/)


## CONNECTING THINGS TOGETHER
-   [COPI.concept](https://docs.google.com/presentation/d/1efQiAdOt54_XoxJaYZPazxCK0T83jdgdmer-le9NwDY/edit#slide=id.gdd654f576_0_6) ([doc](/4 - Documentation technique/copi.md))
-   [INTEROPERABILITY](http://communecter.org/co2/#interoperability) ([doc](/4 - Documentation technique/interop.md))
-   [COPEDIA](http://communecter.org/co2/#interoperability.copedia)


## OPEN SOURCE PROJECTS WE USE AND LOVE
-   [PHP](http://php.net/)
-   [MONGO DB](https://www.mongodb.com/)
-   [JQUERY](https://jquery.com/)
-   [BOOTSTRAP](http://getbootstrap.com/)
-   [METEOR](https://www.meteor.com/)
-   [NODEJS](https://nodejs.org/)
-   [NOUN PROJECT](https://thenounproject.com/)
-   and lots and lots and lots of pluggins !
