# Module graph communecter
- analyse de l'architecture modele vue controlleur<
- comprendre le fonctionnemment d'un controlleur
- utilisation de la librairie D3.js
- analyse du module graph
- mise place d'un controlleur dans le module graph
- mise en place d'une vue dans le module graph
- manipulation de données en json
- 1manipulation d'une base de données json
- mettre des données sur le graph("name")
- réapprentissage de l'algorithmique
- apprendre a récursivité (fibonnaci)


## Le graph nodes and edges (d3follow)
### Dans le controlleur (D3FollowAction)
- J'ai fait apelle a la base de données pour avoir les données sur les citoyens, quelle sont leurs - projets, leurs événements, leurs organisations, leurs followers...
- J'ai modifier le tableau $root pour achicher les données au bon format json du graph (nodes and edges).
- J'ai fait une sucssesion de condition avec (if , elseif , else , foreach) pour remplire le tableau $root avec les données de le base de données ( "name").
    

### Dans la vue (d3follow)
- Mettre les données du tableaux $root dans une variable et utiliser (json encode)
- Ajout des balises script de d3js pour faire apelle a la librarie de D3.
- Remdre les nodes cliquables (si on clique sur la node d'un follower sa affiche son propre graph avec ses propres données).
- Faire un fonction couleur pour les principaux nodes du graph.


### Autre
- j'ai fait d'autre type de graph(de la meme façon que le graph nodes and edges).
- graph avec des barre, graph ciculaire, graph hierarchical.
- Un algorithme récursif (fibonnaci) en javascript.


## Dans le module graph
- Création du controller LinearAction et de la view Linear pour mon style de graphe choisie
- Je me suis basé sur D3Action et de la view pour le principal
- Les modifications apportés au controller :
    - modification de la requête root pour s'adapter au format de donnée du graphe
    - L'affichage du nouveau graphe est simple car rien qu'avec la base du graphe D3, l'affichage des nodes est possible
    - modification dans la boucle foreach de "links" pour s'adapter au nouveau graphe et ainsi afficher les noms de chaque organisation, projets, évènements...
    - ajout des données comme : "Mes experiences ", "Sondages","Actions", "Abonnées" : une boulce pour chaque catégorie qui parcours la base de donnée et l'affiche comme les autres données comme projets
    - ajout de nodes cliquable : juste après la dernière donnée (dernière organisation ou projet), une node "ajouter" est visible et ouvre le formulaire de la catégorie choisie (formulaire d'ajout d'organisation ou de projet)
    - ajout d'une node libre : à la fin du graphe il y a une node que lorsque l'on clique un formulaire s'affiche. Ce formulaire permet d'ajouter une donnée libre au graphe
- Modification de la view :
    - ajout de l'algorithme du graphe
    - ajout des librairies d3.js Modification de la fonction "click" pour ajouter des liens sur les nodes : exemple en cliquant sur une organisation cela affiche le graphe de l'organisation
    - ajout des liens sur les nodes "Ajouter..." qui affiche le formaulaire de la catégorie choisie. Pour ce faire il fallait aller chercher les librairies et les formulaires appelé "dynform"
    - Ajout d'une fonction Color qui affiche les mêmes couleurs que le site voici mon lien gitlab avec ton [mon travail](https://gitlab.adullact.net/benhoa974/graph/tree/stagiaire)
