# Introduction au code source
N’ayez pas peur ! Le code est écrit de manière accessible afin que vous puissiez y contribuer facilement. Si vous êtes à l’aise avec les méthodes plus “standard” dans l’industrie, il est possible de coder de votre côté et de communiquer via l’API.

Les discussions publiques de l'équipe de développeur se font dans le canal [#codev_open](https://chat.communecter.org/channel/codev_open).

## Ressources pour débuter
-   **[Présentation du code en vidéo (30 minutes)](https://www.youtube.com/watch?v=2rkE69--LxM)**
-   [Documentation de l’API](/4 - Documentation technique/api.md)
-   [Fonctionnement PHP](https://docs.google.com/drawings/d/1nSlaLy6ce7CWxvkXUbwIEYNVTeL6f35qm0CqoBjlvR0/edit?usp=sharing)
-   [Communauté des développeurs](https://www.communecter.org/#@codev)
-   [Pense-bête de commandes GIT](https://gist.github.com/aquelito/8596717)
-   [Feuille de route des développeurs](https://raw.githubusercontent.com/pixelhumain/communecter/master/docs/roadmap.org)

## Architecture
Le motif utilisé est le classique [Modèle-vue-contrôleur](https://fr.wikipedia.org/wiki/Mod%C3%A8le-vue-contr%C3%B4leur). Une grosse partie du code se trouve dans le [dépôt CO2 de notre Gitlab](https://gitlab.adullact.net/pixelhumain/co2), tandis que [citizenToolKit](https://gitlab.adullact.net/pixelhumain/citizenToolKit) gère plutôt le backend.
