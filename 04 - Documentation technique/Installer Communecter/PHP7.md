# installer php7 et mongo 3.6 pour dev sur ubuntu

prérequis avoir d'installer git, docker et docker-compose

### install docker
choose community edition CE
https://docs.docker.com/engine/installation/

**example install docker ubuntu**
https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/

**Post-installation steps for Linux**
https://docs.docker.com/engine/installation/linux/linux-postinstall/

### install docker-compose

https://docs.docker.com/compose/install/


### install start complete docker apache php7 auto script (ubuntu,docker,docker-compose,git,unzip)

installer sur la machine : ubuntu,docker,docker-compose,git,unzip


## install start complete docker apache php7
### clone du repo docker

créer un repertoire à la racine de votre user et cloner le repo docker

```bash
git clone https://gitlab.adullact.net/pixelhumain/docker.git ~/pixelhumain-php7/

cd ~/pixelhumain-php7/
```
changer de branch pour php7

```bash
git checkout php7
```

### build les services front (apache php7) et mongo (3.6)

créer le repertoire code/ dans le repertoire ~/pixelhumain-php7/
```sh
mkdir ~/pixelhumain-php7/code/
```

cette commande build le container pour le front à partir du dossier docker-front-apache

```sh
docker-compose -f docker-compose-apache.yml build
```


### clone des différents repo

Depuis le repertoire mkdir ~/pixelhumain-php7/code/ vous pouver cloner les différents repo

```sh
cd ~/pixelhumain-php7/code/

git clone https://gitlab.adullact.net/pixelhumain/pixelhumain.git pixelhumain
```

modules

```sh
mkdir modules
cd modules

git clone https://gitlab.adullact.net/pixelhumain/communecter.git communecter
git clone https://gitlab.adullact.net/pixelhumain/citizenToolkit.git citizenToolKit
git clone https://gitlab.adullact.net/pixelhumain/co2.git co2
git clone https://gitlab.adullact.net/pixelhumain/api.git api
git clone https://gitlab.adullact.net/pixelhumain/dda.git dda
git clone https://gitlab.adullact.net/pixelhumain/news.git news
git clone https://gitlab.adullact.net/pixelhumain/interop.git interop
git clone https://gitlab.adullact.net/pixelhumain/graph.git graph
git clone https://gitlab.adullact.net/pixelhumain/eco.git eco
git clone https://gitlab.adullact.net/pixelhumain/chat.git chat
git clone https://gitlab.adullact.net/pixelhumain/survey.git survey
git clone https://gitlab.adullact.net/pixelhumain/map.git map
git clone https://gitlab.adullact.net/pixelhumain/places.git places
git clone https://gitlab.adullact.net/pixelhumain/cotools.git cotools
git clone https://gitlab.adullact.net/pixelhumain/costum.git costum
```


### lancer composer depuis docker-compose
```sh
cd ~/pixelhumain-php7/
```
lancer les services
```sh
docker-compose -f docker-compose-apache.yml up -d
```
lancer sur le service front la commande pour installer les packages depuis composer
```sh
docker-compose -f docker-compose-apache.yml exec front composer config -g "secure-http" false -d /code/pixelhumain/ph

docker-compose -f docker-compose-apache.yml exec front composer install -d /code/pixelhumain/ph
```
### config mongo dans ~/pixelhumain-php7/code/pixelhumain/dbconfig.php

```sh
  cat > ~/pixelhumain-php7/code/pixelhumain/ph/protected/config/dbconfig.php <<EOF
  <?php

  \$dbconfig = array(
      'class' => 'mongoYii.EMongoClient',
      'server' => 'mongodb://mongo:27017/',
      'db' => 'pixelhumain',
  );
  \$dbconfigtest = array(
      'class' => 'mongoYii.EMongoClient',
      'server' => 'mongodb://mongo:27017/',
      'db' => 'pixelhumaintest',
  );
EOF
```
création repertoire et droit en ecriture
```
docker-compose -f docker-compose-apache.yml exec front mkdir -vp /code/pixelhumain/ph/{assets,upload,protected/runtime}

docker-compose -f docker-compose-apache.yml exec front chmod -R 777 /code/pixelhumain/ph/assets
docker-compose -f docker-compose-apache.yml exec front chmod -R 777 /code/pixelhumain/ph/upload
docker-compose -f docker-compose-apache.yml exec front chmod -R 777 /code/pixelhumain/ph/protected/runtime
```

### mongo data
```sh
docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.createUser({ user: 'pixelhumain', pwd: 'pixelhumain', roles: [ { role: "readWrite", db: "pixelhumain" } ] });
EOF

docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumaintest <<EOF
db.createUser({ user: 'pixelhumain', pwd: 'pixelhumain', roles: [ { role: "readWrite", db: "pixelhumain" } ] });
EOF

docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.lists.dropIndexes();
db.lists.remove({});
EOF

docker-compose -f docker-compose-apache.yml exec -T mongo mongoimport --host mongo --db pixelhumain --collection lists "/code/modules/co2/data/lists.json" --jsonArray

unzip "${PWD}/code/modules/co2/data/cities.json.zip" -d "${PWD}/code/modules/co2/data/"

docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.cities.dropIndexes();
db.cities.remove({});
EOF


docker-compose -f docker-compose-apache.yml exec -T mongo mongoimport --host mongo --db pixelhumain --collection cities "/code/modules/co2/data/cities.json"


unzip "${PWD}/code/modules/co2/data/zones.json.zip" -d "${PWD}/code/modules/co2/data/"


docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.zones.dropIndexes();
db.zones.remove({});
EOF


docker-compose -f docker-compose-apache.yml exec -T mongo mongoimport --host mongo --db pixelhumain --collection zones "/code/modules/co2/data/zones.json"


unzip ~/pixelhumain-php7/code/modules/co2/data/translate.json.zip -d ~/pixelhumain-php7/code/modules/co2/data/

docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.translate.dropIndexes();
db.translate.remove({});
EOF


docker-compose -f docker-compose-apache.yml exec -T mongo mongoimport --host mongo --db pixelhumain --collection translate "/code/modules/co2/data/translate.json"

docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.createCollection("applications")
db.applications.insert({     "_id" : ObjectId("59f1920bc30f30536124355d"),     "name" : "DEV Config",     "key" : "devParams",     "mangoPay" : {         "ClientId" : "communecter",         "ClientPassword" : "xxxx",         "TemporaryFolder" : "../../tmp"     } } )
db.applications.insert({     "_id" : ObjectId("59f1920bc30f30536124355e"),     "name" : "PROD Config",     "key" : "prodParams",     "mangoPay" : {         "ClientId" : "communecter",         "ClientPassword" : "xxxx",         "TemporaryFolder" : "../../tmp"     } } )
EOF


docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.citoyens.dropIndexes();
db.cities.dropIndexes();
db.events.dropIndexes();
db.organizations.dropIndexes();
db.projects.dropIndexes();
EOF


docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.citoyens.createIndex({"email": 1} , { unique: true });
db.cities.createIndex({"geoPosition": "2dsphere"});
db.cities.createIndex({"postalCodes.geoPosition": "2dsphere"});
db.cities.createIndex({"geoShape" : "2dsphere" });
db.cities.createIndex({"insee" : 1});
db.cities.createIndex({"region" : 1});
db.cities.createIndex({"dep" : 1});
db.cities.createIndex({"cp" : 1});
db.cities.createIndex({"country" : 1});
db.cities.createIndex({"postalCodes.name" : 1});
db.cities.createIndex({"postalCodes.postalCode" : 1});
db.events.createIndex({"geoPosition" : "2dsphere" });
db.events.createIndex({"parentId" : 1});
db.events.createIndex({"name" : 1});
db.organizations.createIndex({"geoPosition" : "2dsphere" });
db.projects.createIndex({"geoPosition" : "2dsphere" });
db.citoyens.createIndex({"geoPosition" : "2dsphere" });
EOF
```