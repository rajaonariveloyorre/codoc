# Open Atlas

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTT2ROnz906h3L0WoRosAi-VpT3VrP98wOLv2dQYiuu8lEt8ZU8h6HU08QCbcGjzQ/embed?start=false&amp;loop=false&amp;delayms=3000" width="830" height="495" frameborder="0" allowfullscreen="allowfullscreen"></iframe>


## Histoire
Depuis sa création en 2008, l'association Open Atlas travaille sur des projets liés aux
biens communs, à la cartographie et à la démocratie participative. C’est une
association locale qui œuvre au développement territorial à La Réunion.
- Développement et formation en logiciel libre
- Conseil en développement territorial
- Aide et soutien aux organisations à impact positif
- Cartographie de filière
- Hackathon

## La raison d'etre : Article 2 des statuts : Objet
L'association a pour objet d'être un lieu multidisciplinaire d'échanges permettant de développer
des projets oeuvrant dans des domaines de l’intérêt général, notamment autour de
l’économie de l’information et de la communication.

D’intérêt social et solidaire, l’association s’intéresse à tous les territoires, du local à
l’international.

Elle oeuvre, en toute diversité, dans les champs du développement humanitaire, culturel,
artistique, de la communication de services public ou privé, de la cartographie virtuelle,
de la connectivité et la mise à disposition de l'information, de la communication (au sein
d'un groupe). L’association se donne également pour objectif d’être un laboratoire et une
cellule active de recherche et de développement, en toute transversalité, défendant le
décloisonnement et le partage des compétences, ainsi que la liberté de penser et d’agir
Open Atlas promeut l’action et l’intelligence collective et l’initiative citoyenne, militant pour
la conjugaison de l’éthique et de l’efficacité, autour de l’économie numérique, oeuvrant dans le
principe de développement soutenable, durable et solidaire.

L’association concourt à la conception et à la mise en oeuvre d'outils et de systèmes
d’information et de gestion permettant d'atteindre les objectifs qu’elle se fixe dans les domaines
précédemment mentionnés.L'association inscrit ses projets dans une dimension d’intérêt général en s’ouvrant à tous les publics, notamment les plus fragiles, en préservant à ses activités un caractère non lucratif,
laïque et apolitique. En toutes circonstances, l'association garantit un fonctionnement
démocratique et transparent et préserve le caractère désintéressé de sa gestion.

## Une coopérative d’entrepreneurs
unis pour créer des solutions humaines et locales
spécialisée dans la communication et le numérique appliqués au fonctionnement de la société depuis 2007

## Nos devises
“1 + 1 = 3 | L'intelligence collective au service du citoyen”
“La réflexion individuelle au service de L'intelligence collective”

## Notre intention
L’open source ! Des épices, des recettes et du code source  libre pour créé des communs numériques libres d’utilisation et d’amélioration.

...
L’association est composée d’un Conseil d’Administration qui a élu parmi ses
membres :
Un Président : Luc Lavielle, La SALINE
Un Secrétaire : Béatrice Pothier 
Un Trésorier : Jerome Gonthier

Des salariés gèrent au quotidien la vie et les projets en expansion de l’association.
Une équipe de développeurs indépendants mettent en œuvre ces projets afin d’atteindre les objectifs fixés dans les statuts.

- 4 Développeurs indépendants confirmés et expérimentés
	+ Tibor KATELBACH, Développeur, Gérant bénévole (St-Pierre)
	+ Clément DAMIENS, Développeur (Métropole)
	+ Thomas CREPEAU, Développeur (Itinérant)
- Développeurs junior en acquisition de compétences :
- 2 Graphistes parallèlement en formation continue à l’ILOI :
	+ Agatha CONRAUX, Graphiste Designer (La Possession)
	+  Ambre COTONEA, Graphiste Designer (La Possession)
-  1 Animateur réseau local : Tom BAUMERT (Métropole)
- 1 Community Manager : Jeanne HENRY (St-Pierre)
- 1 Assistante Administrative : Marie-Jean BEAUPAIN (St-Pierre)

### Stagiaires
Nous accueillons régulièrement plusieurs stagiaires en : 
- développement logiciel,
- graphisme, 
- techniques commerciales (en savoir +)... 

### travail en équipe
- Agilité 
- 

### Lieu de travail
tout le monde est en télétravail
