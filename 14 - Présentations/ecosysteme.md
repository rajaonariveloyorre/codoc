# Un écosystème 
- Le savoir faire de l’association OPEN-ATLAS est la culture de l’ouverture qui est dans son ADN
- Disponibilité d’une plateforme numérique open-source collaborative modélisée comme un bien commun
- Liens historiques forts avec le secteur des FabLabs, des tiers-lieux, du monde associatif et des coopératives d’activités, lieux émergents de la recherche citoyenne ouverte 
