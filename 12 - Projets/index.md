# Divers Projets Type 

# Appel à Projet 
## CTE 
- **Costum** 
- un **Territoire** est un projet **Candidat** avec plusieurs états
- les territoires lauréats ouvre un **appel à projet écologie** locale
- les **fiches actions** proposées sont des projets
- Formulaire **COFORMS** à 4 étape 
    + Detail 
    + Caractériser (Domaine d'action, Objectif Dev Durable)
    + Murir 
    + Contractualiser
    + Suivre
- Arborescence 
    + National
        * Annuaire des territoires
        * Annuaire des actions
        * Dashboard : Observatoire de l'écologie 
        * Territoire 
            - Communauté (Partenaire, Financeur)
            - Chiffre Locaux, Observatoire Local
            - Fiche Action
                + Communauté (Partenaire, Financeur)

# Science Participative
## SOMMOM

# Filière et écosystème == Communauté Sectorielle
## Hub Ultra Marin 

# Citoyenneté 
## Pacte pour la transition 

# Collectivité et Territoire
## Ville de la Possession 