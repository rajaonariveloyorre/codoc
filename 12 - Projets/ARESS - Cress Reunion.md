# ARESS - Cress Reunion

Site : https://www.aress-reunion.re/

## Description

Une plateforme sociale et solidaire que vous alimentez, que vous partagez et que vous utilisez afin de faciliter votre quotidien et de consolider les liens dans votre quartier, dans votre ville et sur l'ensemble de l'île...

## Caractéristique du costum

- Pour que les organisations apparaisent dans `#search`, ils doivent être validé par les administrateurs.
- Les formulaires organisations et projet ont été surcharger avec différents inputs
- Formulaires organisations : Le numéro de SIRET est obligatoire, si on saisie un numéro de SIRET existant cela affiche la structures.
- 

## Fichiers spécifiques au costum hva

```
costum
|-----data
|     |-----cressReunion.json # paramètres du costum
|-----asset
|     |-----css
|     |     |-----cressReunion_filters.css #Style du costum
|     |-----js
|     |     |-----editInPlace.js # Surcharge les events de editInPlace.js de la page about.ph de co2
|     |     |-----admin.js # Gestions des vue de l'espace admin
|     |     |-----cressReunion_index.js # Init JS de HVA, surcharge notament le directory
|     |     |-----pageProfil.js # Surcharge pageProfil
|     |-----images
|     |     |-----banner.jpg
|     |     |-----logo.jpg
|-----models
|     |-----CressReunion.php # Class du costum Cress Reunion
|-----views
|     |-----about.php # surcharge la vue about.php de co2
|     |-----banner.php # OLD
|     |-----filters.php # Filtre des pages recherches
|     |-----footer.php # 
|     |-----home.php # Page d'accueil du costum
|     |-----importorga.php # Vue des imports (OLD)
|     |-----importsiret.php # Vue des imports (OLD)
|     |-----map.php # Page qui affiche la carto

```

