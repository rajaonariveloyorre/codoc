# ROCKET CHAT

# RC slash commands 
  [ ] /co show all COmmands, show as autocomplete
  
## myCO /co.xxx
  [ ] /co.tasks my task list
    works with /co+task ""
  [ ] /co.orga my orga list 
    [ ] /co.orga /open/ :  :my: orga list by regex 
  [ ] /co.proj :  :my: orga list by regex  

## search /co?
  [ ] /co? "open" : will :search: and show answers in chat
    [ ] /coPe? ou /co? p "open" : people search 
    [ ] /coOr? "open" : orga search 
    [ ] /coPr? "open" : project search
    [ ] /coEv? "open" : event search
  [ ] /co?. "open" : will :search: in my network and show answers in chat
  
## adding stuff /co+xxx

### add
  [ ] /co+ : add an element 

### project
[ ] /co+ project "TOTOTOTO" 
[ ] /co+ :url: 
  add type , fill metadata
  :chainedCommand:, keep lst added type and id 
  /co+ person >invites
  /co+ project ...etc

[ ] /co+ person "TOTOTOTO" : invites someone 
  [ ] /co+ person "TOTOTOTO" orga:openatlas : invites someone on element
  [ ] /co+ email
  [ ] /co+ p email
  [ ] /co+ o :email: invites an organzation to join
  [ ] /co+ o :url: :> scans the url for content, anf for emails
[ ] /co+ event "TOTOTOTO" startDate endDate
[ ] /co+ news "fd dsfqdsf qds f :link: :tags: #tag :reference: @peop @orga" with auto complete
[ ] /co+ news lastPost from chat
[ ] /co+ vote "blab proposal" startDate endDate

[ ] /co++ : presents as questions 
  > what : ex project
  > where  : ex openatlas
  > title 

### tasks
[ ] /cotask @smarterre ""  {{CODOO}} 
[ ] /cotask list
[ ] /cotask@smarterre
[ ] /cotask@oceatoon 

### copimi
[ ] /co+pad "qfds fdsfqdsfqdsf q" 
  adds to my {{COPIMI}}
* bookmarks
[ ] /co+ link :url:
[ ] /co links 
  gets all the links in the element connect to the channel 

###  ** COnnect 
[ ] /colink person to person 
[ ] /colink bea to benoit 

[ ] /color -2 pink, colors 2 posts before in light pink bg
ideas [[/var/www/dev/modules/copiMe/md/colang.md]]

# CO pluggin for RC
  an organization with a RC instance just install 
  links and invites all users to an element passed as param
## connect a channel to CO.element.news