
[présentation du CTE](https://docs.google.com/presentation/d/1BXqw50VpHbusHyw7Ub9t1uJtizGeJf20yWEAdXfdFeg/edit#slide=id.g9c10fd5ceb_3_0)
# todo 
- btn candidater et tester le lien avec le territoire 
- connecté une answer et un territoire 
    - via context : {
        idxxx : { type : yyy, name : zzz}
    }


# convert answers 
http://127.0.0.1/costum/ctenat/convertAnswers

# export database schema
http://127.0.0.1/survey/form/schema/id/ctenatForm
toujours copié la derniere version ici 
survey/data/ctenatForm.json 

# request mongo 
```
db.getCollection('forms').find({id:{$in:["ctenatForm","action", "caracter", "murir","contractualiser","suivre"]}})

db.getCollection('answers').find({formId:"project|caracter|murir|contractualiser|suivre"})

db.getCollection('answers').update({formId:"organization|project|caracter|murir"},{$set:{formId:"project|caracter|murir|contractualiser|suivre"}},{multi:1})

```

# Liste des territoires
http://127.0.0.1/costum/co/index/id/ctenat#territoires

# Voir un territoire 
http://127.0.0.1/costum/co/index/id/ctenat#@SigneLe9Avril2019

# Liste des fiches actions d'un territoire
http://127.0.0.1/costum/co/index/id/ctenat#@SigneLe9Avril2019.view.projects.dir.projects

# Dashboard 
http://127.0.0.1/costum/co/index/id/ctenat#@SigneLe9Avril2019.view.dashboard

# form candidater 
avant utilise dynform costumisé
todo : utiliser coform 

# form ajouter une fiche action en 3 étape
** project** | **caracter** | **murir** | **suivre**
## Configurer le coform
http://127.0.0.1/costum/co/index/slug/ctenat#form.edit.id.5f0584a8bae2c777804e4bd3

## Fiche Action 
### Lire 
http://127.0.0.1/costum/co/index/slug/ctenat#answer.index.id.5f085452539f222a69fea8f6.mode.r
### Ecrire
http://127.0.0.1/costum/co/index/slug/ctenat#answer.index.id.5f085452539f222a69fea8f6.mode.w

## Observatoire
http://127.0.0.1/costum/co/index/slug/ctenat#dashboard



# generate PDF
costum/views/custom/ctenat/pdf/ 
- answers
- copil
to Debug comment sections


# Search & Filtre

## Filtre Geo

Ajouter le filtre région
```,
scopeList : {
    name : "Region",
    params : {
         countryCode : ["FR", "RE"],
         level : ["3"]
    }
}
```
Pour ajouter un autre DOM-TOM, il faut ajouter le code country dans le parametre et éventuellement rajouter le level 3 a la zone si elle n'en possede pas .

Attention : Pour l'ajout d'un level a une zone, le mieux et de faire un bash pour les ajouter au élément également sinon la recherche sur les adresses ne fonctionnera pas. Vous pouvez prendre une partie du bash `actionRefactorReunion` pour parcourrir les éléments leur ajouter le level manquant a leur adresses


#### Info utile 
level 1 = Pays
level 2 = Canton , région etc en fonction d'un pays , exemple Wallonie , Fallonie pour la belgique
level 3 = Région Province etc en fonction d'un pays Pour la France sa sera les régions 
level 4 = Département, Arrondissement etc en fonction d'un pays Pour la France sa sera les département 
level 5 = EPCI pour la France 


## EXPORT CSV

##### JS
C'est json2csvParser qui se chargera de convertire la liste des élements en csv avec l'ordre des colonne passer en parametre. Voir le code dans co2/filters.js

`Faire attention au caractere # dans les string car cela bloque le parser`

#### Answers

##### PHP 
L'action qui récupere et traite la données ce fait ici : costum.controllers.actions.ctenat.api.AnswerscsvAction 

La donnée est retourner en JSON sous se format 

``` {
    "results" => $newList, // Liste des éléments après traitement
    "fields"=> $sortOrder, // L'ordre des colonnes pour le csv
    "allPath"=>$allPath // l'ensemble des chemins vers la données.
};
```

#### Territoire

#### EXPORT CSV
##### PHP 
L'action qui récupere et traite la données ce fait ici : citizenToolKit.export.csvAction 

C'est une fonction générique qui peut etre réutiliser sur n'importe quel element lier au systeme de recherche.

Si besoin de faire un traitement spéciale, voir le meme procèder que Answer et faire une action spécifique.

La donnée est retourner en JSON sous se format 

``` {
    "results" => $newList, // Liste des éléments après traitement
    "fields"=> $sortOrder, // L'ordre des colonnes pour le csv
    "allPath"=>$allPath // l'ensemble des chemins vers la données.
};
```



