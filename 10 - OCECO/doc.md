# OCECO DOCUMENTATION

## Les objectifs de OCECO
- Gouvernance horizontale 
    - créer de la transparence
    - [stigmergie](http://www.lilianricaud.com/travail-en-reseau/la-stigmergie-un-nouvelle-modele-de-gouvernance-collaborative/)
    - [holoptisme](https://www.ritimo.org/Holoptisme) 
- suivi de projet 
- indicateur d'avancement et d'impacte  
- coConstruction et proposition ouverte
- Priorisation des propositions 
- implication des acteurs de l'organisation 
- Améliorer les dynamiques collectives et le travail au sein des organisations, et plus globalement la qualité de vie au travail
- Favoriser l’implication et les changements de posture nécessaires à tous les niveaux de l’organisation
- Connecter des besoins et des offres 

## Holoptisme
L’holoptisme se définit comme un « Espace physique ou virtuel dont l’architecture est intentionnellement conçue pour donner à ses acteurs la faculté de voir et percevoir l’ensemble de ce qui s’y déroule», ou encore « un espace qui permet à tout participant de percevoir en temps réel les manifestations des autres membres du groupe (axe horizontal) ainsi que celles provenant du niveau supérieur émergeant (axe vertical). Jean François Noubel

## Stigmergie
La stigmergie est une méthode de communication indirecte dans un environnement émergent auto-organisé, où les individus communiquent entre eux en modifiant leur environnement.

Donc tout individu souhaitant participer à un système ouvert a la capacité de trouver par lui meme, sa place dans le fonctionnement d'un système complexe.

# Comment ca marche

## Community Driven and centered
OCECO : 
- ne fonctionne que s'il y a interaction de la communauté autour des propositions.
- peut créer une communauté au travers du partage et d'une volonté commune qui est le contexte porteur du OCECO mais sur tout permet de faciliter l'interaction
- créer de la transparence automatiquement 

4 étapes clefs : 
## Proposer
* Dans un pôle, un service, un programme/projet, sur un sujet, un enjeu particulier, les contributeur(ice)s / parties prenantes / collaborateurs/ postent leurs propositions.

## Évaluer (Décider)
* Permet de statuer, prendre des décisions sur les propositions faites lors de l'étape précédente (Proposer).
* Les modes de décision sont configurables et personnalisables (votes, par consentement, ...)
* Des rôles définissent qui peut participer aux prises de décision.
 
## Financer
* Affectation des financements ou enveloppes budgétaires aux propositions qui ont été sélectionnées à l'étape précédentes. La nature des financement sont spécifiés pour un meilleur suivi financier (financement via prestation, frais de fonctionnement, investissement interne, co-financement, ...)

## Suivre
- Plans et tâches détaillés par projet
- Affectation des ressources (humaines, financières, ...)
- Comités de pilotage dynamiques
- Tableaux de bord (global, par tâche, par contributeur/collaborateur)

# Déployer OCECO sur mon organisation
En quelques clicks Dans communecter, sur votre fiche organisation :
## **Voir tout les COForms**
Cliquez sur le bouton "COform" dans le menu
![](https://codimd.communecter.org/uploads/upload_dfada771045c569026087dfa909f7040.png)
## **Choisir un template de coform existant**
Cliquez sur le bouton "Menu template"
![](https://codimd.communecter.org/uploads/upload_b617cbf19e9f2938b185d3d9856dcd75.png)
## **Dupliquer Oceco pour cette organisation**
Cliquez sur "OCECOFORM PROCESS" 
![](https://codimd.communecter.org/uploads/upload_9762831fd27e4f6346fa30ab92032471.png)
## **Gestion de votre OCECO**
Apparition d'un panneau de gestion dans votre menu COform
![](https://codimd.communecter.org/uploads/upload_cca9c88bb985bb78e1e08048deff02c0.png)
## Gestion de votre OCECO
Au survol du panneau de gestion ![](https://codimd.communecter.org/uploads/upload_d36907b2776cd5ce9228e6abbb67efa2.png)

- ![](https://codimd.communecter.org/uploads/upload_e21c1f7ae911b842980a4b7d0b2abe31.png) Modifier / Personnaliser les questions

- ![](https://codimd.communecter.org/uploads/upload_eeb2edaeea6010fbbe58060e7bf183b7.png) Configurer les paramètres globaux du formulaire (Nom, description, Objectifs, accessibilité, ...) 
- ![](https://codimd.communecter.org/uploads/upload_f2aebcb7c6bf22fb9edc8bfc56187120.png) Se rendre sur l'interface / la plateforme dédié (Costum) à votre OCECO 
- ![](https://codimd.communecter.org/uploads/upload_e02d20eb4f577d477aa2a629204e9296.png) Lien url pour partager / diffuser votre formulaire OCECO auprès de ses utilisateurs
- ![](https://codimd.communecter.org/uploads/upload_dc18656c01189616d753070a6b9bda6b.png) Partager (url et intégration html) / Effacer (supprimer le formulaire OCECO) 


# Qui peut utiliser votre OCECO
Pour le moment 
- Ouvert à la communauté (membres de l'organisation ou du projet qui l'a créé) à laquelle s'adresse l'OCECO Form
    - les autres peuvent voir la fiche mais ne pourront accéder au processus OCECO ou à l'interface dédiée
    
- Gestion par rôles 
    - L'accessibilité aux différentes étapes du processus OCECO est coinfigurable par rôles att
    - 
### à venir bientot
- limiter aux admins
- pouvoir configurer quels écrans (espaces de l'interface) sont accessibles selon
    - public complet
    - par admin 
    - par membre
    - par role

# Utiliser OCECO

## Créer des proposition
La communauté réunit autour de l'organisation contribue ouvertement au fonctionnement, aux critiques, aux créations, à l'amélioration de celle ci en ajoutant des proposition.
Cela a pour effet de donner de la visibilité à la créativité et l'intelligence collective de l'organisation et permet à tout le monde de s'impliquer dans le fonctionnement de chaque organisation 
Le **formulaire d'ajout de proposition** d'origine permet de déposer : 
- Un titre
- Une description
- Un image
- une thématique 
- l'urgence
- Lignes de dépense

## Lignes de dépense
Les Lignes de dépense sont soit rempli par le créateur de la proposition, soit par une communauté d'expert capable de créer des ligne de dépense precise répondant au besoin de la proposition permettant ainsi de décrire précisement celle-ci dans la réalisation et la pratique.
Une ligne de dépense porte 4 attributs :
- 2 critère de classification libre : 
    - Groupé 
    - Nature de l'action
- Poste ou Libélé de l'action 
- Montant 

une fois toutes les lignes de dépense déclarées et claires, les acteurs (interne ou externe) pourront s'engager à réaliser les actions ligne par lignes, et éventuellement proposer un cout pour la réalisation de la tache. 
Cette approche permet de connaitre le cout d'une proposition et sur quelles ressources celle ci reposera pour se réaliser.
Enfin Chaque ligne pourra etre financer par différent type de financement (cf. Section Financement)
- budget particitif 
- contrat (financement unique)
- ...etc

de plus la consomation des budget pourra aussi prendre plusieurs forme (cf.section Suivi)
- prestation classique : paiement sur facture
- co Rémunération : self service dans un budget mis en commun

### Liste des propositions
Permet de naviguer, découvrir toutes les contributions faites dans l'organisation. Certaines Informations s'affichent en modal (popup) pour faciliter la navigation.

Trois **Format de visualisation** des propositions : 
- Complet ( gros block riche)
- Réduit (on affiche le sctricte minum)
- Compacte (3 proposition par lignes)

un **Moteur de Recherche** permet de filtrer et Naviguer dans la liste des propositions qui peut etre  : 
- recherche par nom
- filtres : 
    - Thématique
    - État (Retenu / Rejeté / En attente )
    - Urgence
    - Status 
    - Trier

## Évaluer une proposition
Dans l'objectifs de prioriser horizontalement les actions portés par l'organisation et de décider plus ou moins collectivement des orientations de celle ci, l'étape évaluation permet à une communauté d'évaluer les propositions faites à l'organisation 

### Définir les évaluateurs
Les politiques décisionnelles variant selon la gouvernance de chaque organisation. Elles définissent comment les décisions sont prises et par qui. Cette **communauté de décideur** peut etre paramétré et définie de trois facon : 
- ouverte à tous les membres de l'organisation
- administrateur seulemement
- membres portant un role défini

### Type d'évaluation
Les politiques décisionnelles variant selon la gouvernance de chaque organisation, plusieurs type d'évaluation sont disponible
- par vote
- par critère d'évaluation
    - par note
    - par étoile 
- par évaluation et quadrant Ariane

### Voir toutes les évaluations
l'évaluation globale permet d'avoir une vue d'ensemble de et les résultats des évaluations de toutes les propositions faite, permettant alors d'analyser et prendre les choix pour Retenir dans le temps les prochaines priorités.

## Financer une proposition
Dans l'objectifs de donner de la transparence aux financement du développement, fonctionnement ou des actions portés par l'organisation et de généré un bilan financier des actions. L'étape financement permet à une communauté de financeurs de financer les propositions.
Plusieurs modèle de financement peuvent etre expérimenté avec l'interface financement, les financement se faisant sur chaque ligne de dépense déclaré sur la proposition.
- associer un ou plusieur financeur et un montant à nue ligne d'action de la proposition

### Transformatiopn en projet
à tout les acteurs de l'organisation peuvent transformer la proposition en projet en un simple click. 
Cela a pour effet d'enclencher l'étape de suivi de projet rattacher aux outils ocecoMobi.
une fois en mode Projet, celui ci possède tout les capacités d'un projet communecter : 
- Journal 
- Agenda
- Communauté
- sous projets 
- annonces
- points d'interets
- créer un costum dédié 

## Suivre une proposition
Dans l'objectifs de donner de la transparence et du suivi au déploiment des proposition et d'avoir des indicateur mesurable des résultats ainsi qu'une vision transparente et quasi temps réélle de la consommation des budget qui font tourner l'organisation permettant de généré un bilan d'actoivité des actions menées  . 
L'étape suivi, permet de reellement suivre action par action dont on listera toutes les sous actions, l'aboutissement de chaque sous action et donc de chaque action. 
D'avoir une vue en détail de qui a fait quoi, en combien de temps et donc combien ca a couté, permettant de nourir activement l'observatoire de lap proposition et automatiquement l'observatoire globale.

L'étape suivre sert comme une vraie interface de gestion de projet :
- chaque action (ligne de dépense) est rattachée à une liste de sous tache 
- déclarer un référent par actions
- déclarer des taches à chaque action
- assignation des taches
- date de fin de réalisation des taches
- cout des taches 
- déclarer une fin de tache
- validation d'action 
- validation du budget rattaché à une action e

Assez naturellement et organiquement une communauté se mobilise autour de la proposition initiale.
Des engagements sont validés sur les lignes dépense. 
La mise en oeuvre se déroule naturellement avec une prise en compte et une validation des travaux effectué, valorisant la ou les acteurs de l'action ou des taches.
Le Budget se voit consommer en transparence pour l'organisation, on peut utiliser l'outil comme interface de déclaration de consommation d'un budget contributif, dans le cadre d'une coRémunération par ex.

### OcecoMobi
Une application mobile est disponible, pour la partie suivi, elle est le miroir de l'information décrite dans l'étape de suivi et permet pas mal de chose 
- déclarer de nouvelles actions
- déclarer des sous tache par action
- assignation de tache 
- totalement connecté au Rocket Chat  

## Observatoire d'une proposition
L'objectif de l'observatoire est de donner à voir au fur et à mesure de l'avancement de la proposition autant les aspects de gouvernance autour de la proposition, les ressources , les finance et les actions associés. les mouvements, les délais, les synthèses et les détails qui permettront d'agir convenablement et en conséquence sur la propoisition.

### Observatoire globale
Les organisations sont des organismes vivants, évoluants et bifurquant en permanence. C'est l'objectif de l'observatoire globale, de voir, comprendre, donner de l'holoptisme aux intéréssé et voir tout ce qui se passe de facon globale, cumulant toutes les propositions, les projets, les actions, les acteurs , les délais, les engagments et problème qui pourrait emerger.

## Méthodologie et animation associé à l'outil
L'outil OCECO vient accompagner tout type d'organisation et s'adapte au fonctionnement existant. Cependant l'approche elle meme demande qlqs adaptation de posture et de positionnement face à l'ouverture à la contribution permanente des acteurs. De bien définir la gouvernance et les type de financement souhaités. L'animation qui accompagne le déploiement de l'outil permet de faciliter , d'étudier et adapté au plus pret du fonctionnement de l'organisation ou des souhait de changement de celle ci. 

# Paramétrer votre OCECO
Things you can parameter and configure
- Name of the Form 
- Description
- Active / Not Active
- Private / Public
    - For your community only 
    - or anyone can answer
- If Answers can be read openly: Y/N
- Can be shared by link
- can someone answer more then once, or only once
- Can answers be modified
- Show answers openly
- Start and End Date
- if an account is needed, if not only an email will be asked
    - send email confirmation
- for open sessions , start and end date
- verrouiller a partir d'une étape
- renommer le nom des réponses avec un champ de réponse


## Oceco sur mobile
- [Google Play](https://play.google.com/store/apps/details?id=org.communecter.oceco&hl=fr)
- [Apple Store](https://apps.apple.com/ci/app/oceco/id1523648204)

## Participer au développement
- [Faire des propositions d'idée, de fonctoinnalités, de nouveauté, de critique...](https://www.communecter.org/costum/co/index/slug/proceco#welcome.slug.ocecoform.formid.619392fb3a2d77635e546e53.page.list)
- [Roadmap du développement](https://codimd.communecter.org/4773mbh1TryRriorF79r3Q?both) 

## Description Technique
Le processus OCECO tourne sur un COcktail technologique de COmmunecter : 
- **COForm**  : Des formulaires 100% configurable, dupliquable et totalment connecté à votre réseau social 
- **CObservatoire** : des visualisations personnalisés totalement connectées au COForms
- **COMOBI** : L'application mobile de communecter qui tourne sur Android et Ios 
