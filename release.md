
# release 2020.#2
> [in Progress](inProgress.md)
> - COForms

# PROJET DE STAGIAIRE 
## COstum COCV
## COstum COBrainstorm
--- 
# release 2020.09
# COTools
(!@thomascraipeau)
- https://peertube.communecter.org/login : PEERTUBE dans communecter
- meet.communecter.org : Jistsi dans communecter 

# COForms 
(!@oceatoon !@Bouboule !@RaphaelRiviere)
- integration dans CO génériquement 
- appliqué 

# CObservatory
(!@oceatoon !@Anatole)
- OCECO : oce.co.tools

# OCECO FACT
( !@oceatoon !@BeaPotier !@florentBenameur )
[PPT](https://docs.google.com/presentation/d/1di2j3etdu2CeUJvk6Rq7J2RbO3Qr2evvaKKm9xDWZG8/edit#slide=id.g7f2715dafa_5_0)
- usage de COForm sur 4 étape 
    + Proposer
    + Decider
    + Financer
    + Suivre
- dépot de dossier FACT à ARACT ANACT avec une communauté de testeur

# COstum
- CRESS 
- CMCAS
- SOMMOM
- Hinaura mednum 
- ultranum mednum

# Projet Sommom : Questionnaire pour les observatoires de cétacés 
(!@Anatole)

# [Projet AFNIC] Interop (!@lotik !@florentBenameur !@Lamyne !@catalyst !@anis !@opteos)
## Interop Mediawiki
- lecture et structuration du contenu

## Interop Markdown externe
- ouverture des MD de 
    + codimd
    + github MD 

## Interop documentation Gitlab en MD
- utilsation de l'api GraphQL de gitLab 


--- 
# COstum 
- mednum
- smarterre
- smarterritoire
- costum générique candidat
- open atlas 
- pixelhumain 
