# coforms Tech
[documentation](~d/modules/costum/views/tpls/forms/README.md]
[schema](https://docs.google.com/presentation/d/1jwi3pYEzL_r8IH1AEpzlJe4jTt2kG7BZS5Hyq1kZHeE/edit#slide=id.p)

# json
[[~d/modules/costum/data/freeform.json]]
[[~d/modules/costum/data/forms/openForm.json]]

# templates : can be used anywhere in any costumForm
[[~d/modules/costum/views/custom/co/freeform.php]] 
  [[~d/modules/costum/views/custom/co/formSection.php]]
    [[~d/modules/costum/views/custom/co/formbuilder.php]]
      ** basic form tpls
      [[~d/modules/costum/views/tpls/forms/text.php]]
      [[~d/modules/costum/views/tpls/forms/select.php]]
      [[~d/modules/costum/views/tpls/forms/textarea.php]]
      [[~d/modules/costum/views/tpls/forms/checkbox.php]]
      ** complexe tpls forms
      [[~d/modules/costum/views/tpls/forms/calendar.php]]
  [[~d/modules/costum/views/custom/co/answerList.php]]

# OPEN FORM WITH WIZARD
[[~d/modules/costum/views/custom/co/formWizard.php]] 
  [[~d/modules/costum/views/tpls/forms/wizard.php]]
    [[~d/modules/costum/views/custom/co/formSection.php]]
      [[~d/modules/costum/views/custom/co/formbuilder.php]]


# TECHNICALLY , ARCHITECTURE
## parent Forms and subForms
### PARENT FORM 
are the master piece of a coform session 
it presents and holds 
- the parent Context
- the list of subForms 
- possible parameters 
- 
```
{
    "_id" : ObjectId("5f3e5d62275640c499c2e11a"),
    "id" : "ctenatForm",
    "parent" : {
        "5ca1b2bb40bb4e9352ba351b" : {
            "type" : "organizations",
            "name" : "CTE National"
        }
    },
    "homeTpl" : "survey.views.custom.ctenat.explainRow",
    "subForms" : [ 
        "action", 
        "caracter", 
        "murir", 
        "contractualiser", 
        "suivre"
    ],
    "source" : {
        "key" : "ctenat",
        "keys" : [ 
            "ctenat"
        ],
        "insertOrign" : "costum"
    },
    "active" : true,
    "params" : {
        "period" : {
            "from" : 2018,
            "to" : 2023
        },
        "elementelement1" : {
            "type" : "organizations",
            "limit" : ""
        },
        "elementelement3" : {
            "type" : "projects",
            "limit" : ""
        },
        "elementporteur" : {
            "type" : "organizations",
            "limit" : ""
        },
        "elementprojet" : {
            "type" : "projects",
            "limit" : ""
        },
        "elementaction" : {
            "type" : "projects",
            "limit" : ""
        },
        "elementparents" : {
            "type" : "organizations",
            "limit" : ""
        },
        "elementproject" : {
            "type" : "projects",
            "limit" : ""
        },
        "calendaropenForm19" : {
            "sectionTitles" : [ 
                "1er<br/>Sem<br/>2018", 
                "2ème<br/>Sem<br/>2018", 
                "1er<br/>Sem<br/>2019", 
                "2ème<br/>Sem<br/>2019", 
                "1er<br/>Sem<br/>2020", 
                "2ème<br/>Sem<br/>2020", 
                "1er<br/>Sem<br/>2021", 
                "2ème<br/>Sem<br/>2021", 
                "1er<br/>Sem<br/>2022", 
                "2ème<br/>Sem<br/>2022", 
                "1er<br/>Sem<br/>2023", 
                "2ème<br/>Sem<br/>2023"
            ],
            "dateSections" : [ 
                "01/01/2018", 
                "01/07/2018", 
                "01/01/2019", 
                "01/07/2019", 
                "01/01/2020", 
                "01/07/2020", 
                "01/01/2021", 
                "01/07/2021", 
                "01/01/2022", 
                "01/07/2022", 
                "01/01/2023", 
                "01/07/2023", 
                "01/01/2024"
            ]
        },
        "budgetbudget" : {
            "nature" : {
                "investissement" : "Investissement",
                "fonctionnement" : "Fonctionnement"
            },
            "amounts" : {
                "price" : "Montant",
                "amount2019" : "2019 (euros HT)",
                "amount2020" : "2020 (euros HT)",
                "amount2021" : "2021 (euros HT)",
                "amount2022" : "2022 (euros HT)",
                "amount2023" : "2023 (euros HT)"
            },
            "estimate" : "true"
        }
    },
    "updated" : NumberLong(1597679109),
    "graph" : {
        "budgetbudget_1" : {
            "label" : "test",
            "type" : "amount"
        },
        "budgetbudget_2" : {
            "label" : "",
            "type" : "poste"
        }
    }
}
```
### SUBFORM
## formTpl
  is the definition of a form, saved inside db.forms
  looks like this 
  ```
   "_id" : ObjectId("5e0d010dbf4d14b1f08dcaf7"),
      "id" : "openForm1",
      "type" : "openForm",
      "name" : "just a form",
      "inputs" : {
          "element" : {
              "label" : "L'auteur",
              "placeholder" : "Auteur",
              "type" : "tpls.pods.author",
              "info" : "si tu ne sais pas ou tu vas, regarde d'ou tu viens"
          },
          "element1" : {
              "label" : "Structure porteuse",
              "type" : "tpls.forms.cplx.element",
              "info" : "La simplicité est l'ultime sophistication Léonard de Vinci 1515"
          },
          "element2" : {
              "label" : "Structures associées",
              "type" : "tpls.forms.cplx.element",
              "info" : "La simplicité est l'ultime sophistication Léonard de Vinci 1515"
          },
          ...
  ```

## step rules 
## explain Tpl
## rendering order 

### OPEN FORM WITH WIZARD
[[~d/modules/costum/views/custom/co/formWizard.php]] 
  [[~d/modules/costum/views/tpls/forms/wizard.php]]
    [[~d/modules/costum/views/custom/co/formSection.php]]
      [[~d/modules/costum/views/custom/co/formbuilder.php]]
        individual inputs ased on configs

## links activity (historic, answered, voted....)



## FORM PARAMS

### anyOnewithLinkCanAnswer
### canReadOtherAnswers
### startDate
### endDate 
block editing if End date passed 
### oneAnswerPerPerson
if exist on va dessus 
existe par on génere si F5 
else  
### canModify 
### canVote : false
- add action 
- show voters count
- [ ] show voters list
-   [ ] color action I voted

### canFund : false
*   [X] costum.cms.fundMsg
*   [X] add action 
*   [X] show funders count and ampount
*   [ ] show funders list

### step rules
  - any step can have rules to manage how it is shown
  - required : is a path for a given value in the answer
```
rules.required : ["priorisation"]

or 
"rules.required" : [ 
    "answers.action.parents", 
    "answers.action.project"
]
```
  - checkValue : is a path and a value 
```
"rules" : {
        "checkValue" : {
            "priorisation" : [ 
                "Passage en réunion de finalisation", 
                "Action validée"
            ]
        }
    }
```

### BUG affichage de la liste des contributeur
- [ ] color action I funded
- [ ] costum.form.fundPlan : 4400€
-   [ ] equilibre Budget [[~d/modules/survey/views/custom/ctenat/resultsRow.php]]
- [X] costum.form.showAnswers
- [X] costum.form.params.calendar
- parameters needed to make this complexe input work 
- [X] costum.form.params.sumColumns
- list of column to sumup and show in the answers list
