# COFORMS : Collaborative Open Forms
![](https://codimd.communecter.org/uploads/upload_eeace7a51697831f6d96f276ed6098c0.png)

[PPT](https://docs.google.com/presentation/d/12E7zhh-Tp7J0fRcEVRIuxpXPlbPBVBP7GB3JnycPNm8/edit#slide=id.g737fb454ef_0_7)

# Slogans
- L’intelligence collective structurer
- Open Forms sont des semence libres , des graines qui peuvent faire pousser des communautés, du savoir, du partage, de la collaboration
- DIUKW : Data > Information > Understanding > Knowledge > Wisdom

# POUR QUOI FAIRE
- Créer des questionnaires librement 
    - avec des questions complexes 
- Mieux connaitre une communauté
- Généré un observatoire pour 
    - naviguer dans la donnée de la connaissance
    - mieux comprendre une communauté
    - creer de la stigmergie
    - creer de l'holoptisme
- Sondage
    - Journalismes citoyens 
    - des mémoires dynamiques étudiant- 
- Collaboratif
    - Brainstorming
    - Compte rendu, Taches, Agendas
- Parametrer un Costum 
- Usage Complexe et générique donc dupliquable simplement
    - Appel à projet Générique 
    - OCECO : outils de Gouvernance Horizontale
    - management de projet
    - gestion de groupement d’achat
- Virer Google Forms 

![](https://codimd.communecter.org/uploads/upload_3d2ba1bad1052ce1de487f6fbe3ebf96.png)

## om peut on utiliser les COForms
- directement dans communecter a partir des éléments (organisations, projet)
- il est possible de rattaché vos COforms à vos COstum 


# COFORM en qlqs lignes
- un équivalent Libre et open source à Google Forms, TypeForms et FramaForms
- créer des formulaires par et pour une communauté en toute autonomie (ou presque ;) ) 
- pouvoir ajouter des questions complexes (tableau multi dimension, créer des questions spécifiques qui n'existe pas dans les solutions de formulaire existante)
- pouvoir faire des questionnaires à étapes
- visualiser les résultats sous plusieurs formes (observatoire / graphes , carto, listing,...etc) 
- créer des workflows complets et complexes liés à des COstums (communecter personnalisé / en marque blanche)

# CO TEMPLATES
Les coforms template est une fonctoinnalité permettant d'enregistrer un coform comme un template pour une réutilisation dans un autre contexte ou par d'autre contexte.
la mutualisation (réutilisation) des coforms est possible avec plus ou moins de contrainte avec le tpl d'origine : 
- Réutilisation et synchro avec un coformTpl existant 
    - impossible de modifier 
    - les modifications faites par l'auteur sont appliqué à tout les contexte qui l'utilise
- Copie et autonomie 
    - duplique le tpl choisit 
    - les modifs du parent ne sont pas impacté, donc ne sont pas visible sur le tpl Copié 
    - approprioation total et autonomie complète pour modifier le tpl 
    - peut etre enregistrer comme un nouveau tpl

## Options
Things you can parameter and configure
- Name of the Form 
- Description
- Active / Not Active
- Private / Public
    - For your community only 
    - or anyone can answer
- If Answers can be read openly: Y/N
- Can be shared by link
- can someone answer more then once, or only once
- Can answers be modified
- Show answers openly
- Start and End Date
- if an account is needed, if not only an email will be asked
    - send email confirmation
- for open sessions , start and end date
- verrouiller a partir d'une étape
- renommer le nom des réponses avec un champ de réponse

## description de tout les inputs disponible
### simple inputs
## complexe inputs 

## Questions
- can I use COforms inside my website 
    - unfortunetly COForm are totaly connected to CO
    - you'll have to install all CO to use it 
- can people answer without a CO account 
    - yes, this is an option, you'll need to configure

## créer vos formulaires
- sur une page organisation ou projet  
- click le bouton COform
- 2 options
    - creer un nouveau formulaire : bouton `Nouveau Formulaire`
    - réutiliser un template existant : bouton `Menu Template`
- configurer les paramètres


