 # Innovation

## Description du caractère innovant (technique et/ou usage) du service ou du produit par rapport à l’offre existante

Proposer un réseau social libre et participatif des acteurs de la culture au travers d’une plateforme collaborative et ouverte est innovant à plus d’un titre : 
Au niveau technique, l’innovation de COmmunecter réside aussi bien dans ses fonctionnalités, dont certaines ont fait l’objet d’une incubation à la Technopole de La Réunion, que dans la manière dont il se construit au quotidien suivant un processus agile, souple et interactif au travers d’une gouvernance horizontale et structurée en coopérative. 

Son approche systémique qui permet de réunir sur un même espace de concertation et d’interaction, en leur offrant des avantages à une participation, tous les acteurs de la culture dans la pluralité de leurs métiers, de leurs disciplines, dans leur diversité en y associant également les habitants, le tissu associatif, les entreprises et les collectivités dans une approche de type filière.. 
Son approche locale : pour tendre vers une démocratie culturelle pour être productive et efficace, doit se faire avec les habitants et passe forcément par de la proximité : groupe d’acteurs, quartiers etc…
Une plateforme basée sur COmmunecter se distingue d'ailleurs des réseaux sociaux classiques par les sujets traités (agriculture, alimentation, culture, transports, énergie, déchets, vivre ensemble, économie du partage, participation citoyenne…) et sa volonté d’implantation locale, au niveau de chaque commune, là où les décisions citoyennes prennent tout leur sens.

Des acteurs et des citoyens connectés pour une participation accrue et complémentaire : les technologies actuelles permettent d’échanger, de lancer des actions, d’organiser une concertation directement depuis internet et des applications mobiles en complément des rencontres et actions physiques pour en accroître l’efficacité. Une technologie à la pointe, qui permet de ne pas restreindre le nombre de participants mais d’ouvrir un accès à tous, une technologie qui ne se limite pas à un lieu d’échange avec des contraintes d’horaire d’ouverture, mais propose une plus grande autonomie en étant accessible en tout lieu et 24h/24 ! Une technologie qui permet notamment aux personnes isolées, timides, influençables de pouvoir s’exprimer sans crainte.
Une plateforme multiplicatrice de valeurs du fait d’une information partagée et crowdsourcée
Un mode de gouvernance novateur reliant tous les contributeurs et partenaires. 

Proposer un réseau social libre et participatif des acteurs de la culture comme proposé aujourd’hui pourra devenir une expérience d'intelligence collective mélangeant entre autre innovation sociétale basée sur l’humain, approche systémique, ouverture et collaboration, co-création suivant une méthode agile, cofinancement et mutualisation des coûts : un véritable changement de paradigme. 

#innovation sociétale    
#besoins sociaux
#territoire connecté 
#intelligence collective 
#société de demain 
#système d’information territorial
#base de connaissance localisée
#open-source
#gratuit
#données protégées
#informations partagées
#linked data
#objets connectés 
#interopérabilité
#bien commun 
#humain
#participation citoyenne
#démocratie culturelle
#territoire intelligent
#expérimentation 
#systémique
#collaboratif
#co-construction
#cofinancement
