# Agenda

## Pourquoi partager des évènements sur Communecter ?

Référencer vos évènements sur Communecter apporte de nombreux avantages :

-   L'évènement apparaît dans l'Agenda et sur la carte
-   Vous (ou quelqu'un d'autre) peut créer une carte autonome ([en savoir +](/2 - Utiliser l'outil/Fonctionnalités/network.md))
    -   intégrable sur votre propre site ([exemple](https://zds.fr/adressesfaq/carte-zero-dechet/))
    -   filtrable par territoire (villes, départements et régions) et/ou par thématique (grâce à l'utilisation de tags)
-   Une équipe motivée et engagée est disponible gratuitement pour vous assister, [contrairement à Google Maps qui fait payer son service](https://www.numerama.com/tech/367443-google-maps-devient-beaucoup-plus-cher-petit-seisme-dans-le-monde-de-la-cartographie.html)
-   **Vous, ou les évènements que vous référencer, font partie d'un commun dédié à l'interconnexion des initiatives citoyennes. C'est à dire que vous donner la possibilité à tous les participants d'interagir entre eux via un réseau social qui n'a aucun but lucratif.**
-   Vous participez au bien commun puisque vos évènements seront en Open Data et accessibles via [notre API](/4 - Documentation technique/api.md)

## Utilisation
À venir...

![](/Images/calendrier.png)
