# Cartes dans Communecter

## Sur Communecter
Toutes les informations affichées dans Communecter peuvent être affichées sous forme de carte grâce au bouton bleu en haut à droite.

![](/Images/cartepartout.png)


## Sur une carte extérieure
Pour créer une carte et l'intégrer sur votre site, voir la page [Créer une carte](/2 - Utiliser l'outil/Fonctionnalités/network.md).


### Découverte de l’interface de navigation
<iframe width="560" height="315" src="https://www.youtube.com/embed/XcuYgWg5zj0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](/Images/interfacecarto.png)

Vous pouvez accéder à une carte Communecter via :
-   le site communecter.org
-   un site tierce intégrant la carte dans sa page
-   ou une adresse internet dédiée

Les critères et la zone géographique des points affichés dans la carte sont définis par la personne qui a créé la carte. Sur la droite vous pouvez zoomer plus ou moins sur une zone géographique. Sur la gauche, les filtres vous permettent d’affiner votre recherche selon des critères définis par l’éditeur. Sur la droite, vous trouverez un moteur de recherche pour affiner votre recherche selon vos critères, un bouton pour changer de fond de carte et le zoom. En haut à droite, vous pouvez passer en mode liste. En cliquant sur en savoir plus, vous accédez à la fiche complète du point d’intérêt.


### Créer un compte Communecter via le module network
<iframe width="560" height="315" src="https://www.youtube.com/embed/hK5JaYYj0KA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](/Images/inscrirecarto.png)

Comme pour Wikipédia, un compte est indispensable pour modifier la fiche d’une organisation présente sur la carte. Vous pouvez vous inscrire sur Communecter en cliquant sur le bandeau en haut de la carte, puis “S’inscrire”. Entrez les informations puis cliquez sur je m’inscris.


### Ajouter une organisation
<iframe width="560" height="315" src="https://www.youtube.com/embed/DTuzy7bWo-0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![Ajouter un lieu](/Images/ajoutcarto.png)

Une fois connecté vous trouverez en bas à gauche le bouton vous permettant d’ajouter un élément. Le type d’élément proposé (organisation, événement, … ) a été choisi par la personne ayant créé la carte. Si vous souhaitez compléter la fiche créée, il faudra passer par le site communecter.org.


### Modifier une fiche
<iframe width="560" height="315" src="https://www.youtube.com/embed/fqt_fkV0Mfo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![Accès fiche](/Images/accesfiche.png)
![Modifier fiche](/Images/modifierfiche.png)

Connectez-vous avec votre compte communecter.org. Choisissez un point d'intérêt, puis cliquez sur "en savoir +". Modifiez la fiche et n'oubliez pas de sauvegarder. L'onglet "historique" vous permet de connaître la date, l'auteur et la teneur exacte de chaque modification.
