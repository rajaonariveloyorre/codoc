# Les éléments dans Communecter

Communecter est une base de données libre et géolocalisée des acteurs et des activités des territoires. Pour développer l'outil il a fallut définir plus précisement ce qu'on entendait par _acteurs_ et _activités_.

Pour aller plus loin : [Une base de données des acteurs et des activités de nos territoires : pourquoi et pour qui ?](http://open-atlas.org/une-base-de-donnees-des-acteurs-et-des-activites-de-nos-territoires-pourquoi-et-pour-qui/)

Nous avons développé un "Modèle de territoire" dont les éléments regroupent une majorité (si ce n'est la totalité) des formes d'interactions sociales que nous trouvons dans nos territoires.

Dans ce modèle que nous avons souhaité simple et pragmatique, on distingue 2 types d'éléments :

-   **Les éléments principaux** qui ont une existence autoporteuse dans COmmunecter (possèdent une URL propre) et sont associé à un écosystème propre. Il s'agit de citoyens, d'organisation, de projets ou d'événements.
-   **Les petits éléments** qui découlent nécessairement d'éléments principaux (points d'intérêt, ressources, annonces...)


## Éléments principaux
Chaque élément principal possède une URL propre permettant d'accéder à une fiche de présentation de son écosystème.

![](/Images/elements-ecosysteme.png)


### Citoyens
##### Qu'est ce qu'un Citoyen pour COmmunecter ?
L'individu est au centre de toute l'architecture et de tous les services de COmmunecter.

![](/Images/elements-individu.png)

Le citoyen est ici défini comme une personne souhaitant contribuer à ou simplement prendre connaissance de la vie de son territoire. Son rôle, ses centres d'intérêts, ses capacités et ressources ne sont pas prédéfinies et sont totalement ouverts !

**Citoyens et ouverture de compte COmmunecter** : un simple visiteur peut rechercher et accéder à tout le contenu public référencé sur COmmunecter. Pour devenir "Citoyen" référencé sur le réseau COmmunecter et pour pouvoir contribuer, il faut ouvrir un compte. C'est nécessaire pour poster du contenu, donner son avis, déclarer un projet, s'inscrire à un événement, etc. Ca peut être en son nom ou sous un pseudo, selon les envies de chacun et la manière dont on a envie d'interagir.


##### L'écosystème du citoyen, ses attributs et les outils à sa disposition :
Pour exister, il suffit au citoyen d'avoir un nom d'utilisateur. il peut ensuite librement renseigner diverses autres informations utiles à s'identifier auprès de ses pairs et à ses activités sur COmmunecter (ces informations sont disponible dans **À propos**).

![](/Images/elements-individu-infos.png) Le citoyen peut accéder à différents outils : **l'agenda** affiche les évènements auxquels le citoyen participe. On a également accès aux **Projets** auxquels il contribue, ainsi qu'aux **Ressources** et **Annonces** qu'il a déclaré.

![](/Images/elements-individu-profils.png)

**Photos** ouvre sa galerie publique, et **Librairie** permet d'accèder aux URL et documents partagées. L'onglet **Communauté** liste les éléments auquel il est abonné, les utilisateurs·trices qui souhaitent recevoir son actualité et les organisations dont il est membre.

Une liste de ses **Points d'intérêts** est accessible et n'importe qui peut créer des **Collections** d'éléments pour naviguer plus rapidement.


### Organisations, projets et évènements
Ces éléments peuvent afficher les mêmes informations qu'un citoyen (agenda, projets, communauté, etc..) et possède une rubrique **Nous contacter**.
La section **Nos valeurs** est disponible pour les organisations et projets.


#### Organisation
C'est un groupement d'individus obligatoirement typé dans une de ces catégories : **Association**, **Groupe**, **Entreprise** ou **Service public**.
![](/Images/elements-orga.png)
Pour créer et gérer une organisation il faut d'abord se créer un compte en tant qu'individu.


#### Projet
Les projets sont des actions délimités dans le temps.
![](/Images/elements-projet.png)


#### Événement
Les évènements apparaissent dans l'[agenda](/2 - Utiliser l'outil/Fonctionnalités/agenda.md). Un évènement (ex : un festival) peut créer des sous-évènements (ex : concerts, conférences, ...) qui apparaitront dans l'onglet **Programme**.
![](/Images/elements-evenement.png)


## Petits éléments
Les petits éléments sont obligatoirement déclarés par un élément principal. Ils possèdent une description succincte et il est possible d'y associer une photo.
Ces éléments s'ouvrent dans un simple panneau latéral, contrairement aux éléments principaux qui possèdent une fiche complète.


### Point d'intérêt
Ils sont utilisés pour référencer des endroits où se trouvent du matériel ou des œuvres installées dans un lieu public. Exemples : compost, graffiti, scène de concert, toilettes, …
![](/Images/elements-poi.png)


### Ressource
C'est une offre ou un besoin de service, compétence ou matériel.
![](/Images/elements-ressource.png)


### Annonce
Une annonce est utile pour vendre, louer, donner du matériel, ou pour chercher un emploi.
![](/Images/elements-annonce.png)
