# Les rôles dans Communecter

Les administrateurs d'une organisation, évènement ou projet peuvent attribuer des rôles aux membres.
Si une organisation n'a pas de membres, n'importe qui peut s'auto-attribuer le rôle d'administrateur. **Les règles sont différentes selon si l'élément est en édition libre ou non.**


## Non-membre
Un·e citoyen·ne qui n'est pas membre d'une organisation, d'un évènement ou d'un projet peut :
-   Accéder à toutes les informations publiées :
    -   `Fil d'actualité`
    -   tout ce qui se trouve dans la colonne de gauche : `À propos`, `Annonces`, `Agenda`, etc.
    -   les différentes visualisations : QR code, vue en graphes et impression
-   S'abonner à l'élément pour que l'actualité publiée apparaisse sur votre fil d'actualité personnel
-   Demander à devenir membre et administrateur*
-   Publier un message privé
-   Demander la suppression de l'élément**
-   Inviter des membres

_*S'il y en a, ce sont les membres accepte ou refuse les nouveaux entrants._
_**S'il y en a, les membres ont 5 jours pour s'opposer à la suppression._

Si l'élément est en édition libre :
-   Éditer `À propos`
-   Accéder à la `Messagerie`
-   Gérer l'`Espace coopératif` (ouvrir des espaces, des propositions et des tâches)
-   Créer des sous-éléments (ex : créer un projet à partir d'une association)
-   Attribuer des rôles aux membres


## Membre
En plus de ce qui est possible de faire en tant que non-membre, un·e citoyen·ne faisant partie d'un élément peut :
-   Publier au nom de l'élément
-   Éditer `À propos`
-   Accéder à la `Messagerie`
-   Gérer l'`Espace coopératif` (création d'espaces, de propositions et de tâches)
-   Créer des sous-éléments (ex : créer un projet à partir d'une association)
-   Attribuer des rôles aux membres


## Administrateur
Pour demander à administrateur ça se passe sous l'image de profil :
![](/Images/role-devenir-admin.png)

Les administrateurs ont les mêmes pouvoirs que les membres, plus ceux là :
-   Modifier les paramètres de confidentialité : l'édition (libre ou non) et l'accès aux informations (open data ou non)
-   Accepter les demandes d'intégration à la communauté de l'élément
-   Retirer des membres de l'élément
