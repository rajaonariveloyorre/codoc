# VISION PRODUIT

## Nom
- CO.TOOLS
- CO.OP.TOOLS

## OBJECTIFS RAISON D'ETRE
- COMMUNAUTÉ
- AUTO ORGANISATION
- COLLABORATION 
- COOPÉRATION 
- CREATIVITÉ 
- INTELLIGENCE COLLECTIVE 
- HOLOPTISME 
- STIGMERGIE 
- GOUVERNANCE 
- ORIENTATION DYNAMIQUE 
- MUTUALISATION 
- FINANCEMENT 

## COMMUNECTER
> Un Réseau social Libre et open source 
- Géolocalisé
- Territoire (Nation, REgion, Agglo, Commune
- Citoyen
- Thématique Tags
- [Fonctionalités](https://doc.co.tools/books/2---utiliser-loutil/chapter/fonctionnalit%C3%A9s)

## SMARTERRE
> Des outils pour les territoires
- Territoire 
- Intelligent 
- Apprenant 

## COSTUM
> Tout communecter à la carte
- Personnaliser 
- Marque Blanche
- Design 

## COFORM
> Des formulaires génériques et adaptés
- Connaissance 
- Recolter 

## COBSERVATORY
> Connecter les données pour mieux comprendre, décider et agir
- Mesurer
- Donner du sens
- Analyser
- Décider

## COEUR
> un methode pour gagner en autonomie et créer du lien 
> COllaborer entre Organisation et Evennement pour Unir les Ressources
- Création de liens
- auto organisation
- partage de service compétence 
- systeme d'offre et besoin

## OCECO
> Outils et méthodes 
de travail Collaboratif pour une Économie COopérative

- Action 
- Mouvement 
- Ancrage
- Proposition Projet
- Gouvernance 
- Financement
- Travaux suivi 
- Rémunération Dynamique 

## OUTILS OPEN SOURCE (CHATON) 
- chat : Rocket Chat chat.communecter.org
- kanban : wekan.communecter.org 
- drive : NextCloud conextcloud.communecter.org
- editor : codimd.communecter.org
- video : peertube.communecter.org
- visio : jitsi.communecter.org
- Authentification : sso.communecter.org

![](https://codimd.communecter.org/uploads/upload_d91749148c093b3a085c9c7e8c042711.png)
![](https://codimd.communecter.org/uploads/upload_37d79696dbf9b3053a8d42c400d0adaa.png)
[CO.OP.TOOLS](https://docs.google.com/presentation/d/1sgDu7mya0yq9PgDOj0KuKEFc77j3aevuuuyzO7qSLZE/edit#slide=id.g87ac77bfdb_0_0)
[DESIGN](https://docs.google.com/drawings/d/12VPzz5JL9KnWisny02o45vskjETNUSbqgmBCH70_rdc/edit)
