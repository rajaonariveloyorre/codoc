# Applications Communecter
English version

## Présentation
Groupe de travail et de discussions : **[#comobi](https://chat.communecter.org/channel/comobi)**

## Installation
-   **Android** : [Google Play](https://play.google.com/store/apps/details?id=org.communecter.mobile)
-   **iOS** : [Apple Store](https://itunes.apple.com/fr/app/communecter/id1384473581?l=fr&ls=1&mt=8)
-   **Windows**, **Mac** et **Linux** (App Image) : [télécharger](https://gitlab.adullact.net/pixelhumain/comobi/-/releases)
-   **Linux** Snap store (A universal app store for Linux) : [Snapcraft](https://snapcraft.io/communecter)

![](../Images/comobi.jpg)
